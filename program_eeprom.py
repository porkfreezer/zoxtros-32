#!/usr/bin/python3

import serial
from time import sleep

ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=10)

def send(string):
	for i in string:
		ser.write(i.encode())
		print(ser.read(1).decode(), end='')
	print(ser.read(1).decode(), end='')

name = input('Name= ')
srec = open(input('File= '), 'r').readlines()
send('n ' + name + '\n')
total = 0
program_start = 0x0013
for rec in srec:
	if len(rec) > 9 and rec[1] == '1':
		count = int(rec[2:4], 16)
		total += count - 3
		address = int(rec[4:8], 16) - program_start + 3 + len(name)
		data = []
		for i in range(4, count + 1):
			data.append(rec[i*2:(i*2)+2])
		send('a %04X\n' % address)
		for i in data:
			send('w ' + i + '\n')
send('t %04X\n' % total)
