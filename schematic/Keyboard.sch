EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5906 4331
encoding utf-8
Sheet 3 3
Title "Zoxtros-32 Keyboard sample"
Date "2021-11-11"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1300 1000 0    50   Input ~ 0
A2
Text HLabel 1300 1100 0    50   Input ~ 0
A3
Text HLabel 1300 1200 0    50   Input ~ 0
A4
Text HLabel 1300 1300 0    50   Input ~ 0
A5
Text HLabel 1300 1400 0    50   Input ~ 0
A6
Text HLabel 1300 1500 0    50   Input ~ 0
A7
Text HLabel 1300 1650 0    50   Input ~ 0
B0
Text HLabel 1300 1750 0    50   Input ~ 0
B1
Text HLabel 1300 1850 0    50   Input ~ 0
B2
Text HLabel 1300 1950 0    50   Input ~ 0
B3
Text HLabel 1300 2050 0    50   Input ~ 0
B4
Text HLabel 1300 2150 0    50   Input ~ 0
B5
Text HLabel 1300 2250 0    50   Input ~ 0
B6
$Comp
L Switch:SW_Push SW3
U 1 1 61946056
P 2100 1000
F 0 "SW3" H 2100 1285 50  0000 C CNN
F 1 "SW_Push" H 2100 1194 50  0000 C CNN
F 2 "" H 2100 1200 50  0001 C CNN
F 3 "~" H 2100 1200 50  0001 C CNN
	1    2100 1000
	1    0    0    -1  
$EndComp
$Comp
L pspice:DIODE D4
U 1 1 619464B6
P 2500 1000
F 0 "D4" H 2500 1265 50  0000 C CNN
F 1 "DIODE" H 2500 1174 50  0000 C CNN
F 2 "" H 2500 1000 50  0001 C CNN
F 3 "~" H 2500 1000 50  0001 C CNN
	1    2500 1000
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 6194756C
P 2100 1400
F 0 "SW4" H 2100 1685 50  0000 C CNN
F 1 "SW_Push" H 2100 1594 50  0000 C CNN
F 2 "" H 2100 1600 50  0001 C CNN
F 3 "~" H 2100 1600 50  0001 C CNN
	1    2100 1400
	1    0    0    -1  
$EndComp
$Comp
L pspice:DIODE D5
U 1 1 61947C6A
P 2500 1400
F 0 "D5" H 2500 1665 50  0000 C CNN
F 1 "DIODE" H 2500 1574 50  0000 C CNN
F 2 "" H 2500 1400 50  0001 C CNN
F 3 "~" H 2500 1400 50  0001 C CNN
	1    2500 1400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 1000 1900 1400
Wire Wire Line
	1900 1000 1300 1000
Connection ~ 1900 1000
Wire Wire Line
	1300 1650 2700 1650
Wire Wire Line
	2700 1650 2700 1400
Wire Wire Line
	2700 1000 2850 1000
Wire Wire Line
	2850 1000 2850 1750
Wire Wire Line
	2850 1750 1300 1750
Text Notes 1900 1600 0    50   ~ 0
and so on...
$EndSCHEMATC
