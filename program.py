#!/usr/bin/python3

import serial
import time
from sys import argv
import os
import struct

if len(argv) < 2:
	print("No file")
	exit()

done = 0

srec = open(argv[1], 'r').readlines()
ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=1)
time.sleep(.5)

def send(bytestring):
	bts = struct.unpack(str(len(bytestring)) + 'c', bytestring)
	for i in bts:
		ser.write(i)
#		print(i, end=' ')
		time.sleep(0.00008)
	while ser.in_waiting == 0:
		pass
	while ser.in_waiting > 0:
		ser.read()
#		print(ser.read(), end='')
#	print()
#	print("\nReturn:",end='')
#	time.sleep(.1)
#	while ser.in_waiting > 0:
#		print(ser.read(), end='')
#	print()

start = time.time()		

try:
	send(b'\nc\nwb\n')
	for rec in srec:
		if rec[1] == '1':
			count = int(rec[2:4], 16)
			count_b = (count - 3).to_bytes(1, 'big')
			address = bytes.fromhex(rec[4:8])
			data = bytes.fromhex(rec[8:((count + 1) * 2)])
			send(address + count_b + data)
			done += count - 3
		print('\r' + str(done) + 'B', end='', flush=True)
except:
	print('\rStopped.')

elapsed = time.time()-start;
print('\rSent ' + str(done) + ' B in ' + str(int(elapsed)) + ' s (' + str(int(done/elapsed)) + ' B/s)')
#time.sleep(3)
ser.setDTR(False)
time.sleep(.1)
ser.setDTR(True)
