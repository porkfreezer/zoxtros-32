# Zoxtros-32
This is a 1980s-style computer I built using an HD6309 microprocessor, Hitachi's improved version of the more common Motorola MC6809.
For more information, see [the wiki](https://gitlab.com/porkfreezer/zoxtros-32/-/wikis/home).

<img src="zoxtros-32.jpg" width="600">