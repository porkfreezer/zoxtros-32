;  via.asm
;  This file has shared definitions and routines for the R65C22 VIA  
;
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;  
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;  
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;  
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;  
;  

VIA_PORTB = $8000
VIA_PORTA = $8001
VIA_DDRB = $8002
VIA_DDRA = $8003
VIA_T1CL = $8004
VIA_T1CH = $8005
VIA_T1LL = $8006
VIA_T1LH = $8007
VIA_T2CL = $8008
VIA_T2CH = $8009
VIA_SR = $800A
VIA_ACR = $800B
VIA_PCR = $800C
VIA_IFR = $800D
VIA_IER = $800E
; VIA_PORTA = $800F

; initialize VIA
via_init:
	lda	#$FF
	ldx	#KEY_STATE+2
@l1:
	sta	,--X
	leax	1,X
	bne	@l1
	clra
	sta	VIA_DDRB
	sta	VIA_DDRA
	sta	VIA_PORTA
	lda	#%01000000	; T1 continous interrupts
	sta	VIA_ACR
	lda	#$FF
	sta	VIA_PCR
	sta	VIA_T1CL
	sta	VIA_T1CH
	lda	#%11000000	; T1 interrupt
	sta	VIA_IER
	rts
