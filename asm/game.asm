	include	vdp_macros.asm
	include	kernel.inc

	setdp	0

SPRPAT = $0000
PAT = $0800
NAME = $1400
PLAYER = $1000
COLOR = $2000

MONSTER_SCORE = 2

JUMP = 7
DELAY = 50000000	; ns

	org	PROGRAM_START

PX:		rmb	1
PY:		rmb	1
VY:		rmb	1
DIR:		rmb	1
SWING:	rmb	1
COOL:		rmb	1
TILENUM:	rmd	1
LIVES:	rmb	1
SCORE:	rmb	1
LSCORE:	rmb	1
LLEVEL:	rmd	1
LEVEL:	rmd	1
MONSTERS:

	org	platformer

start:
	ldx	#g1_vdp_init
	jsr	vdp_init
	lda	#4
	sta	DIR
	clrd
	sta	SWING
	vdp_write_address
	ldx	#$2020
!			; clear
	lbrn	0
	stb	VDP_VRAM
	leax	-1,X
	bne	<
	; set color table
	ldd	#COLOR
	vdp_write_address
	ldd	#$CE20	; fg: dark green, bg: gray; $20 entries
!
	lbrn	0
	sta	VDP_VRAM
	decb
	bne	<

	ldd	#NAME+32*23		; set last row to ground
	vdp_write_address
	ldd	#$0320
!
	lbrn	0
	sta	VDP_VRAM
	decb
	bne	<

	ldd	#PAT+8*1
	ldx	#patterns
l7:
	vdp_write_address
	anda	#$BF
	ldf	#8
!
	lde	,X+
	ste	VDP_VRAM
	decf
	bne	<
	addd	#16
	cmpx	#patterns_end
	beq	>
	cmpx	#text_patterns+8+'9'*8
	beq	l8
	cmpx	#patterns2
	bne	l7
	ldd	#PAT+8*8
	bra	l7
!
	ldx	#text_patterns+'0'*8
	ldd	#PAT+8*16
	bra	l7

l8:
	ldd	#COLOR+1
	vdp_write_address
	lda	#$AE			; fg: light yellow bg: gray
	sta	VDP_VRAM
	lda	#$DE			; fg: magenta bg: gray
	sta	VDP_VRAM
	brn	<
	sta	VDP_VRAM
	brn	<
	sta	VDP_VRAM

	ldd	#SPRPAT+$20
	ldx	#sprite_patterns
	ldy	#sprite_pattern_end
	jsr	vdp_copy_vram

reset:
	lda	#5
	clr	SCORE
	clr	LSCORE
	sta	LIVES
	ldx	#level
	stx	LEVEL
loadlvl:
	lda	#%10000010	; blank
	sta	VDP_REG
	lda	#$81
	sta	VDP_REG
	ldd	#NAME
	vdp_write_address
	clre
	ldd	#736
!
	lbrn	0
	ste	VDP_VRAM
	decd
	bne	<
	ldw	#$080E
	ldd	#NAME+32
	vdp_write_address
	ste	VDP_VRAM
	ldd	#NAME+64
	vdp_write_address
	stf	VDP_VRAM

	lda	SCORE
	sta	LSCORE
	ldx	LEVEL
	stx	LLEVEL
plat:
	ldd	,X++
	bmi	platdone
	addd	#NAME
	vdp_write_address
	ldb	,X+
	lda	#1
	sta	VDP_VRAM
	lda	#3
	jsr	vdp_delay
	decb
!
	cmpb	#1
	ble	l5
	sta	VDP_VRAM
	decb
	bra	<
l5:
	lda	#5
	sta	VDP_VRAM
	bra	plat
platdone:
	lde	#8
!
	ldd	,X++
	bmi	>
	addd	#NAME
	vdp_write_address
	ste	VDP_VRAM
	bra	<
!
	ldd	#PLAYER+4
	vdp_write_address
	ldy	#MONSTERS
l18:
	ldd	,X++
	bmi	l17
	sta	VDP_VRAM		; set Y
	sta	4,Y	; save Y
	stb	,Y+	; save X min
	pshs	B
	addb	,X+
	stb	,Y+	; save X max
!
	jsr	random
	adda	,S
	cmpr	B,A
	bhi	<
	cmpa	-2,Y
	blo	<
	leas	1,S
	sta	,Y+			; save X
	sta	VDP_VRAM		; set X
	clr	,Y++	; save direction
	jsr	random
	bpl	>
	inc	-2,Y
!
	ldd	#$1404
	sta	VDP_VRAM	; name
	brn	<
	stb	VDP_VRAM	; color
	bra	l18
l17:
	lda	#$D0
	sta	VDP_VRAM
	clr	,Y+
	clr	,Y+
;
	ldd	,X++
	tsta
	bpl	l15
	lda	SCORE
	ldw	#NAME+33
	jsr	shownum
	lda	LIVES
	ldw	#NAME+65
	jsr	shownum
	ldd	#PLAYER
	ldx	#sprite_attr
	ldy	#sprite_attr_end
	jsr	vdp_copy_vram
	lda	#%10000010
	jsr	vdp_enable
	ldx	#win_song
	jsr	song
!
	tst	JOY_BUTTON
	beq	<
	jmp	reset
l15:
	stx	LEVEL
	lde	#10
	addd	#NAME
	vdp_write_address
	ste	VDP_VRAM
	lde	#12
	anda	#$3F
	addd	#32
	vdp_write_address
	ste	VDP_VRAM

	ldd	#PLAYER
	ldx	#sprite_attr
	ldy	#sprite_attr_end
	jsr	vdp_copy_vram
	ldd	#$10A7
	std	PX

	ldx	#level_song
	jsr	song

	lda	#%10000010
	jsr	vdp_enable

game_loop:
	lda	SCORE
	ldw	#NAME+33
	jsr	shownum
	lda	LIVES
	ldw	#NAME+65
	jsr	shownum

	ldd	#DELAY/2500
!
	decd
	bne	<

	lda	JOY_RIGHT
	beq	>
	ldb	PX
	cmpb	#256-16
	beq	>
	lsla
	adda	PX
	ldb	#4
	stb	DIR
	bra	stpos
!
	lda	JOY_LEFT
	beq	>
	ldb	PX
	beq	>
	lsla
	nega
	adda	PX
	ldb	#12
	stb	DIR
	bra	stpos
!
	lda	PX
stpos:
	sta	PX
	jsr	onground
	beq	flying
	lda	PY
	inca
	tst	JOY_DOWN
	beq	notdown
	cmpa	#$A8
	beq	notdown
!
	inca
	bita	#7
	beq	l6
	bra	<
notdown:
	bita	#7
	beq	l6
	deca
	bra	notdown
l6:
	deca
	sta	PY
	lda	JOY_UP
	beq	>
	ldx	#5000
	ldy	#300
	jsr	sound
	lda	#JUMP
	sta	VY
	bra	flying
!
	lda	#1
	sta	VY
flying:
	dec	VY
	bmi	l1		; if falling, check every pixel for ground
	lda	PY		; else, just add velocity
	suba	VY
	sta	PY
	bra	l2
l1:
	lda	VY
	nega
	tfr	A,F
l3:
	jsr	onground
	beq	>
	ldb	#1
	stb	VY
	bra	l2
!
	inc	PY
	decf
	bne	l3
l2:
	ldd	#PLAYER
	vdp_write_address
	lda	PY
	sta	VDP_VRAM
	lda	PX
	jsr	vdp_delay
	sta	VDP_VRAM

	tst	JOY_TRIGGER
	beq	>
	tst	SWING
	bne	>
	tst	COOL
	bne	>
	lda	#6
	sta	SWING
	adda	#4
	sta	COOL
!
	tst	SWING
	beq	>
	dec	SWING
!
	tst	COOL
	beq	>
	dec	COOL
!
	jsr	setdir

	ldx	#MONSTERS
	ldy	#PLAYER+4+1
l9:
	ldw	,X		; E=min x F=max x
	lbeq	l10
	ldd	2,X	; A=x B=dir
	incb
	lbeq	l13	; B=0xFF = destroyed
	decb
	beq	>
	deca
	cmpr	E,A
	bhi	l11
	decb
	stb	3,X
	bra	l11
!
	inca
	cmpr	F,A
	blo	l11
	incb
	stb	3,X
l11:
	tfr	A,E
	sta	2,X
	suba	PX
	cmpa	#-13
	ble	l12
	cmpa	#13
	bge	l12
	lda	4,X
	suba	PY
	cmpa	#-3
	ble	l12
	cmpa	#15
	bge	l12
	tst	SWING
	beq	>
	tfr	Y,D
	incd
	vdp_write_address
	lda	#24
	sta	VDP_VRAM
	lda	#$FF
	sta	3,X
	pshs	X
	ldx	#kill_song
	jsr	song
	puls	X
	lda	SCORE
	adda	#MONSTER_SCORE
	sta	SCORE
	bra	l13
!
	ldx	#game_over_song
	jsr	song
	ldx	LLEVEL
	stx	LEVEL
	lda	LSCORE
	sta	SCORE
!
	tst	JOY_BUTTON
	beq	<
	dec	LIVES
	lbeq	reset
	jmp	loadlvl
l12:
	tfr	Y,D
	vdp_write_address
	ste	VDP_VRAM
l13:
	leay	4,Y
	leax	5,X
	jmp	l9
l10:

	ldb	#9
	jsr	gettile
	lde	DIR
	ldw	TILENUM
	cmpa	#8
	beq	l16
	ble	>
	cmpa	#12
	lble	loadlvl
!
	incw
	cmpb	#8
	beq	l16
	ble	>
	cmpb	#12
	lble	loadlvl
!
	jmp	game_loop
l16:
	tfr	W,D
	vdp_write_address
	clra
	sta	VDP_VRAM
	inc	SCORE
	ldx	#coin_song
	jsr	song
	jmp	game_loop

; A=num W=vram address
shownum:
	clrb
!
	cmpa	#10
	blt	>
	suba	#10
	incb
	bra	<
!
	lsla
	adda	#16
	lslb
	addb	#16
	exg	D,W
	vdp_write_address
	stf	VDP_VRAM	; tens
	jsr	vdp_delay
	ste	VDP_VRAM	; ones
	rts

setdir:
	ldd	#PLAYER+2
	vdp_write_address
	lda	DIR
	tst	SWING
	beq	>
	adda	#4
!
	sta	VDP_VRAM
	rts

; Set zero if not on ground
onground:
	ldb	#17
gettile:
	addb	PY
	lsrb
	lsrb
	lsrb			; Y/8
	lda	#32
	mul			; (Y/8)*32
	pshs	D
	clra
	ldb	PX
	lsrb
	lsrb
	lsrb			; X/8
	addd	,S++	; (X/8) + (Y/8)*32
	addd	#NAME
	lde	DIR	; if facing left, go to the next tile so you
	cmpe	#12	;  can't stand on your sword
	blt	>
	incd
!
	std	TILENUM
	vdp_read_address
	jsr	vdp_delay
	lda	VDP_VRAM
	jsr	vdp_delay
	ldb	VDP_VRAM
	bitd	#$0101
	rts

coin_song:
	fdb	2500,900
	fdb	1000,1300
	fdb	0
level_song:
	fdb	3000,4000
	fdb	2500,1600
	fdb	1500,2000
	fdb	0
kill_song:
	fdb	2500,1800
	fdb	1500,1400
	fdb	2000,2600
	fdb	0
game_over_song:
	fdb	1,6000
	fdb	3000,8000
	fdb	6000,10000
	fdb	7000,12000
	fdb	9000,32000
	fdb	0
win_song:
	fdb	3000,2000
	fdb	2000,1600
	fdb	1,3000
	fdb	2200,2000
	fdb	1600,1600
	fdb	1,3000
	fdb	1800,2000
	fdb	1400,32000
	fdb	0

patterns:
	fcb	$7F,$FF,$D5,$AA,$D5,$6A,$35,$1A	; 1  left end block
	fcb	$FF,$FF,$55,$AA,$55,$AA,$55,$AA	; 3  middle block
	fcb	$FE,$FF,$55,$AB,$55,$AA,$54,$A8	; 5  right end block
patterns2:
	fcb	$3C,$7E,$FF,$FF,$FF,$FF,$7E,$3C	; 8  coin
	fcb	$3C,$7E,$FF,$C3,$C3,$C3,$FF,$FF	; 10 door 1
	fcb	$FB,$FF,$FF,$FF,$FF,$FF,$FF,$FF	; 12 door 2
	fcb	$00,$72,$56,$2C,$38,$20,$50,$88	; 14 life indicator

patterns_end:
sprite_patterns:
; normal right	4
	fcb	$38,$4C,$44,$38
	fcb	$11,$17,$7B,$15
	fcb	$18,$10,$10,$10
	fcb	$28,$44,$82,$E3
	fcb	$18,$38,$70,$E0
	fcb	$C0,$80,$00,$00
	fcb	$00,$00,$00,$00
	fcb	$00,$00,$00,$80
; swing right	8
	fcb	$38,$4C,$44,$38
	fcb	$10,$10,$7C,$12
	fcb	$11,$13,$10,$10
	fcb	$28,$44,$82,$E3
	fcb	$40,$20,$90,$48
	fcb	$28,$24,$14,$94
	fcb	$80,$C0,$E0,$70
	fcb	$38,$1C,$0C,$80
; normal left	12
	fcb	$18,$1C,$0E,$07
	fcb	$03,$01,$00,$00
	fcb	$00,$00,$00,$00
	fcb	$00,$00,$00,$01
	fcb	$1C,$32,$22,$1C
	fcb	$88,$E8,$DE,$A8
	fcb	$18,$08,$08,$08
	fcb	$14,$22,$41,$C7
; swing left	16
	fcb	$02,$04,$09,$12
	fcb	$14,$24,$28,$29
	fcb	$01,$03,$07,$0E
	fcb	$1C,$38,$30,$01
	fcb	$1C,$32,$22,$1C
	fcb	$08,$08,$3E,$48
	fcb	$88,$C8,$08,$08
	fcb	$14,$22,$41,$C7
; monster		20
	fcb	$3F,$7F,$E8,$C4,$DA,$D8,$C0,$CF
	fcb	$D4,$D1,$CF,$E0,$7F,$3F,$18,$18
	fcb	$FC,$FE,$17,$23,$5B,$1B,$03,$F3
	fcb	$2B,$0B,$F3,$07,$FE,$FC,$18,$18
; dead monster	24
	fcb	$00,$00,$00,$00,$00,$00,$00,$3F
	fcb	$7F,$D4,$C8,$D4,$C0,$CF,$C3,$7F
	fcb	$00,$00,$00,$00,$00,$00,$00,$FC
	fcb	$FE,$2B,$13,$2B,$03,$F3,$C3,$FE
sprite_pattern_end:
sprite_attr:
	fcb	$A7,$10,4,6
sprite_attr_end:

; Platforms: Tile number (2 bytes), length ... end with $FFFF
; Coins: Tile number (2 bytes) ... end with $FFFF
; Monsters: Y, X, patrol length ... end with $FFFF
; Door Tile number (2 bytes)
level:
; Level 1
	fdb	89
	fcb	3
	fdb	158
	fcb	2
	fdb	169
	fcb	4
	fdb	197
	fcb	2
	fdb	238
	fcb	3
	fdb	243
	fcb	3
	fdb	250
	fcb	4
	fdb	291
	fcb	6
	fdb	343
	fcb	3
	fdb	362
	fcb	5
	fdb	464
	fcb	4
	fdb	524
	fcb	4
	fdb	552
	fcb	2
	fdb	646
	fcb	5
	fdb	$FFFF
;
	fdb	259
	fdb	312
	fdb	616
	fdb	$FFFF
;
	fdb	$FFFF
;
	fdb	26
; Level 2
	fdb	373
	fcb	1
	fdb	425
	fcb	5
	fdb	440
	fcb	3
	fdb	453
	fcb	1
	fdb	465
	fcb	3
	fdb	480
	fcb	2
	fdb	547
	fcb	4
	fdb	554
	fcb	1
	fdb	567
	fcb	1
	fdb	571
	fcb	3
	fdb	597
	fcb	1
	fdb	621
	fcb	2
	fdb	657
	fcb	3
	fdb	679
	fcb	4
	fdb	$FFFF
;
	fdb	335
	fdb	540
	fdb	$FFFF
;
	fcb	87,74,20
	fdb	$FFFF
;
	fdb	309
; Level 3
	fdb	90
	fcb	3
	fdb	106
	fcb	13
	fdb	132
	fcb	4
	fdb	192
	fcb	3
	fdb	288
	fcb	10
	fdb	301
	fcb	14
	fdb	381
	fcb	3
	fdb	477
	fcb	3
	fdb	554
	fcb	22
	fdb	582
	fcb	2
	fdb	641
	fcb	4
	fdb	$FFFF
;
	fdb	82
	fdb	203
	fdb	542
	fdb	$FFFF
;
	fcb	7,82,84
	fcb	55,2,60
	fcb	55,106,92
	fcb	119,82,156
	fdb	$FFFF
;
	fdb	27
; Level 4
	fdb	70
	fcb	3
	fdb	99
	fcb	2
	fdb	195
	fcb	2
	fdb	288
	fcb	20
	fdb	311
	fcb	6
	fdb	382
	fcb	2
	fdb	387
	fcb	3
	fdb	395
	fcb	3
	fdb	474
	fcb	6
	fdb	560
	fcb	9
	fdb	666
	fcb	6
	fdb	$FFFF
;
	fdb	356
	fdb	364
	fdb	$FFFF
;
	fcb	55,2,60
	fcb	55,82,60
	fcb	119,130,52
	fdb	$FFFF
;
	fdb	7
; Level 5
	fdb	72
	fcb	8
	fdb	92
	fcb	4
	fdb	164
	fcb	4
	fdb	184
	fcb	4
	fdb	256
	fcb	4
	fdb	284
	fcb	4
	fdb	356
	fcb	4
	fdb	376
	fcb	4
	fdb	448
	fcb	4
	fdb	476
	fcb	4
	fdb	548
	fcb	4
	fdb	560
	fcb	12
	fdb	640
	fcb	4
	fdb	$FFFF
;
	fdb	44
	fdb	419
	fdb	$FFFF
;
	fcb	71,34,12
	fcb	71,194,12
	fcb	119,34,12
	fcb	119,130,76
	fdb	$FFFF
;
	fdb	30
; End
	fdb	$FFFF
	fdb	$FFFF
	fdb	$FFFF
	fdb	$FFFF
