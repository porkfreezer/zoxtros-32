
vdp_read_address	macro
	stb	VDP_REG
	sta	VDP_REG
	endm

vdp_write_address	macro
	stb	VDP_REG
	ora	#$40
	sta	VDP_REG
	endm