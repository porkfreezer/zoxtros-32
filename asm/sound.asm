;  sound.asm
;  This file generates sounds using an HD6340 PTM. One timer is used
;  to generate a square wave on the output, and a second to stop the
;  sound. After starting the sound, the CPU is free.
;
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;


PTM_CR1 = $9000
PTM_CR2 = $9001
PTM_CR3 = $9000
PTM_T1 = $9002
PTM_T2 = $9004
PTM_T3 = $9006
PTM_STATUS = $9001

; Initial timer module
; Uses A,B
sound_init:
	lda	#%01000010	; Interrupt, continuous, enable clock, 16 bit, CR3
	sta	PTM_CR2
	lda	#%10000010	; Output, continuous, enable clock, 16 bit
	sta	PTM_CR3
	lda	#%01000011	; CR1
	sta	PTM_CR2
	lda	#%00000001	; Disable timers
	sta	PTM_CR1
	clrd
	std	SONG
	sta	SOUND_PLAYING
	andcc	#%10111111
	rts

; Start a song from X
song:
	pshs	X
	ldd	,X++
	ldy	,X++
	stx	SONG
	tfr	D,X
	jsr	sound
	puls	X,PC

; Wait for a note to finish
sound_wait:
	tst	SOUND_PLAYING
	bne	sound_wait
	rts

; Start a sound of pulse width X and duration Y
; X = 1 / (frequency * 1us)
; frequency = 1 / (X * 1us)
; Y = duration / 60us
; duration = Y * 60us
sound:
	stx	PTM_T3
	sty	PTM_T2
	pshs	A
	lda	#60
	sta	SOUND_NUM
	sta	SOUND_PLAYING
	clra
	sta	PTM_CR1
	andcc	#%10111111
	puls	A,PC

; Interrupt to stop sound
sound_int:
	pshs	A,B,X,Y
	lda	PTM_STATUS	; Clear interrupt
	lda	PTM_T2
	dec	SOUND_NUM
	bne	@done
	ldx	SONG
	beq	@end
	ldd	,X++
	beq	@end
	ldy	,X++
	stx	SONG
	tfr	D,X
	jsr	sound
	bra	@done
@end:
	lda	#%00000001	; Stop timers
	sta	PTM_CR1
	clrd
	std	SONG
	sta	SOUND_PLAYING
@done:
	puls	A,B,X,Y
	rti
