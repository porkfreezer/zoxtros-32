;  kernel.asm
;  This file is the central file of the Zoxtros 'kernel.'
;  It declares the RAM variables and includes the rest of the files.
;
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;
;

basic=$CB00
platformer=$E400
invaders=$EB00

	org	$0000	; Kernel variables

KEY_BUFFER:		rmb	6	; current state of all keys (0 = pressed)
KEY_STATE:		rmb	1	; positive when new key pressed
KEY:				rmd	1	; newest key
TMPB:				rmb	1	; used in key scan routine
I2C:				rmb	1	; used in key scan routine
JOYSTICK:
JOY_UP:			rmb	1
JOY_DOWN:		rmb	1
JOY_RIGHT:		rmb	1
JOY_LEFT:		rmb	1
JOY_TRIGGER:	rmb	1
JOY_BUTTON:		rmb	1
VDP_CURSOR:		rmd	1	; location of cursor in name table
SOUND_NUM:		rmb	1
SONG:				rmd	1
SOUND_PLAYING:	rmb	1
SAFE_RAM:		rmd	1
I2C_ADDR:		rmb	1
RANDOM:			rmb	1
PROGRAM_START:

;;;;;;;;;;;;;;;

; vectors
	org	$FFF0

	fdb	noint			; Divide by 0 / illegal instruction
	fdb	noint			; SWI3
	fdb	noint			; SWI2
	fdb	sound_int	; FIRQ
	fdb	noint			; IRQ
	fdb	noint			; SWI
	fdb	key_scan		; NMI
	fdb	RES

;;;;;;;;;;;;;;;

	org	$C000

RES:
	ldmd	#1
	clra
	tfr	A,DP
	lds	#$7FFE
	jsr	acia_init
	ldx	#vt100_clear
	jsr	acia_print
	ldx	#text_vdp_init
	jsr	vdp_init
	jsr	text_load_name
	jsr	text_load_patterns
	lda	#%10010000
	jsr	vdp_enable
	jsr	via_init
	jsr	sound_init
	tst	RANDOM
	bne	>
	inc	RANDOM
!
	ldx	#@choices
	jsr	text_print
	lde	#$FF
	clra
	jsr	text_select
@select:
	tst	KEY_STATE
	bmi	@select
	ldb	KEY
	ste	KEY_STATE
	cmpb	#10
	beq	>
	subb	#'1'
	blt	@select
	cmpb	#4
	bgt	@select
	jsr	text_select
	tfr	B,A
	jsr	text_select
	bra	@select
!
	pshs	A
	tsta
	lbeq	basic
	deca
	lbeq	platformer
	deca
	lbeq	invaders
	ldb	#%10100000
	ldx	#ROM_TYPE
	deca
	beq	>
	orb	#%00000110
!
	stb	I2C_ADDR
	jsr	eeprom_read
	lbeq	eeprom_load
	puls	A
	bra	@select
@choices:
	fcn	'1 BASIC\n2 Platformer\n3 Space Invaders\n4 Slot 1\n5 Slot 2\n'

random:
	lda	RANDOM
	lsra
	bcc	>
	eora	#$B8
!
	sta	RANDOM
	rts

; unused interrupts
noint:
	rti

	include	video.asm
	include	via.asm
	include	i2c.asm
	include	acia.asm
	include	keyboard.asm
	include	sound.asm
