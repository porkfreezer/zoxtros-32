;  acia.asm
;  This file contains some basic routines for an R65C51 ACIA (uart)
;  
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;  
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;  
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;  
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;  
; 

; register addresses
ACIA_TX = $B800
ACIA_RX = $B800
ACIA_RES = $B801
ACIA_STATUS = $B801
ACIA_COMM = $B802
ACIA_CTRL = $B803

; Initialize ACIA registers
; Uses A
acia_init:
	sta	ACIA_RES
	lda	#%00001011
	sta	ACIA_COMM
	lda	#%00011111
	sta	ACIA_CTRL
	rts

; Print string pointed to by X
; Uses A,X
acia_print:
@loop
	lda	,X+
	beq	@done
	bsr	acia_putc
	bra	@loop
@done:
	rts

; Send A
acia_putc:
	pshs	A
@waittx:
	lda	ACIA_STATUS
	anda	#%00010000
	beq	@waittx
	puls	A
	sta	ACIA_TX
	rts

; Wait for a line of input, store at X
; Uses A,B,X
; Returns string stored at original X
acia_getline:
	clrb
@loop:
	bsr	acia_getc
	; Check for enter (CR or LF)
	cmpa	#$0A
	beq	@done
	cmpa	#$0D
	beq	@done
	; Check for delete
	cmpa	#$7F
	bne	@continue
	cmpb	#$00
	beq	@loop
	decb
	leax	-1,X
	pshs	X
	ldx	#vt100_left
	jsr	acia_print
	lda	#' '
	jsr	acia_putc
	ldx	#vt100_left
	jsr	acia_print
	puls	X
	bra @loop
@continue:
	; Otherwise must be printable
	cmpa	#$20
	blt	@loop
	cmpa	#$7F
	bgt	@loop
	; All good, print and store
	incb
	tstb
	beq	@done
	bsr	acia_putc
	sta	,X+
	bra	@loop
@done:
	clra
	sta	,X+
	bsr	acia_crlf
	rts

; Wait for a character, store in A
; Uses A
acia_getc:
	lda	ACIA_STATUS
	anda	#%00001000
	beq	acia_getc
	lda	ACIA_RX
	rts

; Next line
; Uses A
acia_crlf:
	lda	#$0D ; CR
	bsr	acia_putc
	lda	#$0A ; LF
	bsr	acia_putc
	rts

; Escape codes
vt100_clear: ; also homes cursor
	fcb	27,'[','2','J',27,'[','H',0
vt100_left:
	fcb	27,'[','1','D',0
vt100_right:
	fcb	27,'[','1','C',0
