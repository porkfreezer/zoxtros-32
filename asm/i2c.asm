;  i2c.asm
;  I²C EEPROM using A0 and A1 of the R65C22 VIA
;  also see via.asm
;  
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;  
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;  
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;  
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;  
;  

SET_SCL = %11111110
SET_SDA = %11111101
CLR_SCL = %00000001
CLR_SDA = %00000010

ROM_LEN = $0001
ROM_TYPE = $0000

; Types:
; 00    Machine code single-file cartridge
; 

; Wait 5.5µs
i2c_delay:			; 7
	rts				; 4

; Start condition
i2c_start:
	pshs	A
;	lda	#'S'
;	jsr	text_putc
	lda	VIA_DDRA
	anda	#SET_SDA
	sta	VIA_DDRA
	jsr	i2c_delay
	anda	#SET_SCL
	sta	VIA_DDRA
	jsr	i2c_delay
	ora	#CLR_SDA
	sta	VIA_DDRA
	jsr	i2c_delay
	ora	#CLR_SCL
	sta	VIA_DDRA
	jsr	i2c_delay
	puls	A,PC

; Stop condition
i2c_stop:
	pshs	A
;	lda	#'P'
;	jsr	text_putc
	lda	VIA_DDRA
	ora	#CLR_SDA
	sta	VIA_DDRA
	jsr	i2c_delay
	anda	#SET_SCL
	sta	VIA_DDRA
	jsr	i2c_delay
	anda	#SET_SDA
	sta	VIA_DDRA
	jsr	i2c_delay
	puls	A,PC

; Write MSB of A to bus
i2c_write_bit:
	pshs	A,B
	rola	; move to 2 bit
	rola
	rola
;	pshs	A
;	anda	#%00000010
;	lsra
;	adda	#'0'
;	jsr	text_putc
;	puls	A
	coma
	anda	#%00000010
	ldb	VIA_DDRA
	andb	#%11111101
	orr	B,A
	sta	VIA_DDRA
	jsr	i2c_delay
	anda	#SET_SCL
	sta	VIA_DDRA
	jsr	i2c_delay
	ora	#CLR_SCL
	sta	VIA_DDRA
	puls	A,B,PC

; Write A to bus without start or stop
; Carry is set if NACK
i2c_write:
	pshs	A,B
	ldb	#8
@loop:
	jsr	i2c_write_bit
	lsla
	decb
	bne	@loop
	jsr	i2c_read_bit
	puls	A,B,PC

; Read bit into Carry
i2c_read_bit:
	pshs	A
	lda	VIA_DDRA
	anda	#SET_SDA
	sta	VIA_DDRA
	jsr	i2c_delay
	anda	#SET_SCL
	sta	VIA_DDRA
	jsr	i2c_delay
	lda	VIA_PORTA
	rora
;	pshs	A
;	anda	#1
;	adda	#'0'
;	jsr	text_putc
;	puls	A
	rora
	lda	VIA_DDRA
	ora	#CLR_SCL
	sta	VIA_DDRA
	puls	A,PC

; Read byte into A
; Sends MSB of B as NACK
i2c_read:
	pshs	B
	ldb	#8
	clra
@loop:
	jsr	i2c_read_bit
	rola
	decb
	bne	@loop
	puls	B
	exg	B,A
	jsr	i2c_write_bit
	exg	A,B
	rts

; Read byte X into A
eeprom_read:
	pshs	B
	jsr	i2c_start
	lda	I2C_ADDR
	jsr	i2c_write
;	lda	#' '
;	jsr	text_putc
	tfr	X,D
	jsr	i2c_write
;	lda	#' '
;	jsr	text_putc
	tfr	B,A
	jsr	i2c_write
	jsr	i2c_start
	lda	I2C_ADDR
	inca
	jsr	i2c_write
	ldb	#%10000000
	jsr	i2c_read
	jsr	i2c_stop
	tsta
	puls	B,PC

; Load a machine-language program using the EEPROM like a cartridge,
;   rather than a cassette like BASIC does
eeprom_load:
	ldx	#ROM_LEN+1		; length low
	jsr	eeprom_read
	tfr	A,B
	leax	-1,X				; length high
	jsr	eeprom_read
	std	PROGRAM_START	; save length
	ldx	#PROGRAM_START
	leax	D,X				; X = #PROGRAM_START + ROM_LEN 
	stx	SAFE_RAM
	ldx	#@loading
	jsr	text_print
	ldx	#ROM_LEN+2		; Name
	ldb	#8					; Name length
@nameloop:
	jsr	eeprom_read
	beq	>
	jsr	text_putc
!
	leax	1,X
	decb
	bne	@nameloop
@namedone:
	jsr	text_crlf
	ldw	PROGRAM_START	; length
	ldy	#PROGRAM_START
!
	jsr	eeprom_read
	sta	,Y+
	leax	1,X
	decw
	bne	<
;
	jmp	PROGRAM_START	
@loading:
	fcn	"Loading "

; Write A to X
eeprom_write:
	pshs	A,B
	pshs	A
	jsr	i2c_start
	lda	I2C_ADDR
	jsr	i2c_write
	tfr	X,D
	jsr	i2c_write
	tfr	B,A
	jsr	i2c_write
	puls	A
	jsr	i2c_write
	jsr	i2c_stop
	puls	A,B,PC
