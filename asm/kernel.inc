ACIA_COMM EQU $B802
acia_crlf EQU $C8AF
ACIA_CTRL EQU $B803
acia_getc EQU $C8A4
acia_getline EQU $C85F
acia_init EQU $C839
acia_print EQU $C847
acia_putc EQU $C850
ACIA_RES EQU $B801
ACIA_RX EQU $B800
ACIA_STATUS EQU $B801
ACIA_TX EQU $B800
basic EQU $CB00
BLACK EQU $0001
CLR_SCL EQU $0001
CLR_SDA EQU $0002
CYAN EQU $0007
DRK_BLUE EQU $0004
DRK_GREEN EQU $000C
DRK_RED EQU $0006
DRK_YELLOW EQU $000A
eeprom_load EQU $C7CB
eeprom_read EQU $C7A3
eeprom_write EQU $C819
g1_vdp_init EQU $C1E5
GRAY EQU $000E
I2C EQU $000A
I2C_ADDR EQU $0019
i2c_delay EQU $C6ED
i2c_read EQU $C78D
i2c_read_bit EQU $C769
i2c_start EQU $C6EE
i2c_stop EQU $C715
i2c_write EQU $C759
i2c_write_bit EQU $C734
invaders EQU $EB00
JOY_BUTTON EQU $0010
JOY_DOWN EQU $000C
JOY_LEFT EQU $000E
JOY_RIGHT EQU $000D
JOY_TRIGGER EQU $000F
JOY_UP EQU $000B
JOYSTICK EQU $000B
KEY EQU $0007
KEY_BUFFER EQU $0000
key_getkey EQU $C8CA
key_getline EQU $C8D6
key_scan EQU $C947
KEY_STATE EQU $0006
keys EQU $C9D0
LT_BLUE EQU $0005
LT_GREEN EQU $0003
LT_RED EQU $0009
LT_YELLOW EQU $000B
MAGENTA EQU $000D
MED_GREEN EQU $0002
MED_RED EQU $0008
noint EQU $C0C7
platformer EQU $E400
PROGRAM_START EQU $001B
PTM_CR1 EQU $9000
PTM_CR2 EQU $9001
PTM_CR3 EQU $9000
PTM_STATUS EQU $9001
PTM_T1 EQU $9002
PTM_T2 EQU $9004
PTM_T3 EQU $9006
RANDOM EQU $001A
random EQU $C0BD
RES EQU $C000
ROM_LEN EQU $0001
ROM_TYPE EQU $0000
SAFE_RAM EQU $0017
SET_SCL EQU $00FE
SET_SDA EQU $00FD
SONG EQU $0014
song EQU $CA8D
sound EQU $CAA2
sound_init EQU $CA70
sound_int EQU $CAB9
SOUND_NUM EQU $0013
SOUND_PLAYING EQU $0016
sound_wait EQU $CA9D
text_backspace EQU $C128
text_crlf EQU $C184
text_end_patterns EQU $C6C2
text_load_name EQU $C223
text_load_patterns EQU $C247
text_patterns EQU $C2BA
text_print EQU $C144
text_putc EQU $C150
text_scroll EQU $C0CD
text_select EQU $C27F
text_vdp_init EQU $C1DD
TMPB EQU $0009
vdp_clear_vram EQU $C1ED
vdp_copy_vram EQU $C202
VDP_CURSOR EQU $0011
vdp_delay EQU $C0C8
vdp_enable EQU $C218
vdp_init EQU $C1CD
VDP_REG EQU $8801
VDP_VRAM EQU $8800
VIA_ACR EQU $800B
VIA_DDRA EQU $8003
VIA_DDRB EQU $8002
VIA_IER EQU $800E
VIA_IFR EQU $800D
via_init EQU $C6C2
VIA_PCR EQU $800C
VIA_PORTA EQU $8001
VIA_PORTB EQU $8000
VIA_SR EQU $800A
VIA_T1CH EQU $8005
VIA_T1CL EQU $8004
VIA_T1LH EQU $8007
VIA_T1LL EQU $8006
VIA_T2CH EQU $8009
VIA_T2CL EQU $8008
vt100_clear EQU $C8B8
vt100_left EQU $C8C0
vt100_right EQU $C8C5
WHITE EQU $000F
