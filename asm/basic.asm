;
; Modified from Color Basic Unravelled to assemble and work with my Zoxtros hardware
;
;

	include	vdp_macros.asm
	include	kernel.inc

	org PROGRAM_START	; RAM vars
SKP		equ	$21
SKP2		equ	$8C
SKP1LD	equ	$86
LBUFMX	equ	250
STKBUF	equ	58

ENDFLG:	rmb	1
CHARAC:	rmb	1
ENDCHR:	rmb	1
TMPLOC:	rmb	1
IFCTR:	rmb	1
DIMFLG:	rmb	1
VALTYP:	rmb	1
GARBFL:	rmb	1
ARYDIS:	rmb	1
INPFLG:	rmb	1
RELFLG:	rmb	1
TEMPPT:	rmb	2
LASTPT:	rmb	2
TEMPTR:	rmb	2
TMPTR1:	rmb	2

FPA2:		rmb	4
BOTSTK:	rmb	2
TXTTAB:	rmb	2
VARTAB:	rmb	2
ARYTAB:	rmb	2
ARYEND:	rmb	2
FRETOP:	rmb	2
STRTAB:	rmb	2
FRESPC:	rmb	2
MEMSIZ:	rmb	2
OLDTXT:	rmb	2
BINVAL:	rmb	2
OLDPTR:	rmb	2
TINPTR:	rmb	2
DATTXT:	rmb	2
DATPTR:	rmb	2
DATTMP:	rmb	2
VARNAM:	rmb	2
VARPTR:	rmb	2
VARDES:	rmb	2
RELPTR:	rmb	2
TRELFL:	rmb	2

V40:		rmb	1
V41:		rmb	1
V42:		rmb	1
V43:		rmb	1
V44:		rmb	1

V45:		rmb	1
V46:		rmb	1
V47:		rmb	1
V48:		rmb	2

V4A:		rmb	1
V4B:		rmb	2
V4D:		rmb	2

FP0EXP:	rmb	1
FPA0:		rmb	4
FP0SGN:	rmb	1
COEFCT:	rmb	1
STRDES:	rmb	1
FPCARY:	rmb	1

FP1EXP:	rmb	1
FPA1:		rmb	4
FP1SGN:	rmb	1

RESSGN:	rmb	1
FPSBYT:	rmb	1
COEFPT:	rmb	2
LSTTXT:	rmb	2
CURLIN:	rmb	2

CINBFL:	rmb	1
TOPRAM:	rmb	2
CINCTR:	rmb	1
CINPTR:	rmb	2
IKEYIM:	rmb	1

EXECJP:	rmb	2
GETNCH:	inc	<CHARAD+1
			bne	GETCCH
			inc	<CHARAD
GETCCH:	fcb	$B6
CHARAD:	rmb	2
			jmp	BROMHK

VAB:		rmb	1
VAC:		rmb	1
VAD:		rmb	1
VAE:		rmb	1

VCB:		rmb	2
VCD:		rmb	2
VCF:		rmb	2
VD1:		rmb	2
VD3:		rmb	1
VD4:		rmb	1
VD5:		rmb	1
VD6:		rmb	1
VD7:		rmb	1
VD8:		rmb	1
VD9:		rmb	1
VDA:		rmb	1

USRJMP:	rmb	3
RVSEED:	rmb	1
			rmb	4
EXPJMP:	rmb	3

COMVEC:	rmb	10

STRSTK:	rmb	8*5
LINHDR:	rmb	2
LINBUF:	rmb	LBUFMX+1
STRBUF:	rmb	41


			org	basic

			jmp	BACDST

; Some stuff from Extended Basic
L880E:	clr	FPA0
			clr	FPA0+1
			clr	VALTYP
			clr	FPSBYT
			clr	FP0SGN
			ldb	#$A0
			stb	FP0EXP
			jmp	LBA1C

L89B4:	lda	,U+
			sta	,X+
L89B8:	cmpu	VARTAB
			bne	L89B4
			stx	VARTAB
L89BF:	rts

L89FC:	jsr	LAF67
			ldx	BINVAL
			rts

L8A02:	ldx	VD1
L8A04:	stx	BINVAL
			jmp	LAD01

RENUM:	jsr	LAD26
			ldd	#10
			std	VD5
			std	VCF
			clrb
			std	VD1
			jsr	GETCCH
			bcc	L8A20
			bsr	L89FC
			stx	VD5
			jsr	GETCCH
L8A20:	beq	L8A3D
			jsr	SYNCOMMA
			bcc	L8A2D
			bsr	L89FC
			stx	VD1
			jsr	GETCCH
L8A2D:	beq	L8A3D
			jsr	SYNCOMMA
			bcc	L8A3A
			bsr	L89FC
			stx	VCF
			beq	L8A83
L8A3A:	jsr	LA5C7
L8A3D:	bsr	L8A02
			stx	VD3
			ldx	VD5
			bsr	L8A04
			cmpx	VD3
			blo	L8A83
			bsr	L8A67
			jsr	L8ADD
			jsr	LACEF
			bsr	L8A02
			stx	VD3
			bsr	L8A91
			bsr	L8A68
			bsr	L8A91
			jsr	L8B7B
			jsr	LAD26
			jsr	LACEF
			jmp	LAC73
L8A67:	fcb	SKP1LD
L8A68:	clra
			sta	VD8
			ldx	VD3
			ldd	VD5
			bsr	L8A86
L8A71:	tst	VD8
			bne	L8A77
			std	2,X
L8A77:	ldx	,X
			bsr	L8A86
			addd	VCF
			blo	L8A83
			cmpa	#$FA
			blo	L8A71
L8A83:	jmp	LB44A


L8A86:	pshs	B,A
			ldd	,X
			puls	A,B
			bne	L8A90
			leas	2,S
L8A90:	rts
L8A91:	ldx	TXTTAB
			leax	-1,X
L8A95:	leax	1,X
			bsr	L8A86
L8A99:	leax	3,X
L8A9B:	leax	1,X
			lda	,X
			beq	L8A95
			stx	TEMPTR
			deca
			beq	L8AB2
			deca
			beq	L8AD3
			deca
			bne	L8A9B
L8AAC:	lda	#3
			sta	,X+
			bra	L8A99
L8AB2:	ldd	1,X
			dec	2,X
			beq	L8AB9
			clra
L8AB9:	ldb	3,X
			dec	4,X
			beq	L8AC0
			clrb
L8AC0:	std	1,X
			std	BINVAL
			jsr	LAD01
L8AC7:	ldx	TEMPTR
			blo	L8AAC
			ldd	V47
			inc	,X+
			std	,X
			bra	L8A99
L8AD3:	clr	,X
			ldx	1,X
			ldx	2,X
			stx	V47
			bra	L8AC7
L8ADD:	ldx	TXTTAB
			bra	L8AE5
L8AE1:	ldx	CHARAD
			leax	1,X
L8AE5:	bsr	L8A86
			leax	2,X
L8AE9:	leax	1,X
L8AEB:	stx	CHARAD
L8AED:	jsr	GETNCH
L8AEF:	tsta
			beq	L8AE1
			bpl	L8AED
			ldx	CHARAD
			cmpa	#$FF
			beq	L8AE9
			cmpa	#TOKTHN
			beq	L8B13
			cmpa	#TOKELS
			beq	L8B13
			cmpa	#TOKGO
			bne	L8AED
			jsr	GETNCH
			cmpa	#TOKTO
			beq	L8B13
			cmpa	#TOKSUB
			bne	L8AEB
L8B13:	jsr	GETNCH
			blo	L8B1B
L8B17:	jsr	GETCCH
			bra	L8AEF
L8B1B:	ldx	CHARAD
			pshs	X
			jsr	LAF67
			ldx	CHARAD
L8B24:	lda	,-X
			jsr	L90AA
			blo	L8B24
			leax	1,X
			tfr	X,D
			subb	1,S
			subb	#5
			beq	L8B55
			blo	L8B41
			leau	,X
			negb
			leax	B,X
			jsr	L89B8
			bra	L8B55
L8B41:	stx	V47
			ldx	VARTAB
			stx	V43
			negb
			leax	B,X
			stx	V41
			stx	VARTAB
			jsr	LAC1E
			ldx	V45
			stx	CHARAD
L8B55:	puls	X
			lda	#1
			sta	,X
			sta	2,X
			sta	4,X
			ldb	BINVAL
			bne	L8B67
			ldb	#1
			inc	2,X
L8B67:	stb	1,X
			ldb	BINVAL+1
			bne	L8B71
			ldb	#1
			inc	4,X
L8B71:	stb	3,X
			jsr	GETCCH
			cmpa	#','
			beq	L8B13
			bra	L8B17
L8B7B:	ldx	TXTTAB
			leax	-1,X
L8B7F:	leax	1,X
			ldd	2,X
			std	CURLIN
			jsr	L8A86
			leax	3,X
L8B8A:	leax	1,X
L8B8C:	lda	,X
			beq	L8B7F
			deca
			beq	L8BAE
			suba	#2
			bne	L8B8A
			pshs	X
			ldx	#L8BD9-1
			jsr	text_print
			ldx	,S
			ldd	1,X
			jsr	LBDCC
			jsr	LBDC5
			jsr	LB958
			puls	X
L8BAE:	pshs	X
			ldd	1,X
			std	FPA0+2
			jsr	L880E
			jsr	LBDD9
			puls	U
			ldb	#5
L8BBE:	leax	1,X
			lda	,X
			beq	L8BC9
			decb
			sta	,U+
			bra	L8BBE
L8BC9:	leax	,U
			tstb
			beq	L8B8C
			leay	,U
			leau	B,U
			jsr	L89B8
			leax	,Y
			bra	L8B8C
L8BD9:	fcn	'UL '

L90AA:	cmpa	#'0'
			blo	L90B2
			suba	#'9'+1
			suba	#-('9'+1)
L90B2:	rts

BACDST:
			ldx	#$0400
@l1:		clr	,-X
			cmpx	#I2C
			bne	@l1
			jsr	text_load_name
			ldx	#$400+511
			clr	,X+
			clr	,X
			stx	TXTTAB
@l2:		ldx	#$7FFF
@l3:		stx	TOPRAM
			stx	MEMSIZ
			stx	STRTAB
			leax	-200,X
			stx	FRETOP
			tfr	X,S
			ldx	#vars_rom
			ldu	#EXECJP
			ldb	#14
			jsr	LA59A
			ldx	#vars_rom2
			ldu	#USRJMP
			ldb	#21
			jsr	LA59A
			lda	#$39
			sta	LINHDR-1
			jsr	LAD19
			ldx	#copyright
			jsr	text_print
			jmp	LAC73

vars_rom:
			fdb	LB277
			inc	CHARAD+1
			bne	@l1
			inc	CHARAD
@l1:		lda	>0000
			jmp	BROMHK
vars_rom2:
			jmp	LB44A
			fcb	$80
			fdb	$4FC7
			fdb	$5259
			jmp	LB277
LA13D:	fcb	46
LA13E:	fdb	LAA66
LA140:	fdb	LAB67
LA142:	fcb	19
LA143:	fdb	LAB1A
LA145:	fdb	LAA29


copyright:
			fcn	'COLOR BASIC 1.2\n(C) 1982 TANDY\n'
microsoft:
			fcn	'MICROSOFT\n'

LA390:	ldx	#LINBUF+1
			jsr	key_getline
			pshs	CC
			ldx	#LINBUF
			puls	CC
			rts

EXEC:		beq	LA545
			jsr	LB73D
			stx	EXECJP
LA545:	jmp	[EXECJP]

LA554:	jsr	LB3E4
			cmpd	#959
			lbhi	LB44A
			tfr	D,W
			ldd	VDP_CURSOR
			vdp_write_address
			lda	#' '
			sta	VDP_VRAM
			stw	VDP_CURSOR
			rts

INKEY:	lda	IKEYIM
			bne	LA56B
			tst	KEY_STATE
			bmi	LA56B
			lda	KEY
			ldb	#$FF
			stb	KEY_STATE
LA56B:	clr	IKEYIM
			sta	FPA0+3
			lbne	LB68F
			sta	STRDES
			jmp	LB69B

LA59A:	lda	,X+
			sta	,U+
			decb
			bne	LA59A
LA5A1:	rts

LA5C7:	jsr	GETCCH
LA5C9:	beq	LA5A1
			jmp	LB277

EOF:		rts	;	A5CE

SET:		bsr	LA8C1
			jsr	LB738
			cmpb	#2
			bhi	>
			addb	#7
!
			pshs	B
			bsr	LA90D
			puls	B
			stb	VDP_VRAM
			rts

RESET:	bsr	LA8C1
			bsr	LA90D
			ldb	#7
			stb	VDP_VRAM
			rts

LA8C1:	jsr	LB26A
			jsr	LB70B
			cmpb	#39
			bhi	LA8D5
			pshs	B
			jsr	LB738
			cmpb	#23
LA8D5:	bhi	LA948
			lda	#40
			mul
			addb	,S+
			adca	#0
			vdp_write_address
			rts

LA90D:	jmp	LB267

CLS:		beq	LA928
			jsr	LB70B
			stb	VDP_REG
			ldb	#$87
			stb	VDP_REG
LA928:	jsr	text_load_name
			ldx	#microsoft
			jmp	text_print

LA93F:	jsr	LB26D
LA942:	jsr	LB70B
			tstb
			bne	LA984
LA948:	jmp	LB44A

SOUND:	pshs	X,Y
			jsr	LB141
			jsr	LB3E9
			pshs	D
			jsr	LB26D
			jsr	LB141
			jsr	LB3E9
			tfr	D,Y
			puls	X
			jsr	sound
			jsr	sound_wait
			puls	X,Y
LA984:	rts

JOYSTK:	jsr	LB70E
			cmpb	#1
			lbhi	LB44A
			beq	>
			subb	JOY_LEFT
			addb	JOY_RIGHT
			incb
			bra	@l1
!			subb	JOY_UP
			addb	JOY_DOWN
@l1:		lslb
			lslb
			lslb
			lslb
			lslb
			decb
			bpl	>
			incb
!			jmp	LB4F3

BROMHK:	cmpa	#'9'+1
			bhs	LAA28
			cmpa	#' '
			bne	LAA24
			jmp	GETNCH
LAA24:	suba	#'0'
			suba	#-'0'
LAA28:	rts

LAA29:	FDB	SGN
			FDB	INT
			FDB	ABS
			FDB	USRJMP
			FDB	RND
			FDB	SIN
			FDB	PEEK
			FDB	LEN
			FDB	STR
			FDB	VAL
			FDB	ASC
			FDB	CHR
			FDB	EOF
			FDB	JOYSTK
			FDB	LEFT
			FDB	RIGHT
			FDB	MID
			FDB	INKEY
			FDB	MEM

LAA51:	FCB	$79
			FDB	LB9C5	;	+
			FCB	$79
			FDB	LB9BC	;	-
			FCB	$7B
			FDB	LBACC	;	*
			FCB	$7B
			FDB	LBB91	;	/
			FCB	$7F
			FDB	LB277	;	^ (error)
			FCB	$50
			FDB	LB2D5	;	AND
			FCB	$46
			FDB	LB2D4	;	OR

TOKGO = $81
TOKREM = $82
TOKELS = $84
TOKIF = $85
TOKDAT = $86
TOKPRT = $87
TOKTO = $9E
TOKSUB = $9F
TOKTHN = $A0
TOKNOT = $A1
TOKSTP = $A2
TOKPL = $A3
TOKMI = $A4
TOKGT = $AB
TOKEQ = $AC

TOKMXE = $9C
TOKMX = $AD
NUMSEC = 18

LAA66:	FCB	'F','O','R'+$80							;	$80
			FCB	'G','O'+$80									;	$81
			FCB	'R','E','M'+$80							;	$82
			FCB	'''+$80										;	$83
			FCB	'E','L','S','E'+$80						;	$84
			FCB	'I','F'+$80									;	$85
			FCB	'D','A','T','A'+$80						;	$86
			FCB	'P','R','I','N','T'+$80					;	$87
			FCB	'O','N'+$80									;	$88
			FCB	'I','N','P','U','T'+$80					;	$89
			FCB	'E','N','D'+$80							;	$8A
			FCB	'N','E','X','T'+$80						;	$8B
			FCB	'D','I','M'+$80							;	$8C
			FCB	'R','E','A','D'+$80						;	$8D
			FCB	'R','U','N'+$80							;	$8E
			FCB	'R','E','S','T','O','R','E'+$80		;	$8F
			FCB	'R','E','T','U','R','N'+$80			;	$90
			FCB	'S','T','O','P'+$80						;	$91
			FCB	'P','O','K','E'+$80						;	$92
			FCB	'C','O','N','T'+$80						;	$93
			FCB	'L','I','S','T'+$80						;	$94
			FCB	'S','E','T'+$80							;	$95
			FCB	'R','E','S','E','T'+$80					;	$96
			FCB	'C','L','E','A','R'+$80					;	$97
			FCB	'N','E','W'+$80							;	$98
			FCB	'C','L','S'+$80							;	$99
			FCB	'S','O','U','N','D'+$80					;	$9A
			FCB	'E','X','E','C'+$80						;	$9B
			FCB	'R','E','N','U','M'+$80					;	$9C
			FCB	'T','A','B','('+$80						;	$9D
			FCB	'T','O'+$80									;	$9E
			FCB	'S','U','B'+$80							;	$9F
			FCB	'T','H','E','N'+$80						;	$A0
			FCB	'N','O','T'+$80							;	$A1
			FCB	'S','T','E','P'+$80						;	$A2
			FCB	'O','F','F'+$80							;	$A3
			FCB	'+'+$80										;	$A4
			FCB	'-'+$80										;	$A5
			FCB	'*'+$80										;	$A6
			FCB	'/'+$80										;	$A7
			FCB	'^'+$80										;	$A8
			FCB	'A','N','D'+$80							;	$A9
			FCB	'O','R'+$80									;	$AA
			FCB	'>'+$80										;	$AB
			FCB	'='+$80										;	$AC
			FCB	'<'+$80										;	$AD

LAB1A:	FCB	'S','G','N'+$80							;	$80
			FCB	'I','N','T'+$80							;	$81
			FCB	'A','B','S'+$80							;	$82
			FCB	'U','S','R'+$80							;	$83
			FCB	'R','N','D'+$80							;	$84
			FCB	'S','I','N'+$80							;	$85
			FCB	'P','E','E','K'+$80						;	$86
			FCB	'L','E','N'+$80							;	$87
			FCB	'S','T','R','$'+$80						;	$88
			FCB	'V','A','L'+$80							;	$89
			FCB	'A','S','C'+$80							;	$8A
			FCB	'C','H','R','$'+$80						;	$8B
			FCB	'E','O','F'+$80							;	$8C
			FCB	'J','O','Y','S','T','K'+$80			;	$8D
			FCB	'L','E','F','T','$'+$80					;	$8E
			FCB	'R','I','G','H','T','$'+$80			;	$8F
			FCB	'M','I','D','$'+$80						;	$90
			FCB	'I','N','K','E','Y','$'+$80			;	$91
			FCB	'M','E','M'+$80							;	$92

LAB67:	FDB	FOR
			FDB	GO
			FDB	REM
			FDB	REM
			FDB	REM
			FDB	IF
			FDB	DATA
			FDB	PRINT
			FDB	ON
			FDB	INPUT
			FDB	END
			FDB	NEXT
			FDB	DIM
			FDB	READ
			FDB	RUN
			FDB	RESTOR
			FDB	RETURN
			FDB	STOP
			FDB	POKE
			FDB	CONT
			FDB	LIST
			FDB	SET
			FDB	RESET
			FDB	CLEAR
			FDB	NEW
			FDB	CLS
			FDB	SOUND
			FDB	EXEC
			FDB	RENUM


LABAF:
			FCC	'NF'
			FCC	'SN'
			FCC	'RG'
			FCC	'OD'
			FCC	'FC'
			FCC	'OV'
			FCC	'OM'
			FCC	'UL'
			FCC	'BS'
			FCC	'DD'
			FCC	'/0'
			FCC	'ID'
			FCC	'TM'
			FCC	'OS'
			FCC	'LS'
			FCC	'ST'
			FCC	'CN'
			FCC	'FD'
			FCC	'AO'
			FCC	'DN'
			FCC	'IO'
			FCC	'FM'
			FCC	'NO'
			FCC	'IE'
			FCC	'DS'

LABE1:	fcn	' ERROR'
LABE8:	fcn	' IN '
LABED:	fcc	'\n'
LABEE:	fcn	'OK\n'
LABF2:	fcn	'\nBREAK'

LABF9:	leax	4,S
LABFB:	ldb	#18
			stx	TEMPTR
			lda	,X
			suba	#$80
			bne	LAC1A
			ldx	1,X
			stx	TMPTR1
			ldx	VARDES
			beq	LAC16
			cmpx	TMPTR1
			beq	LAC1A
			ldx	TEMPTR
			abx
			bra	LABFB
LAC16:	ldx	TMPTR1
			stx	VARDES
LAC1A:	ldx	TEMPTR
			tsta
			rts

LAC1E:	bsr	LAC37
LAC20:	ldu	V41
			leau	1,U
			ldx	V43
			leax	1,X
LAC28:	lda	,-X
			pshu	A
			cmpx	V47
			bne	LAC28
			stu	V45
LAC32:	rts

LAC33:	clra
			aslb
			addd	ARYEND
LAC37:	addd	#STKBUF
			bcs	LAC44
			sts	BOTSTK
			cmpd	BOTSTK
			bcs	LAC32
LAC44:	ldb	#6*2

LAC46:	jsr	LAD33
			jsr	LB95C
			jsr	LB9AF
			ldx	#LABAF
			abx
			bsr	LACA0
			bsr	LACA0
			ldx	#LABE1
LAC68:	jsr	text_print
			lda	CURLIN
			inca
			beq	LAC73
			jsr	LBDC5

LAC73:	jsr	LB95C
			ldx	#LABEE
			jsr	text_print
LAC7C:	jsr	LA390
			ldu	#$FFFF
			stu	CURLIN
			bcs	LAC7C
			stx	CHARAD
			jsr	GETNCH
			beq	LAC7C
			bcs	LACA5
			jsr	LB821
			jmp	LADC0

LACA0:	lda	,X+
			jmp	text_putc

LACA5:	jsr	LAF67
			ldx	BINVAL
			stx	LINHDR
			jsr	LB821
			stb	TMPLOC
			bsr	LAD01
			bcs	LACC8
			ldd	V47
			subd	,X
			addd	VARTAB
			std	VARTAB
			ldu	,X
LACC0:	pulu	A
			sta	,X+
			cmpx	VARTAB
			bne	LACC0
LACC8:	lda	LINBUF
			beq	LACE9
			ldd	VARTAB
			std	V43
			addb	TMPLOC
			adca	#0
			std	V41
			jsr	LAC1E
			ldu	#LINHDR-2
LACDD:	pulu	A
			sta	,X+
			cmpx	V45
			bne	LACDD
			ldx	V41
			stx	VARTAB
LACE9:	bsr	LAD21
			bsr	LACEF
			bra	LAC7C

LACEF:	ldx	TXTTAB
LACF1:	ldd	,X
			beq	LAD16
			leau	4,X
LACF7:	lda	,U+
			bne	LACF7
			stu	,X
			ldx	,X
			bra	LACF1

LAD01:	ldd	BINVAL
			ldx	TXTTAB
LAD05:	ldu	,X
			beq	LAD12
			cmpd	2,X
			bls	LAD14
			ldx	,X
			bra	LAD05
LAD12:	orcc	#1
LAD14:	stx	V47
LAD16:	RTS

NEW:		bne	LAD14
LAD19:	ldx	TXTTAB
			clr	,X+
			clr	,X+
			stx	VARTAB
LAD21:	ldx	TXTTAB
			jsr	LAEBB
LAD26:	ldx	MEMSIZ
			stx	STRTAB
			jsr	RESTOR
			ldx	VARTAB
			stx	ARYTAB
			stx	ARYEND
LAD33:	ldx	#STRSTK
			stx	TEMPPT
			ldx	,S
			lds	FRETOP
			clr	,-S
			clr	OLDPTR
			clr	OLDPTR+1
			clr	ARYDIS
			jmp	,X

FOR:		lda	#$80
			sta	ARYDIS
			jsr	LET
			jsr	LABF9
			leas	2,S
			bne	LAD59
			ldx	TEMPTR
			leas	B,X
LAD59:	ldb	#$09
			jsr	LAC33
			jsr	LAEE8
			ldd	CURLIN
			pshs	X,B,A
			ldb	#TOKTO
			jsr	LB26F
			jsr	LB143
			jsr	LB141
			ldb	FP0SGN
			orb	#$7F
			andb	FPA0
			stb	FPA0
			ldy	#LAD7F
			jmp	LB1EA
LAD7F:	ldx	#LBAC5
			jsr	LBC14
			jsr	GETCCH
			cmpa	#TOKSTP
			bne	#LAD90
			jsr	GETNCH
			jsr	LB141
LAD90:	jsr	LBC6D
			jsr	LB1E6
			ldd	VARDES
			pshs	B,A
			lda	#$80
			pshs	A

LAD9E:	bsr	LADEB
			ldx	CHARAD
			stx	TINPTR
			lda	,X+
			beq	LADB4
			cmpa	#':'
			beq	LADC0
LADB1:	jmp	LB277
LADB4:	lda	,X++
			sta	ENDFLG
			beq	LAE15
			ldd	,X+
			std	CURLIN
			stx	CHARAD
LADC0:	jsr	GETNCH
			bsr	LADC6
			bra	LAD9E
LADC6:	beq	LAE40
			tsta
			lbpl	LET
			cmpa	#TOKMXE
			bhi	LADDC
			ldx	COMVEC+3
LADD4:	asla
			tfr	A,B
			abx
			jsr	GETNCH
			jmp	[,X]
LADDC:	cmpa	#TOKMX
			bls	LADB1
			jmp	[COMVEC+13]

RESTOR:	ldx	TXTTAB
			leax	-1,X
LADE8:	stx	DATPTR
			rts

LADEB:	tst	KEY_STATE
			bmi	LADFA
			lda	KEY
			neg	KEY_STATE
			cmpa	#3
			beq	STOP
			sta	IKEYIM
LADFA:	rts

END:		jsr	GETCCH
			bra	LAE0B

STOP:		orcc	#1
LAE0B:	bne	LAE40
			ldx	CHARAD
			stx	TINPTR
LAE11:	ror	ENDFLG
			leas	2,S
LAE15:	ldx	CURLIN
			cmpx	#$FFFF
			beq	LAE22
			stx	OLDTXT
			ldx	TINPTR
			stx	OLDPTR
LAE22:	ldx	#LABF2-2
			tst	ENDFLG
			lbpl	LAC73
			jmp	LAC68

CONT:		bne	LAE40
			ldb	#2*16
			ldx	OLDPTR
			lbeq	LAC46
			stx	CHARAD
			ldx	OLDTXT
			stx	CURLIN
LAE40:	rts

CLEAR:	beq	LAE6F
			jsr	LB3E6
			pshs	B,A
			ldx	MEMSIZ
			jsr	GETCCH
			beq	LAE5A
			jsr	LB26D
			jsr	LB73D
			leax	-1,X
			cmpx	TOPRAM
			bhi	LAE72
LAE5A:	tfr	X,D
			subd	,S++
			bcs	LAE72
			tfr	D,U
			subd	#STKBUF
			bcs	LAE72
			subd	VARTAB
			bcs	LAE72
			stu	FRETOP
			stx	MEMSIZ
LAE6F:	jmp	LAD26
LAE72:	jmp	LAC44

RUN:		jsr	GETCCH
			lbeq	LAD21
			jsr	LAD26
			bra	LAE9F

GO:		tfr	A,B
LAE88:	jsr	GETNCH
			cmpb	#TOKTO
			beq	LAEA4
			cmpb	#TOKSUB
			bne	LAED7
			ldb	#3
			jsr	LAC33
			ldu	CHARAD
			ldx	CURLIN
			lda	#$A6
			pshs	U,X,A
LAE9F:	bsr	LAEA4
			jmp	LAD9E
LAEA4:	jsr	GETCCH
			jsr	LAF67
			bsr	LAEEB
			leax	1,X
			ldd	BINVAL
			cmpd	CURLIN
			bhi	LAEB6
			ldx	TXTTAB
LAEB6:	jsr	LAD05
			bcs	LAED2
LAEBB:	leax	-1,X
			stx	CHARAD
LAEBF:	rts

RETURN:	bne	LAEBF
			lda	#$FF
			sta	VARDES
			jsr	LABF9
			tfr	X,S
			cmpa	#TOKSUB-$80
			beq	LAEDA
			ldb	#2*2
			fcb	SKP2
LAED2:	ldb	#7*2
			jmp	LAC46
LAED7:	jmp	LB277
LAEDA:	puls	A,X,U
			stx	CURLIN
			stu	CHARAD

DATA:		bsr	LAEE8
			fcb	SKP2

ELSE:
REM:		bsr	LAEEB
			stx	CHARAD
LAEE7:	rts

LAEE8:	ldb	#':'
LAEEA:	fcb	SKP1LD
LAEEB:	clrb
			stb	CHARAC
			clrb
			ldx	CHARAD
LAEF1:	tfr	B,A
			ldb	CHARAC
			sta	CHARAC
LAEF7:	lda	,X
			beq	LAEE7
			pshs	B
			cmpa	,S+
			beq	LAEE7
			leax	1,X
			cmpa	#'"'
			beq	LAEF1
			inca
			bne	LAF0C
			leax	1,X
LAF0C:	cmpa	#TOKIF+1
			bne	LAEF7
			inc	IFCTR
			bra	LAEF7

IF:		jsr	LB141
			jsr	GETCCH
			cmpa	#TOKGO
			beq	LAF22
			ldb	#TOKTHN
			jsr	LB26F
LAF22:	lda	FP0EXP
			bne	LAF39
			clr	IFCTR
LAF28:	bsr	DATA
			tsta
			beq	LAEE7
			jsr	GETNCH
			cmpa	#TOKELS
			bne	LAF28
			dec	IFCTR
			bpl	LAF28
			jsr	GETNCH
LAF39:	jsr	GETCCH
			lbcs	LAEA4
			jmp	LADC6

ON:		jsr	LB70B
			ldb	#TOKGO
			jsr	LB26F
			pshs	A
			cmpa	#TOKSUB
			beq	LAF54
			cmpa	#TOKTO
LAF52:	bne	LAED7
LAF54:	dec	FPA0+3
			bne	LAF5D
			puls	B
			jmp	LAE88
LAF5D:	jsr	GETNCH
			bsr	LAF67
			cmpa	#','
			beq	LAF54
			puls	B,PC
LAF67:	ldx	#$0000
			stx	BINVAL

LAF6B:	bcc	LAFCE
			suba	#'0'
			sta	CHARAC
			ldd	BINVAL
			cmpa	#$24
			bhi	LAF52
			aslb
			rola
			aslb
			rola
			addd	BINVAL
			aslb
			rola
			addb	CHARAC
			adca	#0
			std	BINVAL
			jsr	GETNCH
			bra	LAF6B

LET:		jsr	LB357
			stx	VARDES
			ldb	#TOKEQ
			jsr	LB26F
			lda	VALTYP
			pshs	A
			jsr	LB156
			puls	A
			rora
			jsr	LB148
			lbeq	LBC33

LAFA4:	ldx	FPA0+2
			ldd	FRETOP
			cmpd	2,X
			bcc	LAFBE
			cmpx	VARTAB
			bcs	LAFBE
LAFB1:	ldb	,X
			jsr	LB50D
			ldx	V4D
			jsr	LB643
			ldx	#STRDES
LAFBE:	stx	V4D
			jsr	LB675
			ldu	V4D
			ldx	VARDES
			pulu	A,B,Y
			sta	,X
			sty	2,X
LAFCE:	rts

LAFCF:	fcn	'?REDO\n'

LAFD6:	ldb	#2*17
			bra	LAFDF
LAFDC:	jmp	LAC46
LAFDF:	ldx	#LAFCF
			jsr	text_print
			ldx	TINPTR
			stx	CHARAD
			rts

INPUT:	ldb	#11*2
			ldx	CURLIN
			leax	1,X
			beq	LAFDC
			bsr	LB002
			rts
LB002:	cmpa	#'#'
			bne	LB00F
			jsr	LB26D
LB00F:	cmpa	#'"'
			bne	LB01E
			jsr	LB244
			ldb	#';'
			jsr	LB26F
			jsr	LB99F
LB01E:	ldx	#LINBUF
			clr	,X
			bsr	LB02F
			ldb	#','
			stb	,X
			bra	LB049
LB02F:	jsr	LB9AF
			jsr	LB9AC
LB035:	jsr	LA390
			bcc	LB03F
			leas	4,S
			jmp	LAE11
LB03F:	rts

READ:		ldx	DATPTR
			fcb	SKP1LD
LB049:	clra
			sta	INPFLG
			stx	DATTMP
LB04E:	jsr	LB357
			stx	VARDES
			ldx	CHARAD
			stx	BINVAL
			ldx	DATTMP
			lda	,X
			bne	LB069
			lda	INPFLG
			bne	LB0B9
			jsr	LB9AF
			bsr	LB02F
LB069:	stx	CHARAD
			jsr	GETNCH
			ldb	VALTYP
			beq	LB098
			ldx	CHARAD
			sta	CHARAC
			cmpa	#'"'
			beq	LB08B
			leax	-1,X
			clra
			sta	CHARAC
			lda	#':'
			sta	CHARAC
			lda	#','
LB08B:	sta	ENDCHR
			jsr	LB51E
			jsr	LB249
			jsr	LAFA4
			bra	LB09E

LB098:	jsr	LBD12
			jsr	LBC33
LB09E:	jsr	GETCCH
			beq	LB0A8
			cmpa	#','
			lbne	LAFD6
LB0A8:	ldx	CHARAD
			stx	DATTMP
			ldx	BINVAL
			stx	CHARAD
			jsr	GETCCH
			beq	LB0D5
			jsr	LB26D
			bra	LB04E

LB0B9:	stx	CHARAD
			jsr	LAEE8
			leax	1,X
			tsta
			bne	LB0CD
			ldb	#2*3
			ldu	,X++
			beq	LB10A
			ldd	,X++
			std	DATTXT
LB0CD:	lda	,X
			cmpa	#TOKDAT
			bne	LB0B9
			bra	LB069

LB0D5:
LB0E7:	rts

NEXT:		bne	LB0FE
			ldx	#$0000
			bra	LB101
LB0FE:	jsr	LB357
LB101:	stx	VARDES
			jsr	LABF9
			beq	LB10C
			clrb
LB10A:	bra	LB153
LB10C:	tfr	X,S
			leax	3,X
			jsr	LBC14
			lda	8,S
			sta	FP0SGN
			ldx	VARDES
			jsr	LB9C2
			jsr	LBC33
			leax	9,S
			jsr	LBC96
			subb	8,S
			beq	LB134
			ldx	14,S
			stx	CURLIN
			ldx	16,S
			stx	CHARAD
LB131:	jmp	LAD9E
LB134:	leas	18,S
			jsr	GETCCH
			cmpa	#','
			bne	LB131
			jsr	GETNCH
			bsr	LB0FE

LB141:	bsr	LB156
LB143:	andcc	#$FE
LB145:	fcb	$7D
LB146:	orcc	#1
LB148:	tst	VALTYP
			bcs	LB14F
			bpl	LB0E7
			fcb	SKP2
LB14F:	bmi	LB0E7
			ldb	#12*2
LB153:	jmp	LAC46
LB156:	bsr	LB1C6
			clra
			fcb	SKP2
LB15A:	pshs	B
			pshs	A
			ldb	#1
			jsr	LAC33
			jsr	LB223
			clr	TRELFL
LB168:	jsr	GETCCH
LB16A:	suba	#TOKGT
			bcs	LB181
			cmpa	#3
			bcc	LB181
			cmpa	#1
			rola
			eora	TRELFL
			cmpa	TRELFL
			bcs	LB1DF
			sta	TRELFL
			jsr	GETNCH
			bra	LB16A
LB181:	ldb	TRELFL
			bne	LB1B8
			lbcc	LB1F4
			adda	#7
			bcc	LB1F4
			adca	VALTYP
			lbeq	LB60F
			adca	#-1
			pshs	A
			asla
			adda	,S+
			ldx	#LAA51
			leax	A,X
LB19F:	puls	A
			cmpa	,X
			bcc	LB1FA
			bsr	LB143

LB1A7:	pshs	A
			bsr	LB1D4
			ldx	RELPTR
			puls	A
			bne	LB1CE
			tsta
			lbeq	LB220
			bra	LB203

LB1B8:	asl	VALTYP
			rolb
			bsr	LB1C6
			ldx	#LB1CB
			stb	TRELFL
			clr	VALTYP
			bra	LB19F

LB1C6:	ldx	CHARAD
			jmp	LAEBB
LB1CB:	fcb	$64
LB1CC:	fdb	LB2F4

LB1CE:	cmpa	,X
			bcc	LB203
			bra	LB1A7

LB1D4:	ldd	1,X
			pshs	B,A
			bsr	LB1E2
			ldb	TRELFL
			lbra	LB15A
LB1DF:	jmp	LB277
LB1E2:	ldb	FP0SGN
			lda	,X
LB1E6:	puls	Y
			pshs	B
LB1EA:	ldb	FP0EXP
			ldx	FPA0
			ldu	FPA0+2
			pshs	U,X,B
			jmp	,Y

LB1F4:	ldx	#$0000
			lda	,S+
			beq	LB220
LB1FA:	cmpa	#$64
			beq	LB201
			jsr	LB143
LB201:	stx	RELPTR
LB203:	puls	B
			cmpa	#$5A
			beq	LB222
			cmpa	#$7D
			beq	LB222
			lsrb
			stb	RELFLG
			puls	A,X,U
			sta	FP1EXP
			stx	FPA1
			stu	FPA1+2
			puls	B
			stb	FP1SGN
			eorb	FP0SGN
			stb	RESSGN
LB220:	ldb	FP0EXP
LB222:	rts

LB223:	clr	VALTYP
			jsr	GETNCH
			bcc	LB22F
LB22C:	jmp	LBD12

LB22F:	jsr	LB3A2
			bcc	LB284
			cmpa	#'.'
			beq	LB22C
			cmpa	#TOKMI
			beq	LB27C
			cmpa	#TOKPL
			beq	LB223
			cmpa	#'"'
			bne	LB24E
LB244:	ldx	CHARAD
			jsr	LB518
LB249:	ldx	COEFPT
			stx	CHARAD
			rts
LB24E:	cmpa	#TOKNOT
			bne	LB25F
			lda	#$5A
			jsr	LB15A
			jsr	INTCNV
			comd
			jmp	GIVABF
LB25F:	inca
			beq	LB290
LB262:	bsr	LB26A
			jsr	LB156
LB267:	ldb	#')'
			fcb	SKP2
LB26A:	ldb	#'('
			fcb	SKP2
SYNCOMMA:
LB26D:	ldb	#','
LB26F:	cmpb	[CHARAD]
			bne	LB277
			jmp	GETNCH
LB277:	ldb	#2*1
			jmp	LAC46

LB27C:	lda	#$7D
			jsr	LB15A
			jmp	LBEE9

LB284:	jsr	LB357
			stx	FPA0+2
			lda	VALTYP
			bne	LB222
			jmp	LBC14

LB290:	jsr	GETNCH
			tfr	A,B
			aslb
			jsr	GETNCH
			cmpb	#2*NUMSEC
			bls	LB29F
			jmp	[COMVEC+18]
LB29F:	pshs	B
			cmpb	#14*2
			bcs	LB2C7
			cmpb	#17*2
			bcc	LB2C9
			bsr	LB26A
			lda	,S
			jsr	LB156
			bsr	LB26D
			jsr	LB146
			puls	A
			ldu	FPA0+2
			pshs	U,A
			jsr	LB70B
			puls	A
			pshs	B,A
			fcb	$8E
LB2C7:	bsr	LB262
LB2C9:	puls	B
			ldx	COMVEC+8
LB2CE:	abx
			jsr	[,X]
			jmp	LB143

LB2D4:	fcb	SKP1LD
LB2D5:	clra
			sta	TMPLOC
			jsr	INTCNV
			std	CHARAC
			jsr	LBC4A
			jsr	INTCNV
			tst	TMPLOC
			bne	LB2ED
			anda	CHARAC
			andb	ENDCHR
			bra	LB2F1
LB2ED:	ora	CHARAC
			orb	ENDCHR
LB2F1:	jmp	GIVABF

LB2F4:	jsr	LB148
			bne	LB309
			lda	FP1SGN
			ora	#$7F
			anda	FPA1
			sta	FPA1
			ldx	#FP1EXP
			jsr	LBC96
			bra	LB33F

LB309:	clr	VALTYP
			dec	TRELFL
			jsr	LB657
			stb	STRDES
			stx	STRDES+2
			ldx	FPA1+2
			jsr	LB659
			lda	STRDES
			pshs	B
			suba	,S+
			beq	LB328
			lda	#1
			bcc	LB328
			ldb	STRDES
			nega
LB328:	sta	FP0SGN
			ldu	STRDES+2
			incb
LB32D:	decb
			bne	LB334
			ldb	FP0SGN
			bra	LB33F
LB334:	lda	,X+
			cmpa	,U+
			beq	LB32D
			ldb	#$FF
			bcc	LB33F
			negb
LB33F:	addb	#1
			rolb
			andb	RELFLG
			beq	LB348
			ldb	#$FF
LB348:	jmp	LBC7C

LB34B:	jsr	LB26D
DIM:		ldb	#1
			bsr	LB35A
			jsr	GETCCH
			bne	LB34B
			rts

LB357:	clrb
			jsr	GETCCH
LB35A:	stb	DIMFLG
			sta	VARNAM
LB35E:	jsr	GETCCH
			bsr	LB3A2
			lbcs	LB277
			clrb
			stb	VALTYP
			jsr	GETNCH
			bcs	LB371
			bsr	LB3A2
			bcs	LB37B
LB371:	tfr	A,B
LB373:	jsr	GETNCH
			bcs	LB373
			bsr	LB3A2
			bcc	LB373
LB37B:	cmpa	#'$'
			bne	LB385
			com	VALTYP
			addb	#$80
			jsr	GETNCH
LB385:	stb	VARNAM+1
			ora	ARYDIS
			suba	#'('
			lbeq	LB404
			clr	ARYDIS
			ldx	VARTAB
			ldd	VARNAM
LB395:	cmpx	ARYTAB
			beq	LB3AB
			cmpd	,X++
			beq	LB3DC
			leax	5,X
			bra	LB395

LB3A2:	cmpa	#'A'
			bcs	LB3AA
			suba	#'Z'+1
			suba	#-('Z'+1)
LB3AA:	rts

LB3AB:	ldx	#$0000
			ldu	,S
			cmpu	#$B287
			beq	LB3DE
			ldd	ARYEND
			std	V43
			addd	#7
			std	V41
			ldx	ARYTAB
			stx	V47
			jsr	LAC1E
			ldx	V41
			stx	ARYEND
			ldx	V45
			stx	ARYTAB
			ldx	V47
			ldd	VARNAM
			std	,X++
			clrd
			std	,X
			std	2,X
			sta	4,X
LB3DC:	stx	VARPTR
LB3DE:	rts

LB3DF:	FCB	$90,$80,$00,$00,$00

LB3E4:	jsr	GETNCH
LB3E6:	jsr	LB141
LB3E9:	lda	FP0SGN
			bmi	LB44A

INTCNV:	jsr	LB143
			lda	FP0EXP
			cmpa	#$90
			bcs	LB3FE
			ldx	#LB3DF
			jsr	LBC96
			bne	LB44A
LB3FE:	jsr	LBCC8
			ldd	FPA0+2
			rts

LB404:	ldd	DIMFLG
			pshs	B,A
			clrb
LB40A:	ldx	VARNAM
			pshs	X,B
			bsr	LB3E4
			puls	B,X,Y
			stx	VARNAM
			ldu	FPA0+2
			pshs	U,Y
			incb
			jsr	GETCCH
			cmpa	#','
			beq	LB40A
			stb	TMPLOC
			jsr	LB267
			puls	A,B
			std	DIMFLG
			ldx	ARYTAB
LB42A:	cmpx	ARYEND
			beq	LB44F
			ldd	VARNAM
			cmpd	,X
			beq	LB43B
			ldd	2,X
			leax	D,X
			bra	LB42A
LB43B:	ldb	#9*2
			lda	DIMFLG
			bne	LB44C
			ldb	TMPLOC
			cmpb	4,X
			beq	LB4A0
LB447:	ldb	#8*2
			fcb	SKP2
LB44A:	ldb	#4*2
LB44C:	jmp	LAC46

LB44F:	ldd	#5
			std	COEFPT
			ldd	VARNAM
			std	,X
			ldb	TMPLOC
			stb	4,X
			jsr	LAC33
			stx	V41
LB461:	ldb	#11
			clra
			tst	DIMFLG
			beq	LB46D
			puls	A,B
			addd	#1
LB46D:	std	5,X
			bsr	LB4CE
			std	COEFPT
			leax	2,X
			dec	TMPLOC
			bne	LB461
			stx	TEMPTR
			addd	TEMPTR
			lbcs	LAC44
			tfr	D,X
			jsr	LAC37
			subd	#STKBUF-5
			std	ARYEND
			clra
LB48C:	leax	-1,X
			sta	5,X
			cmpx	TEMPTR
			bne	LB48C
			ldx	V41
			lda	ARYEND
			subd	V41
			std	2,X
			lda	DIMFLG
			bne	LB4CD
LB4A0:	ldb	4,X
			stb	TMPLOC
			clrd
LB4A6:	std	COEFPT
			puls	A,B
			std	FPA0+2
			cmpd	5,X
			bcc	LB4EB
			ldu	COEFPT
			beq	LB4B9
			bsr	LB4CE
			addd	FPA0+2
LB4B9:	leax	2,X
			dec	TMPLOC
			bne	LB4A6
			std	,--S
			aslb
			rola
			aslb
			rola
			addd	,S++
			leax	D,X
			leax	5,X
			stx	VARPTR
LB4CD:	rts

LB4CE:	lda	#16
			sta	V45
			ldd	5,X
			std	BOTSTK
			clrd
LB4D8:	aslb
			rola
			bcs	LB4EB
			asl	COEFPT+1
			rol	COEFPT
			bcc	LB4E6
			addd	BOTSTK
			bcs	LB4EB
LB4E6:	dec	V45
			bne	LB4D8
			rts
LB4EB:	jmp	LB447

MEM:		tfr	S,D
			subd	ARYEND
			fcb	SKP
LB4F3:	clra
GIVABF:	clr	VALTYP
			std	FPA0
			ldb	#$90
			jmp	LBC82

STR:		jsr	LB143
			ldu	#STRBUF+2
			jsr	LBDDC
			leas	2,S
			ldx	#STRBUF+1
			bra	LB518

LB50D:	stx	V40
LB50F:	bsr	LB56D
LB511:	stx	STRDES+2
			stb	STRDES
			rts

LB516:	leax	-1,X
LB518:	lda	#'"'
			sta	CHARAC
LB51A:	sta	ENDCHR
LB51E:	leax	1,X
			stx	RESSGN
			stx	STRDES+2
			ldb	#-1
LB526:	incb
			lda	,X+
			beq	LB537
			cmpa	CHARAC
			beq	LB533
			cmpa	ENDCHR
			bne	LB526
LB533:	cmpa	#'"'
			beq	LB539
LB537:	leax	-1,X
LB539:	stx	COEFPT
			stb	STRDES
			ldu	RESSGN
			cmpu	#STRBUF+2
LB543:	bhi	LB54C
			bsr	LB50D
			ldx	RESSGN
			jsr	LB645
LB54C:	ldx	TEMPPT
			cmpx	#LINHDR
			bne	LB558
			ldb	#15*2
LB555:	jmp	LAC46
LB558:	lda	STRDES
			sta	,X
			ldd	STRDES+2
			std	2,X
			lda	#$FF
			sta	VALTYP
			stx	LASTPT
			stx	FPA0+2
			leax	5,X
			stx	TEMPPT
			rts

LB56D:	clr	GARBFL
LB56F:	clra
			pshs	B,A
			ldd	STRTAB
			subd	,S+
			cmpd	FRETOP
			bcs	LB585
			std	STRTAB
			ldx	STRTAB
			leax	1,X
			stx	FRESPC
			puls	B,PC
LB585:	ldb	#2*13
			com	GARBFL
			beq	LB555
			bsr	LB591
			puls	B
			bra	LB56F

LB591:	ldx	MEMSIZ
LB593:	stx	STRTAB
			clrd
			std	V48
			ldx	FRETOP
			stx	V47
			ldx	#STRSTK
LB5A0:	cmpx	TEMPPT
			beq	LB5A8
			bsr	LB5D8
			bra	LB5A0
LB5A8:	ldx	VARTAB
LB5AA:	cmpx	ARYTAB
			beq	LB5B2
			bsr	LB5D2
			bra	LB5AA
LB5B2:	stx	V41
LB5B4:	ldx	V41
LB5B6:	cmpx	ARYEND
			beq	LB5EF
			ldd	2,X
			addd	V41
			std	V41
			lda	1,X
			bpl	LB5B4
			ldb	4,X
			aslb
			addb	#5
			abx
LB5CA:	cmpx	V41
			beq	LB5B6
			bsr	LB5D8
			bra	LB5CA
LB5D2:	lda	1,X
			leax	2,X
			bpl	LB5EC

LB5D8:	ldb	,X
			beq	LB5EC
			ldd	2,X
			cmpd	STRTAB
			bhi	LB5EC
			cmpd	V47
			bls	LB5EC
			stx	V4B
			std	V47
LB5EC:	leax	5,X
LB5EE:	rts
LB5EF:	ldx	V4B
			beq	LB5EE
			clra
			ldb	,X
			decb
			addd	V47
			std	V43
			ldx	STRTAB
			stx	V41
			jsr	LAC20
			ldx	V4B
			ldd	V45
			leax	-1,X
			jmp	LB593

LB60F:	ldd	FPA0+2
			pshs	B,A
			jsr	LB223
			jsr	LB146
			puls	X
			stx	RESSGN
			ldb	,X
			ldx	FPA0+2
			addb	,X
			bcc	LB62A
			ldb	#14*2
			jmp	LAC46
LB62A:	jsr	LB50D
			ldx	RESSGN
			ldx	,X
			bsr	LB643
			ldx	V4D
			bsr	LB659
			bsr	LB645
			ldx	RESSGN
			bsr	LB659
			jsr	LB54C
			jmp	LB168

LB643:	ldx	2,X
LB645:	ldu	FRESPC
			incb
			bra	LB64E

LB64A:	lda	,X+
			sta	,U+
LB64E:	decb
			bne	LB64A
			stu	FRESPC
			rts

LB654:	jsr	LB146
LB657:	ldx	FPA0+2
LB659:	ldb	,X
			bsr	LB675
			bne	LB672
			ldx	5+2,X
			leax	-1,X
			cmpx	STRTAB
			bne	LB66F
			pshs	B
			addd	STRTAB
			std	STRTAB
			puls	B
LB66F:	leax	1,X
			rts
LB672:	ldx	2,X
			rts

LB675:	cmpx	LASTPT
			bne	LB680
			stx	TEMPPT
			leax	-5,X
			stx	LASTPT
			clra
LB680:	rts

LEN:		bsr	LB686
LB683:	jmp	LB4F3
LB686:	bsr	LB654
			clr	VALTYP
			tstb
			rts

CHR:		jsr	LB70E
LB68F:	ldb	#1
			jsr	LB56D
			lda	FPA0+3
			jsr	LB511
			sta	,X
LB69B:	leas	2,S
LB69D:	jmp	LB54C

ASC:		bsr	LB6A4
			bra	LB683
LB6A4:	bsr	LB686
			beq	LB706
			ldb	,X
			rts

LEFT:		bsr	LB6F5
			clra
LB6AE:	cmpb	,X
			bls	LB6B5
			ldb	,X
			clra
LB6B5:	pshs	B,A
			jsr	LB50F
			ldx	V4D
			bsr	LB659
			puls	B
			abx
			puls	B
			jsr	LB645
			bra	LB69D

RIGHT:	bsr	LB6F5
			suba	,X
			nega
			bra	LB6AE

MID:		ldb	#$FF
			stb	FPA0+3
			jsr	GETCCH
			cmpa	#')'
			beq	LB6DE
			jsr	LB26D
			bsr	LB70B
LB6DE:	bsr	LB6F5
			beq	LB706
			clrb
			deca
			cmpa	,X
			bcc	LB6B5
			tfr	A,B
			subb	,X
			negb
			cmpb	FPA0+3
			bls	LB6B5
			ldb	FPA0+3
			bra	LB6B5
LB6F5:	jsr	LB267
			ldu	,S
			ldx	5,S
			stx	V4D
			lda	4,S
			ldb	4,S
			leas	7,S
			tfr	U,PC

LB706:	jmp	LB44A

LB709:	jsr	GETNCH
LB70B:	jsr	LB141
LB70E:	jsr	LB3E9
			tsta
			bne	LB706
			jmp	GETCCH

VAL:		jsr	LB686
			lbeq	LBA39
			ldu	CHARAD
			stx	CHARAD
			abx
			lda	,X
			pshs	U,X,A
			clr	,X
			jsr	GETCCH
			jsr	LBD12
			puls	A,X,U
			sta	,X
			stu	CHARAD
			rts

LB734:	bsr	LB73D
			stx	BINVAL
LB738:	jsr	LB26D
			bra	LB70B
LB73D:	jsr	LB141
LB740:	lda	FP0SGN
			bmi	LB706
			lda	FP0EXP
			cmpa	#$90
			bhi	LB706
			jsr	LBCC8
			ldx	FPA0+2
			rts

PEEK:		bsr	LB740
			ldb	,X
			jmp	LB4F3

POKE:		bsr	LB734
			ldx	BINVAL
			stb	,X
			rts

LIST:		pshs	CC
			jsr	LAF67
			jsr	LAD01
			stx	LSTTXT
			puls	CC
			beq	LB784
			jsr	GETCCH
			beq	LB789
			cmpa	#TOKMI
			bne	LB783
			jsr	GETNCH
			beq	LB784
			jsr	LAF67
			beq	LB789
LB783:	rts
LB784:	ldu	#$FFFF
			stu	BINVAL
LB789:	leas	2,S
			ldx	LSTTXT
LB78D:	jsr	text_crlf
			jsr	LADEB
			ldd	,X
			bne	LB79F
LB797:	jmp	LAC73
LB79F:	stx	LSTTXT
			ldd	2,X
			cmpd	BINVAL
			bhi	LB797
			jsr	LBDCC
			jsr	LB9AC
			ldx	LSTTXT
			bsr	LB7C2
			ldx	[LSTTXT]
			ldu	#LINBUF+1
LB7B9:	lda	,U+
			beq	LB78D
			jsr	LB9B1
			bra	LB7B9

LB7C2:	leax	4,X
			ldy	#LINBUF+1
LB7CB:	lda	,X+
			beq	LB820
			bmi	LB7E6
			cmpa	#':'
			bne	LB7E2
			ldb	,X
			cmpb	#TOKELS
			beq	LB7CB
			cmpb	#TOKREM
			beq	LB7CB
			fcb	SKP2
LB7E0:	lda	#'!'
LB7E2:	bsr	LB814
			bra	LB7CB
LB7E6:	ldu	#COMVEC-10
			cmpa	#$FF
			bne	LB7F1
			lda	,X+
			leau	5,U
LB7F1:	anda	#$7F
LB7F3:	leau	10,U
			tst	,U
			beq	LB7E0
			suba	,U
			bpl	LB7F3
			adda	,U
			ldu	1,U
LB801:	deca
			bmi	LB80A
LB804:	tst	,U+
			bpl	LB804
			bra	LB801
LB80A:	lda	,U
			bsr	LB814
			tst	,U+
			bpl	LB80A
			bra	LB7CB
LB814:	cmpy	#LINBUF+LBUFMX
			bcc	LB820
			anda	#$7F
			sta	,Y+
			clr	,Y
LB820:	rts

LB821:	ldx	CHARAD
			ldu	#LINBUF
LB829:	clr	V43
			clr	V44
LB82D:	lda	,X+
			beq	LB852
			tst	V43
			beq	LB844
			jsr	LB3A2
			bcc	LB852
			cmpa	#'0'
			blo	LB842
			cmpa	#'9'
			bls	LB852
LB842:	clr	V43
LB844:	cmpa	#' '
			beq	LB852
			sta	V42
			cmpa	#'"'
			beq	LB886
			tst	V44
			beq	LB86B
LB852:	sta	,U+
			beq	LB85C
			cmpa	#':'
			beq	LB829
LB85A:	bra	LB82D
LB85C:	clr	,U+
			clr	,U+
			tfr	U,D
			subd	#LINHDR
			ldx	#LINBUF-1
			stx	CHARAD
			rts
LB86B:	cmpa	#'?'
			bne	LB873
			lda	#TOKPRT
			bra	LB852
LB873:	cmpa	#'''
			bne	LB88A
			ldd	#$3A83
			std	,U++
LB87C:	clr	V42
LB87E:	lda	,X+
			beq	LB852
			cmpa	V42
			beq	LB852
LB886:	sta	,U+
			bra	LB87E
LB88A:	cmpa	#'0'
			bcs	LB892
			cmpa	#';'+1
			bcs	LB852
LB892:	leax	-1,X
			pshs	U,X
			clr	V41
			ldu	#COMVEC-10
LB89B:	clr	V42
LB89D:	leau	10,U
			lda	,U
			beq	LB8D4
			ldy	1,U
LB8A6:	ldx	,S
LB8A8:	ldb	,Y+
			subb	,X+
			beq	LB8A8
			cmpb	#$80
			bne	LB8EA
			leas	2,S
			puls	U
			orb	V42
			lda	V41
			bne	LB8C2
			cmpb	#TOKELS
			bne	LB8C6
			lda	#':'
LB8C2:	std	,U++
			bra	LB85A
LB8C6:	stb	,U+
			cmpb	#TOKDAT
			bne	LB8CE
			inc	V44
LB8CE:	cmpb	#TOKREM
			beq	LB87C
LB8D2:	lbra	LB85A
LB8D4:	ldu	#COMVEC-5
			com	V41
			bne	LB89B
			puls	X,U
			lda	,X+
			sta	,U+
			jsr	LB3A2
			bcs	LB8D2
			com	V43
			bra	LB8D2
LB8EA:	inc	V42
			deca
			beq	LB89D
			leay	-1,Y
LB8F1:	ldb	,Y+
			bpl	LB8F1
			bra	LB8A6

PRINT:	beq	LB958
			bsr	LB8FE
			rts
LB8FE:	cmpa	#'@'
			bne	LB907
			jsr	LA554
			bra	LB911
LB907:	cmpa	#'#'
			bne	LB918
LB911:	jsr	GETCCH
			beq	LB958
			jsr	LB26D
LB918:
LB91B:	beq	LB965
LB91D:	cmpa	#';'
			beq	LB997
			jsr	LB156
			lda	VALTYP
			pshs	A
			bne	LB938
			jsr	LBDD9
			jsr	LB516
LB938:	bsr	LB99F
			puls	B
LB949:	tstb
			bne	LB954
			jsr	GETCCH
			bsr	LB9AC
LB954:	jsr	GETCCH
			bne	LB91D
LB958:	jsr	text_crlf
LB95C:	ldw	VDP_CURSOR
@l1:		subw	#40
			bge	@l1
			addw	#40
			bne	LB958
LB965:	rts

LB997:	jsr	GETNCH
			jmp	LB91B

LB99F:	jsr	LB657
			incb
LB9A3:	decb
			beq	LB965
			lda	,X+
			jsr	text_putc
			bra	LB9A3

LB9AC:	lda	#' '
			fcb	SKP2
LB9AF:	lda	#'?'
LB9B1:	jmp	text_putc

LB9B4:	ldx	#LBEC0
			bra	LB9C2

LB9B9:	jsr	LBB2F

LB9BC:	com	FP0SGN
			com	RESSGN
			bra	LB9C5

LB9C2:	jsr	LBB2F

LB9C5:	tstb
			lbeq	LBC4A
			ldx	#FP1EXP
LB9CD:	tfr	A,B
			tstb
			beq	LBA3E
			subb	FP0EXP
			beq	LBA3F
			bcs	LB9E2
			sta	FP0EXP
			lda	FP1SGN
			sta	FP0SGN
			ldx	#FP0EXP
			negb
LB9E2:	cmpb	#-8
			ble	LBA3F
			clra
			lsr	1,X
			jsr	LBABA
LB9EC:	ldb	RESSGN
			bpl	LB9FB
			com	1,X
			com	2,X
			com	3,X
			com	4,X
			coma
			adca	#0

LB9FB:	sta	FPSBYT
			lda	FPA0+3
			adca	FPA1+3
			sta	FPA0+3
			lda	FPA0+2
			adca	FPA1+2
			sta	FPA0+2
			lda	FPA0+1
			adca	FPA1+1
			sta	FPA0+1
			lda	FPA0
			adca	FPA1
			sta	FPA0
			tstb
			bpl	LBA5C
LBA18:	bcs	LBA1C
			bsr	LBA79

LBA1C:	clrb
LBA1D:	lda	FPA0
			bne	LBA4F
			lda	FPA0+1
			sta	FPA0
			lda	FPA0+2
			sta	FPA0+1
			lda	FPA0+3
			sta	FPA0+2
			lda	FPSBYT
			sta	FPA0+3
			clr	FPSBYT
			addb	#8
			cmpb	#5*8
			blt	LBA1D
LBA39:	clra
LBA3A:	sta	FP0EXP
			sta	FP0SGN
LBA3E:	rts
LBA3F:	bsr	LBAAE
			clrb
			bra	LB9EC
LBA44:	incb
			asl	FPSBYT
			rol	FPA0+3
			rol	FPA0+2
			rol	FPA0+1
			rol	FPA0
LBA4F:	bpl	LBA44
			lda	FP0EXP
			pshs	B
			suba	,S+
			sta	FP0EXP
			bls	LBA39
			fcb	SKP2
LBA5C:	bcs	LBA66
			asl	FPSBYT
			lda	#0
			sta	FPSBYT
			bra	LBA72
LBA66:	inc	FP0EXP
			beq	LBA92
			ror	FPA0
			ror	FPA0+1
			ror	FPA0+2
			ror	FPA0+3
LBA72:	bcc	LBA78
			bsr	LBA83
			beq	LBA66
LBA78:	rts

LBA79:	com	FP0SGN
LBA7B:	com	FPA0
			com	FPA0+1
			com	FPA0+2
			com	FPA0+3
LBA83:	ldx	FPA0+2
			leax	1,X
			stx	FPA0+2
			bne	LBA91
			ldx	FPA0
			leax	1,X
			stx	FPA0
LBA91:	rts
LBA92:	ldb	#2*5
			jmp	LAC46
LBA97:	ldx	#FPA2-1

LBA9A:	lda	4,X
			sta	FPSBYT
			lda	3,X
			sta	4,X
			lda	2,X
			sta	3,X
			lda	1,X
			sta	2,X
			lda	FPCARY
			sta	1,X
LBAAE:	addb	#8
			ble	LBA9A
			lda	FPSBYT
			subb	#8
			beq	LBAC4

LBAB8:	asr	1,X
LBABA:	ror	2,X
			ror	3,X
			ror	4,X
			rora
			incb
			bne	LBAB8
LBAC4:	rts
LBAC5:	fcb	$81,$00,$00,$00,$00

LBACA:	bsr	LBB2F
LBACC:	beq	LBB2E
			bsr	LBB48
LBAD0:	lda	#0
			sta	FPA2
			sta	FPA2+1
			sta	FPA2+2
			sta	FPA2+3
			ldb	FPA0+3
			bsr	LBB00
			ldb	FPSBYT
			stb	VAE
			ldb	FPA0+2
			bsr	LBB00
			ldb	FPSBYT
			stb	VAD
			ldb	FPA0+1
			bsr	LBB00
			ldb	FPSBYT
			stb	VAC
			ldb	FPA0
			bsr	LBB02
			ldb	FPSBYT
			stb	VAB
			jsr	LBC0B
			jmp	LBA1C
LBB00:	beq	LBA97
LBB02:	coma
LBB03:	lda	FPA2
			rorb
			beq	LBB2E
			bcc	LBB20
			lda	FPA2+3
			adda	FPA1+3
			sta	FPA2+3
			lda	FPA2+2
			adca	FPA1+2
			sta	FPA2+2
			lda	FPA2+1
			adca	FPA1+1
			sta	FPA2+1
			lda	FPA2
			adca	FPA1
LBB20:	rora
			sta	FPA2
			ror	FPA2+1
			ror	FPA2+2
			ror	FPA2+3
			ror	FPSBYT
			clra
			bra	LBB03
LBB2E:	rts

LBB2F:	ldd	1,X
			sta	FP1SGN
			ora	#$80
			std	FPA1
			ldb	FP1SGN
			eorb	FP0SGN
			stb	RESSGN
			ldd	3,X
			std	FPA1+2
			lda	,X
			sta	FP1EXP
			ldb	FP0EXP
			rts

LBB48:	tsta
			beq	LBB61
			adda	FP0EXP
			rora
			rola
			bvc	LBB61
			adda	#$80
			sta	FP0EXP
			beq	LBB63
			lda	RESSGN
			sta	FP0SGN
			rts

LBB5C:	lda	FP0SGN
			coma
			bra	LBB63
LBB61:	leas	2,S
LBB63:	lbpl	LBA39
LBB67:	jmp	LBA92
LBB6A:	jsr	LBC5F
			beq	LBB7C
			adda	#2
			bcs	LBB67
			clr	RESSGN
			jsr	LB9CD
			inc	FP0EXP
			beq	LBB67
LBB7C:	rts

LBB7D:	FCB	$84,$20,$00,$00,$00
LBB82:	jsr	LBC5F
			ldx	#LBB7D
			clrb
LBB89:	stb	RESSGN
			jsr	LBC14
			fcb	SKP2
LBB8F:	bsr	LBB2F
LBB91:	beq	LBC06
			neg	FP0EXP
			bsr	LBB48
			inc	FP0EXP
			beq	LBB67
			ldx	#FPA2
			ldb	#4
			stb	TMPLOC
			ldb	#1
LBBA4:	lda	FPA0
			cmpa	FPA1
			bne	LBBBD
			lda	FPA0+1
			cmpa	FPA1+1
			bne	LBBBD
			lda	FPA0+2
			cmpa	FPA1+2
			bne	LBBBD
			lda	FPA0+3
			cmpa	FPA1+3
			bne	LBBBD
			coma
LBBBD:	tfr	CC,A
			rolb
			bcc	LBBCC
			stb	,X+
			dec	TMPLOC
			bmi	LBBFC
			beq	LBBF8
			ldb	#1
LBBCC:	tfr	A,CC
			bcs	LBBDE
LBBD0:	asl	FPA1+3
			rol	FPA1+2
			rol	FPA1+1
			rol	FPA1
			bcs	LBBBD
			bmi	LBBA4
			bra	LBBBD

LBBDE:	lda	FPA1+3
			suba	FPA0+3
			sta	FPA1+3
			lda	FPA1+2
			sbca	FPA0+2
			sta	FPA1+2
			lda	FPA1+1
			sbca	FPA0+1
			sta	FPA1+1
			lda	FPA1
			sbca	FPA0
			sta	FPA1
			bra	LBBD0
LBBF8:	ldb	#$40
			bra	LBBCC
LBBFC:	rorb
			rorb
			rorb
			stb	FPSBYT
			bsr	LBC0B
			jmp	LBA1C

LBC06:	ldb	#2*10
			jmp	LAC46

LBC0B:	ldx	FPA2
			stx	FPA0
			ldx	FPA2+2
			stx	FPA0+2
			rts

LBC14:	pshs	A
			ldd	1,X
			sta	FP0SGN
			ora	#$80
			std	FPA0
			clr	FPSBYT
			ldb	,X
			ldx	3,X
			stx	FPA0+2
			stb	FP0EXP
			puls	A,PC

LBC2A:	ldx	#V45
			bra	LBC35
LBC2F:	ldx	#V40
			fcb	SKP2
LBC33:	ldx	VARDES
LBC35:	lda	FP0EXP
			sta	,X
			lda	FP0SGN
			ora	#$7F
			anda	FPA0
			sta	1,X
			lda	FPA0+1
			sta	2,X
			ldu	FPA0+2
			stu	3,X
			rts

LBC4A:	lda	FP1SGN
LBC4C:	sta	FP0SGN
			ldx	FP1EXP
			stx	FP0EXP
			clr	FPSBYT
			lda	FPA1+1
			sta	FPA0+1
			lda	FP0SGN
			ldx	FPA1+2
			stx	FPA0+2
			rts

LBC5F:	ldd	FP0EXP
			std	FP1EXP
			ldx	FPA0+1
			stx	FPA1+1
			ldx	FPA0+3
			stx	FPA1+3
			tsta
			rts

LBC6D:	ldb	FP0EXP
			beq	LBC79
LBC71:	ldb	FP0SGN
LBC73:	rolb
			ldb	#$FF
			bcs	LBC79
			negb
LBC79:	rts

SGN:		bsr	LBC6D
LBC7C:	stb	FPA0
			clr	FPA0+1
			ldb	#$88
LBC82:	lda	FPA0
			suba	#$80
LBC86:	stb	FP0EXP
			ldd	#$0000
			std	FPA0+2
			sta	FPSBYT
			sta	FP0SGN
			jmp	LBA18

ABS:		clr	FP0SGN
			rts

LBC96:	ldb	,X
			beq	LBC6D
			ldb	1,X
			eorb	FP0SGN
			bmi	LBC71
LBCA0:	ldb	FP0EXP
			cmpb	,X
			bne	LBCC3
			ldb	1,X
			orb	#$7F
			andb	FPA0
			cmpb	1,X
			bne	LBCC3
			ldb	FPA0+1
			cmpb	2,X
			bne	LBCC3
			ldb	FPA0+2
			cmpb	3,X
			bne	LBCC3
			ldb	FPA0+3
			subb	4,X
			bne	LBCC3
			rts
LBCC3:	rorb
			eorb	FP0SGN
			bra	LBC73

LBCC8:	ldb	FP0EXP
			beq	LBD09
			subb	#$A0
			lda	FP0SGN
			bpl	LBCD7
			com	FPCARY
			jsr	LBA7B
LBCD7:	ldx	#FP0EXP
			cmpb	#-8
			bgt	LBCE4
			jsr	LBAAE
			clr	FPCARY
			rts
LBCE4:	clr	FPCARY
			lda	FP0SGN
			rola
			ror	FPA0
			jmp	LBABA

INT:		ldb	FP0EXP
			cmpb	#$A0
			bcc	LBD11
			bsr	LBCC8
			stb	FPSBYT
			lda	FP0SGN
			stb	FP0SGN
			suba	#$80
			lda	#$A0
			sta	FP0EXP
			lda	FPA0+3
			sta	CHARAC
			jmp	LBA18

LBD09:	stb	FPA0
			stb	FPA0+1
			stb	FPA0+2
			stb	FPA0+3
LBD11:	rts

LBD12:	ldx	#$0000
			stx	FP0SGN
			stx	FP0EXP
			stx	FPA0+1
			stx	FPA0+2
			stx	V47
			stx	V45
			bcs	LBD86
			cmpa	#'-'
			bne	LBD2D
			com	COEFCT
			bra	LBD31
LBD2D:	cmpa	#'+'
			bne	LBD35
LBD31:	jsr	GETNCH
			bcs	LBD86
LBD35:	cmpa	#'.'
			beq	LBD61
			cmpa	#'E'
			bne	LBD65
			jsr	GETNCH
			bcs	LBDA5
			cmpa	#TOKMI
			beq	LBD53
			cmpa	#'-'
			beq	LBD53
			cmpa	#TOKPL
			beq	LBD55
			bra	LBD59
LBD53:	com	V48
LBD55:	jsr	GETNCH
			bcs	LBDA5
LBD59:	tst	V48
			beq	LBD65
			neg	V47
			bra	LBD65
LBD61:	com	V46
			bne	LBD31
LBD65:	lda	V47
			suba	V45
			sta	V47
			beq	LBD7F
			bpl	LBD78
LBD6F:	jsr	LBB82
			inc	V47
			bne	LBD6F
			bra	LBD7F
LBD78:	jsr	LBB6A
			dec	V47
			bne	LBD78
LBD7F:	lda	COEFCT
			bpl	LBD11
			jmp	LBEE9

LBD86:	ldb	V45
			subb	V46
			stb	V45
			pshs	A
			jsr	LBB6A
			puls	B
			subb	#'0'
			bsr	LBD99
			bra	LBD31
LBD99:	jsr	LBC2F
			jsr	LBC7C
			ldx	#V40
			jmp	LB9C2

LBDA5:	ldb	V47
			aslb
			aslb
			addb	V47
			aslb
			suba	#'0'
			pshs	B
			adda	,S+
			sta	V47
			bra	LBD55

LBDB6:	FCB	$9B,$3E,$BC,$1F,$FD
LBDBB:	FCB	$9E,$6E,$6B,$27,$FD
LBDC0:	FCB	$9E,$6E,$6B,$28,$00

LBDC5:	ldx	#LABE8
			jsr	text_print
			ldd	CURLIN
LBDCC:	std	FPA0
			ldb	#$90
			coma
			jsr	LBC86
			bsr	LBDD9
			leax	1,X
LBDD6:	jmp	text_print

LBDD9:	ldu	#STRBUF+3
LBDDC:	lda	#' '
			ldb	FP0SGN
			bpl	LBDE4
			lda	#'-'
LBDE4:	sta	,U+
			stu	COEFPT
			sta	FP0SGN
			lda	#'0'
			ldb	FP0EXP
			lbeq	LBEB8
			clra
			cmpb	#$80
			bhi	LBDFF
			ldx	#LBDC0
			jsr	LBACA
			lda	#-9
LBDFF:	sta	V45
LBE01:	ldx	#LBDBB
			jsr	LBCA0
			bgt	LBE18
LBE09:	ldx	#LBDB6
			jsr	LBCA0
			bgt	LBE1F
			jsr	LBB6A
			dec	V45
			bra	LBE09
LBE18:	jsr	LBB82
			inc	V45
			bra	LBE01
LBE1F:	jsr	LB9B4
			jsr	LBCC8
			ldb	#1
			lda	V45
			adda	#9+1
			bmi	LBE36
			cmpa	#9+2
			bcc	LBE36
			deca
			tfr	A,B
			lda	#2
LBE36:	deca
			deca
			sta	V47
			stb	V45
			bgt	LBE4B
			ldu	COEFPT
			lda	#'.'
			sta	,U+
			tstb
			beq	LBE4B
			lda	#'0'
			sta	,U+

LBE4B:	ldx	#LBEC5
			ldb	#0+$80
LBE50:	lda	FPA0+3
			adda	3,X
			sta	FPA0+3
			lda	FPA0+2
			adca	2,X
			sta	FPA0+2
			lda	FPA0+1
			adca	1,X
			sta	FPA0+1
			lda	FPA0
			adca	,X
			sta	FPA0
			incb
			rorb
			rolb
			bvc	LBE50
			bcc	LBE72
			subb	#10+1
			negb
LBE72:	addb	#'0'-1
			leax	4,X
			tfr	B,A
			anda	#$7F
			sta	,U+
			dec	V45
			bne	LBE84
			lda	#'.'
			sta	,U+
LBE84:	comb
			andb	#$80
			cmpx	#LBEC5+9*4
			bne	LBE50
LBE8C:	lda	,-U
			cmpa	#'0'
			beq	LBE8C
			cmpa	#'.'
			bne	LBE98
			leau	-1,U
LBE98:	lda	#'+'
			ldb	V47
			beq	LBEBA
			bpl	LBEA3
			lda	#'-'
			negb
LBEA3:	sta	2,U
			lda	#'E'
			sta	1,U
			lda	#'0'-1
LBEAB:	inca
			subb	#10
			bcc	LBEAB
			addb	#'9'+1
			std	3,U
			clr	5,U
			bra	LBEBC
LBEB8:	sta	,U
LBEBA:	clr	1,U
LBEBC:	ldx	#STRBUF+3
			rts
LBEC0:	FCB	$80,$00,$00,$00,$00
LBEC5:	FCB	$FA,$0A,$1F,$00
LBEC9:	FCB	$00,$98,$96,$80
LBECD:	FCB	$FF,$F0,$BD,$C0
LBED1:	FCB	$00,$01,$86,$A0
LBED5:	FCB	$FF,$FF,$D8,$F0
LBED9:	FCB	$00,$00,$03,$E8
LBEDD:	FCB	$FF,$FF,$FF,$9C
LBEE1:	FCB	$00,$00,$00,$0A
LBEE5:	FCB	$FF,$FF,$FF,$FF

LBEE9:	lda	FP0EXP
			beq	LBEEF
			com	FP0SGN
LBEEF:	rts

LBEF0:	stx	COEFPT
			jsr	LBC2F
			bsr	LBEFC
			bsr	LBF01
			ldx	#V40
LBEFC:	jmp	LBACA

LBEFF:	stx	COEFPT
LBF01:	jsr	LBC2A
			ldx	COEFPT
			ldb	,X+
			stb	COEFCT
			stx	COEFPT
LBF0C:	bsr	LBEFC
			ldx	COEFPT
			leax	5,X
			stx	COEFPT
			jsr	LB9C2
			ldx	#V45
			dec	COEFCT
			bne	LBF0C
			rts

RND:		jsr	LBC6D
			bmi	LBF45
			beq	LBF3B
			bsr	LBF38
			jsr	LBC2F
			bsr	LBF3B
			ldx	#V40
			bsr	LBEFC
			ldx	#LBAC5
			jsr	LB9C2
LBF38:	jmp	INT

LBF3B:	ldx	RVSEED+1
			stx	FPA0
			ldx	RVSEED+3
			stx	FPA0+2
LBF45:	ldx	RSEED
			stx	FPA1
			ldx	RSEED+2
			stx	FPA1+2
			jsr	LBAD0
			ldd	VAD
			addd	#$658B
			std	RVSEED+3
			std	FPA0+2
			ldd	VAB
			adcb	#$B0
			adca	#$5
			std	RVSEED+1
			std	FPA0
			clr	FP0SGN
			lda	#$80
			sta	FP0EXP
			lda	FPA2+2
			sta	FPSBYT
			jmp	LBA1C
RSEED:	fdb	$40E6
			fdb	$4DAB

SIN:		jsr	LBC5F
			ldx	#LBFBD
			ldb	FP1SGN
			jsr	LBB89
			jsr	LBC5F
			bsr	LBF38
			clr	RESSGN
			lda	FP1EXP
			ldb	FP0EXP
			jsr	LB9BC
			ldx	#LBFC2
			jsr	LB9B9
			lda	FP0SGN
			pshs	A
			bpl	LBFA6
			jsr	LB9B4
			lda	FP0SGN
			bmi	LBFA9
			com	RELFLG
LBFA6:	jsr	LBEE9
LBFA9:	ldx	#LBFC2
			jsr	LB9C2
			puls	A
			tsta
			bpl	LBFB7
			jsr	LBEE9
LBFB7:	ldx	#LBFC7
			jmp	LBEF0

LBFBD:	FCB	$83,$49,$0F,$DA,$A2
LBFC2:	FCB	$7F,$00,$00,$00,$00

LBFC7:	FCB	6-1
LBFC8:	FCB	$84,$E6,$1A,$2D,$1B
LBFCD:	FCB	$86,$28,$07,$FB,$F8
LBFD2:	FCB	$87,$99,$68,$89,$01
LBFD7:	FCB	$87,$23,$35,$DF,$E1
LBFDC:	FCB	$86,$A5,$5D,$E7,$28
LBFE1:	FCB	$83,$49,$0F,$DA,$A2
