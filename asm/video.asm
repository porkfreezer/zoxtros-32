;  video.asm
;  This file has routines for the TMS9918A VDP, mostly relating to the
;  text mode. It creates a decent terminal for other programs
;
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;


VDP_VRAM = $8800
VDP_REG = $8801

BLACK = $1			; 000000
MED_GREEN = $2		; 0AAD1E
LT_GREEN = $3		; 34C84C
DRK_BLUE = $4		; 2D2BE3
LT_BLUE = $5		; 514BFB
DRK_RED = $6		; BD2925
CYAN = $7			; 1EE2EF
MED_RED = $8		; FB2C2B
LT_RED = $9			; FF5F4C
DRK_YELLOW = $A	; BDA22B
LT_YELLOW = $B		; D7B454
DRK_GREEN = $C		; 0A8C18
MAGENTA = $D		; AF329A
GRAY = $E			; B2B2B2
WHITE = $F			; FFFFFF

	include	vdp_macros.asm

vdp_delay:	; 16 cycles = 8µs
	lbrn	0
	rts

; Scroll up one line of text (destroys top)
text_scroll:
	pshs	D
	pshsw
	ldd	#40
@loop:
	vdp_read_address
	jsr	vdp_delay
	lde	VDP_VRAM
	subd	#40
	vdp_write_address
	anda	#$3F
	ste	VDP_VRAM
	addd	#41
	cmpd	#960
	blt	@loop
;
	ldd	#920
	std	VDP_CURSOR
	vdp_write_address
	anda	#$3F
	lde	#$80
	ste	VDP_VRAM
	lde	#' '
@loop2:
	lbrn	0
	ste	VDP_VRAM
	incd
	cmpd	#960
	blt	@loop2
;
	pulsw
	puls	D,PC

; Erase and move cursor back
text_backspace:
	pshs	D
	ldd	VDP_CURSOR
	decd
	std	VDP_CURSOR
	vdp_write_address
	lda	#$80
	sta	VDP_VRAM
	lda	#' '
	sta	VDP_VRAM
	puls	D,PC

; Print a null-terminated string
; X is pointer to string
text_print:
	pshs	A
@loop:
	lda	,X+
	beq	@end
	bsr	text_putc
	bra	@loop
@end:
	puls	A,PC

; Write a character to screen
; A is the character
text_putc:
	pshs	D
	pshs	A
	cmpa	#$0A
	bne	@real
	jsr	text_crlf
	puls	A
	puls	D,PC
@real:
	ldd	VDP_CURSOR
	vdp_write_address
	puls	A
	sta	VDP_VRAM
	lda	#$80
	sta	VDP_VRAM
	ldd	VDP_CURSOR
	incd
	std	VDP_CURSOR
	cmpd	#960
	blt	@onscreen
	jsr	text_scroll
@onscreen
	puls	D,PC

; New line of text
text_crlf:
	pshs	D
	pshsw
	ldd	VDP_CURSOR
	vdp_write_address
	lda	#' '
	sta	VDP_VRAM
	ldd	VDP_CURSOR
@loop:
	cmpd	#39
	ble	@cont
	subd	#40
	bra	@loop
@cont:
	tfr	D,B
	negb
	addb	#40
	pshs	D
	ldd	VDP_CURSOR
	addd	,S
	std	VDP_CURSOR
	cmpd	#960
	blt	@onscreen
	jsr	text_scroll
@onscreen
	vdp_write_address
	lda	#$80
	sta	VDP_VRAM
	puls	D
	pulsw
	puls	D,PC

; Initialize write-only VDP registers from X
; Uses A,B
vdp_init:
	ldb	#$80
@loop:
	lda	,X+
	sta	VDP_REG
	stb	VDP_REG
	incb
	cmpb	#$88
	bne	@loop
	rts
text_vdp_init:
	fcb	$00			; M3=0, no external VDP
	fcb	%10010000	; 16K, blanked, M1=1 M2=0
	fcb	$00			; Pattern name table at $0000
	fcb	$00			; Color table unused
	fcb	$01			; Pattern generator table at $0800
	fcb	$00			; Sprite atribute table unused
	fcb	$00			; Sprite pattern generator table unused
	fcb	$1F			; Black text, white background
g1_vdp_init:
	fcb	$00			; M3=0, no external VDP
	fcb	%10000010	; 16K, blanked, M1=0 M2=0, SIZE=1
	fcb	$05			; Pattern name table at $1400
	fcb	$80			; Color table at $2000
	fcb	$01			; Pattern generator table at $0800
	fcb	$20			; Sprite atribute table at $1000
	fcb	$00			; Sprite pattern generator table $0000
	fcb	$00			; Transparent background

; Set VRAM block X bytes long starting at D to zero
vdp_clear_vram:
	vdp_write_address
	clra
@l1:
	lbrn	0
	sta	VDP_VRAM
	leax	-1,X
	bne	@l1
	rts

; Copy memory from X through Y to D in VRAM
vdp_copy_vram:
	vdp_write_address
!
	jsr	vdp_delay
	lda	,X+
	sta	VDP_VRAM
	cmpr	X,Y
	bne	<
	rts


; Disable blanking
; Uses A
vdp_enable:
	ora	#%01000000
	sta	VDP_REG
	lda	#$81
	sta	VDP_REG
	rts

; Initialize name table to spaces
text_load_name:
	pshs	X,D
	clrd
	std	VDP_CURSOR
	vdp_write_address
	lda	#$80
	sta	VDP_VRAM
	ldx	#$03FF
	lda	#' '
@loop:
	jsr	vdp_delay
	sta	VDP_VRAM
	leax	-1,X
	bne	@loop
	puls	D,X,PC

; Load pattern table with text patterns
; Uses D,X
text_load_patterns:
	ldx	#text_patterns
	ldd	#$0800
	vdp_write_address
!
	jsr	vdp_delay
	lda	,X+
	sta	VDP_VRAM
	cmpx	#text_end_patterns
	bne	<
;  inverted patterns
	ldx	#text_patterns+8*$20
	ldd	#$0800+8*$A0
	vdp_write_address
!
	jsr	vdp_delay
	lda	,X+
	coma
	sta	VDP_VRAM
	cmpx	#text_end_patterns-8
	bne	<
	rts

; Invert the characters on line A
text_select:
	pshs	D
	ldb	#40
	mul
	pshs	D
	vdp_read_address
	ldb	#40
!
	adde	#0
	lda	VDP_VRAM
	pshs	A
	decb
	bne	<
	ldb	#40
;
	leas	40,S
	ldd	,S
	vdp_write_address
	ldb	#40
!
	coma
	lda	,-S
	adda	#$80
	sta	VDP_VRAM
	decb
	bne	<
	leas	42,S
	puls	D,PC

text_patterns:
  .byte $00,$00,$00,$FF,$FF,$00,$00,$00 ; lr
  .byte $18,$18,$18,$18,$18,$18,$18,$18 ; ud
  .byte $00,$00,$00,$F8,$F8,$18,$18,$18 ; ld
  .byte $00,$00,$00,$1F,$1F,$18,$18,$18 ; rd
  .byte $18,$18,$18,$F8,$F8,$00,$00,$00 ; lu
  .byte $18,$18,$18,$1F,$1F,$00,$00,$00 ; ur
  .byte $18,$18,$18,$FF,$FF,$18,$18,$18 ; lurd
  .byte $00,$00,$00,$00,$00,$00,$00,$00 ; Color 0
  .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF ; Color 1
  .byte $AA,$55,$AA,$55,$AA,$55,$AA,$55 ; Color 2 (checker)
  .byte $0A,$0A,$0A,$0A,$0A,$0A,$0A,$00 ; 0A
  .byte $0B,$0B,$0B,$0B,$0B,$0B,$0B,$00 ; 0B
  .byte $0C,$0C,$0C,$0C,$0C,$0C,$0C,$00 ; 0C
  .byte $0D,$0D,$0D,$0D,$0D,$0D,$0D,$00 ; 0D
  .byte $0E,$0E,$0E,$0E,$0E,$0E,$0E,$00 ; 0E
  .byte $0F,$0F,$0F,$0F,$0F,$0F,$0F,$00 ; 0F
  .byte $10,$10,$10,$10,$10,$10,$10,$00 ; 10
  .byte $11,$11,$11,$11,$11,$11,$11,$00 ; 11
  .byte $12,$12,$12,$12,$12,$12,$12,$00 ; 12
  .byte $13,$13,$13,$13,$13,$13,$13,$00 ; 13
  .byte $14,$14,$14,$14,$14,$14,$14,$00 ; 14
  .byte $15,$15,$15,$15,$15,$15,$15,$00 ; 15
  .byte $16,$16,$16,$16,$16,$16,$16,$00 ; 16
  .byte $17,$17,$17,$17,$17,$17,$17,$00 ; 17
  .byte $18,$18,$18,$18,$18,$18,$18,$00 ; 18
  .byte $19,$19,$19,$19,$19,$19,$19,$00 ; 19
  .byte $1A,$1A,$1A,$1A,$1A,$1A,$1A,$00 ; 1A
  .byte $1B,$1B,$1B,$1B,$1B,$1B,$1B,$00 ; 1B
  .byte $1C,$1C,$1C,$1C,$1C,$1C,$1C,$00 ; 1C
  .byte $1D,$1D,$1D,$1D,$1D,$1D,$1D,$00 ; 1D
  .byte $1E,$1E,$1E,$1E,$1E,$1E,$1E,$00 ; 1E
  .byte $1F,$1F,$1F,$1F,$1F,$1F,$1F,$00 ; 1F
;
  .byte $00,$00,$00,$00,$00,$00,$00,$00 ; ' '
  .byte $20,$20,$20,$00,$20,$20,$00,$00 ; !
  .byte $50,$50,$50,$00,$00,$00,$00,$00 ; "
  .byte $50,$50,$F8,$50,$F8,$50,$50,$00 ; #
  .byte $20,$78,$A0,$70,$28,$F0,$20,$00 ; $
  .byte $C0,$C8,$10,$20,$40,$98,$18,$00 ; %
  .byte $40,$A0,$A0,$40,$A8,$90,$68,$00 ; &
  .byte $20,$20,$40,$00,$00,$00,$00,$00 ; '
  .byte $20,$40,$80,$80,$80,$40,$20,$00 ; (
  .byte $20,$10,$08,$08,$08,$10,$20,$00 ; )
  .byte $20,$A8,$70,$20,$70,$A8,$20,$00 ; *
  .byte $00,$20,$20,$F8,$20,$20,$00,$00 ; +
  .byte $00,$00,$00,$00,$20,$20,$40,$00 ; ,
  .byte $00,$00,$00,$F8,$00,$00,$00,$00 ; -
  .byte $00,$00,$00,$00,$20,$20,$00,$00 ; .
  .byte $00,$08,$10,$20,$40,$80,$00,$00 ; /
  .byte $70,$88,$98,$A8,$C8,$88,$70,$00 ; 0
  .byte $20,$60,$20,$20,$20,$20,$70,$00 ; 1
  .byte $70,$88,$08,$30,$40,$80,$F8,$00 ; 2
  .byte $F8,$08,$10,$30,$08,$88,$70,$00 ; 3
  .byte $10,$30,$50,$90,$F8,$10,$10,$00 ; 4
  .byte $F8,$80,$F0,$08,$08,$88,$70,$00 ; 5
  .byte $38,$40,$80,$F0,$88,$88,$70,$00 ; 6
  .byte $F8,$08,$10,$20,$40,$40,$40,$00 ; 7
  .byte $70,$88,$88,$70,$88,$88,$70,$00 ; 8
  .byte $70,$88,$88,$78,$08,$10,$E0,$00 ; 9
  .byte $00,$00,$20,$00,$20,$00,$00,$00 ; :
  .byte $00,$00,$20,$00,$20,$20,$40,$00 ; ;
  .byte $10,$20,$40,$80,$40,$20,$10,$00 ; <
  .byte $00,$00,$F8,$00,$F8,$00,$00,$00 ; =
  .byte $40,$20,$10,$08,$10,$20,$40,$00 ; >
  .byte $70,$88,$10,$20,$20,$00,$20,$00 ; ?
  .byte $70,$88,$A8,$B8,$B0,$80,$78,$00 ; @
  .byte $20,$50,$88,$88,$F8,$88,$88,$00 ; A
  .byte $F0,$88,$88,$F0,$88,$88,$F0,$00 ; B
  .byte $70,$88,$80,$80,$80,$88,$70,$00 ; C
  .byte $F0,$88,$88,$88,$88,$88,$F0,$00 ; D
  .byte $F8,$80,$80,$F0,$80,$80,$F8,$00 ; E
  .byte $F8,$80,$80,$F0,$80,$80,$80,$00 ; F
  .byte $78,$80,$80,$80,$98,$88,$78,$00 ; G
  .byte $88,$88,$88,$F8,$88,$88,$88,$00 ; H
  .byte $70,$20,$20,$20,$20,$20,$70,$00 ; I
  .byte $08,$08,$08,$08,$08,$88,$70,$00 ; J
  .byte $88,$90,$A0,$C0,$A0,$90,$88,$00 ; K
  .byte $80,$80,$80,$80,$80,$80,$F8,$00 ; L
  .byte $88,$D8,$A8,$A8,$88,$88,$88,$00 ; M
  .byte $88,$88,$C8,$A8,$98,$88,$88,$00 ; N
  .byte $70,$88,$88,$88,$88,$88,$70,$00 ; O
  .byte $F0,$88,$88,$F0,$80,$80,$80,$00 ; P
  .byte $70,$88,$88,$88,$A8,$90,$68,$00 ; Q
  .byte $F0,$88,$88,$F0,$A0,$90,$88,$00 ; R
  .byte $70,$88,$80,$70,$08,$88,$70,$00 ; S
  .byte $F8,$20,$20,$20,$20,$20,$20,$00 ; T
  .byte $88,$88,$88,$88,$88,$88,$70,$00 ; U
  .byte $88,$88,$88,$88,$50,$50,$20,$00 ; V
  .byte $88,$88,$88,$A8,$A8,$D8,$88,$00 ; W
  .byte $88,$88,$50,$20,$50,$88,$88,$00 ; X
  .byte $88,$88,$50,$20,$20,$20,$20,$00 ; Y
  .byte $F8,$08,$10,$20,$40,$80,$F8,$00 ; Z
  .byte $F8,$C0,$C0,$C0,$C0,$C0,$F8,$00 ; [
  .byte $00,$80,$40,$20,$10,$08,$00,$00 ; \
  .byte $F8,$18,$18,$18,$18,$18,$F8,$00 ; ]
  .byte $00,$00,$20,$50,$88,$00,$00,$00 ; ^
  .byte $00,$00,$00,$00,$00,$00,$F8,$00 ; _
  .byte $40,$20,$10,$00,$00,$00,$00,$00 ; `
  .byte $00,$00,$70,$88,$88,$98,$68,$00 ; a
  .byte $80,$80,$F0,$88,$88,$88,$F0,$00 ; b
  .byte $00,$00,$78,$80,$80,$80,$78,$00 ; c
  .byte $08,$08,$78,$88,$88,$88,$78,$00 ; d
  .byte $00,$00,$70,$88,$F8,$80,$78,$00 ; e
  .byte $30,$40,$E0,$40,$40,$40,$40,$00 ; f
  .byte $00,$00,$70,$88,$F8,$08,$F0,$00 ; g
  .byte $80,$80,$F0,$88,$88,$88,$88,$00 ; h
  .byte $00,$40,$00,$40,$40,$40,$40,$00 ; i
  .byte $00,$20,$00,$20,$20,$A0,$60,$00 ; j
  .byte $00,$80,$80,$A0,$C0,$A0,$90,$00 ; k
  .byte $C0,$40,$40,$40,$40,$40,$60,$00 ; l
  .byte $00,$00,$D8,$A8,$A8,$A8,$A8,$00 ; m
  .byte $00,$00,$F0,$88,$88,$88,$88,$00 ; n
  .byte $00,$00,$70,$88,$88,$88,$70,$00 ; o
  .byte $00,$00,$70,$88,$F0,$80,$80,$00 ; p
  .byte $00,$00,$F0,$88,$78,$08,$08,$00 ; q
  .byte $00,$00,$70,$88,$80,$80,$80,$00 ; r
  .byte $00,$00,$78,$80,$70,$08,$F0,$00 ; s
  .byte $40,$40,$F0,$40,$40,$40,$30,$00 ; t
  .byte $00,$00,$88,$88,$88,$88,$78,$00 ; u
  .byte $00,$00,$88,$88,$90,$A0,$40,$00 ; v
  .byte $00,$00,$88,$88,$88,$A8,$D8,$00 ; w
  .byte $00,$00,$88,$50,$20,$50,$88,$00 ; x
  .byte $00,$00,$88,$88,$78,$08,$F0,$00 ; y
  .byte $00,$00,$F8,$10,$20,$40,$F8,$00 ; z
  .byte $38,$40,$20,$C0,$20,$40,$38,$00 ; {
  .byte $40,$40,$40,$00,$40,$40,$40,$00 ; |
  .byte $E0,$10,$20,$18,$20,$10,$E0,$00 ; }
  .byte $40,$A8,$10,$00,$00,$00,$00,$00 ; ~
  .byte $A8,$50,$A8,$50,$A8,$50,$A8,$00 ; checkerboard
  .byte $F8,$F8,$F8,$F8,$F8,$F8,$F8,$00 ; cursor
text_end_patterns:
