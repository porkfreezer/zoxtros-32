;  keyboard.asm
;  This file has routines that scan the keyboard on an interrupt, and 
;  retrieve the results.
;  
;  Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
;  
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; either version 2 of the License, or
;  (at your option) any later version.
;  
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;  
;  You should have received a copy of the GNU General Public License
;  along with this program; if not, write to the Free Software
;  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;  MA 02110-1301, USA.
;  
;  



; Wait for a key to be pressed, and store the ASCII key in KEY
key_getkey:
	pshs	A
	lda	#$FF
	sta	KEY_STATE
@l1:
	tst	KEY_STATE
	bmi	@l1
	puls	A,PC

; Get line of input from keyboard
; Stores string at original X. X now points to end of string
key_getline:
	pshs	A,B,Y
	clrb
	lda	#$FF
	sta	KEY_STATE
@loop:
	tst	KEY_STATE
	bmi	@loop
	lda	#$FF
	sta	KEY_STATE
	lda	KEY
	cmpa	#$0A
	beq	@end
	cmpa	#$7F
	bne	@break
	tstb
	bge	@loop
	jsr	text_backspace
	leax	-1,X
	incb
	bra	@loop
@break:
	cmpa	#3
	orcc	#1
	beq	@endbreak
@clear:
	cmpa	#$80
	bne	@real
	tstb
	beq	@loop
	leax	B,X
	lda	#$FF
	tfr	D,Y
	pshs	B
	ldd	VDP_CURSOR
	addr	Y,D
	std	VDP_CURSOR
	vdp_write_address
	puls	B
	lda	#$80
	sta	VDP_VRAM
	lda	#' '
@l2:
	sta	VDP_VRAM
	incb
	bne	@l2
	bra	@loop
@real:
	sta	,X+
	jsr	text_putc
	decb
	bra	@loop
@end:
	clra
@endbreak:
	pshs	CC
	jsr	text_crlf
	clra
	sta	,X
	coma
	sta	KEY_STATE
	puls	CC
	puls	A,B,Y,PC
	
; NMI interrupt to scan the keyboard
key_scan:
	lda	VIA_T1CL		; clear interrupt
	lda	VIA_DDRA
	anda	#%00000011
	sta	I2C
	lda	#%100
	tfr	A,F
	ora	I2C
	sta	VIA_DDRA
	ldy	#KEY_BUFFER
	ldb	VIA_PORTB
	stb	TMPB
	comb
	andb	#%01100000
	lslb
	lslb
	rolb
	rolb
	tfr	B,E
	ldx	#keys-4
	lda	#%00100000
	bsr	@row
	ldb	TMPB
	stb	,Y+
!
	tfr	F,A
	lsla
	tfr	A,F
	beq	@end
	ora	I2C
	sta	VIA_DDRA
	lda	#%10000000
	ldb	VIA_PORTB
	stb	TMPB
	bsr	@row
	ldb	TMPB
	stb	,Y		; Bizarrely, stb ,Y+ 
	leay	1,Y	; does NOT work
	bra	<
@end:
;
; Joystick
	lda	$9800
	eora	#$0C	; flip X bits
	ldx	#JOYSTICK
	pshs	A
!
	lda	,S
	anda	#1
	sta	,X+
	lsr	,S
	cmpx	#JOY_BUTTON+1
	bne	<
	leas	1,S
;
	rti
;
@row:
	lsra
	beq	@rowend
	leax	4,X
	ldb	TMPB
	andr	A,B
	bne	@row
	eorb	,Y
	andr	A,B
	beq	@row
	tst	KEY_STATE
	bpl	@row
	ldb	E,X
	beq	@row
	stb	KEY
	inc	KEY_STATE
	bra	@row
@rowend:
	rts
	
keys:
;	A2
;	shift
;	fn
	fcb	'1',0,'!',0
	fcb	'u','-','U','_'
	fcb	't',0,'T',0
	fcb	'e',0,'E',0
	fcb	'q',0,'Q',0
;	A3
	fcb	'7',0,'&',0
	fcb	'8',0,'*',0
	fcb	'2',0,'@',0
	fcb	'i','=','I','+'
	fcb	'y',0,'Y',0
	fcb	'r',0,'R',0
	fcb	'w',0,'W',0
;	A4
	fcb	$7F,0,$7F,$80		; Backspace & clear
	fcb	'9',0,'(',0
	fcb	'3',0,'#',0
	fcb	'j',0,'J',0
	fcb	'g',0,'G',0
	fcb	'd','`','D','~'
	fcb	'a',0,'A',0
;	A5
	fcb	' ',' ',' ',' '
	fcb	'0',0,')',0
	fcb	'4',0,'$',0
	fcb	'k','\','K','|'
	fcb	'h',0,'H',0
	fcb	'f',0,'F',0
	fcb	's',0,'S',0
;	A6
	fcb	'l',''','L','"'
	fcb	'o','[','O','{'
	fcb	'5',0,'%',0
	fcb	'm','/','M','?'
	fcb	'b','<','B',0
	fcb	'c',3,'C',3			; Break
	fcb	'z',';','Z',':'
;	A7
	fcb	$0A,0,0,0			; Enter
	fcb	'p','[','P','{'
	fcb	'6',0,'^',0
	fcb	'.',0,',',0
	fcb	'n','>','N',0
	fcb	'v',0,'V',0
	fcb	'x',0,'X',0
