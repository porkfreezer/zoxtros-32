	include	vdp_macros.asm
	include	kernel.inc

	setdp	0

SPRPAT = $0000
PAT = $0800
NAME = $1400
PLAYER = $1000
COLOR = $2000

	org	PROGRAM_START
PX:			rmb	1
TICK:			rmb	1
SHOOTING:	rmb	1	; 0xFF if player shooting
SHOTX:		rmb	1
SHOTY:		rmb	1
ESHOT1:		rmb	1
ESHOT1X:		rmb	1
ESHOT1Y:		rmb	1
ESHOT2:		rmb	1
ESHOT2X:		rmb	1
ESHOT2Y:		rmb	1
BUFFER:		rmb	32
BUFFER_END:
TICK_MAX:	rmb	1
WALK_POS:	rmb	1
WALK_DIR:	rmb	1
ENEMY_TOP:	rmb	2
ENEMY_BOT:	rmb	2
ROW_OK:		rmb	5
TMP:			rmb	2

	org	invaders

start:
	ldx	#g1_vdp_init
	jsr	vdp_init
	ldd	#$0800
	ldx	#$0F00
	jsr	vdp_clear_vram

	clrd
	vdp_write_address
	ldx	#sprite_patterns
!
	lda	,X+
	sta	VDP_VRAM
	cmpx	#sprite_patterns_end
	bne	<

	ldd	#$1000
	vdp_write_address
	ldx	#sprite_attr
!
	lda	,X+
	sta	VDP_VRAM
	cmpx	#sprite_attr_end
	bne	<
	lda	#$D0
	sta	VDP_VRAM

	ldd	#COLOR
	vdp_write_address
	lda	#$F0		; fg: white, bg: black
	sta	VDP_VRAM
	sta	VDP_VRAM
	ldd	#$201E	; fg: med green, bg: black; $1E entries
!
	lbrn	0
	sta	VDP_VRAM
	decb
	bne	<

	ldd	#PAT+8*4
	ldx	#patterns
	vdp_write_address
!
	lda	,X+
	sta	VDP_VRAM
	cmpx	#patterns_end
	bne	<
	bra	@l2
@rows:	fcb	8,4,4,12,12
@l2:
	ldd	#NAME+32*3+5
	ldx	#@rows
@l1:
	vdp_write_address
	anda	#$3F
	pshs	D
	lde	#22
	lda	,X+
!
	sta	VDP_VRAM
	eora	#1
	dece
	bne	<
	puls	D
	addd	#64
	cmpx	#@l2
	bne	@l1

	ldd	#NAME+32*17+6
	vdp_write_address
	ldx	#shelters
!
	lda	,X+
	sta	VDP_VRAM
	cmpx	#shelters_end
	bne	<

	lda	#%10000010
	jsr	vdp_enable

	ldd	#$0501
	sta	<WALK_POS
	stb	<WALK_DIR
	ldd	#$7846
	sta	<PX
	stb	<TICK_MAX
	clr	<TICK
	clr	<SHOOTING
	clr	<ESHOT1
	clr	<ESHOT2
	ldd	#NAME+11*32
	std	<ENEMY_BOT
	ldd	#NAME+32
	std	<ENEMY_TOP
	ldd	#$0006
	ldx	#ROW_OK
!
	sta	,X+
	decb
	bne	<

game_loop:
	lda	<JOY_RIGHT
	beq	>
	ldb	<PX
	cmpb	#216
	beq	>
	lsla
	adda	<PX
	bra	stpos
!
	lda	<JOY_LEFT
	beq	>
	ldb	<PX
	cmpb	#34
	beq	>
	lsla
	nega
	adda	<PX
	bra	stpos
!
	lda	PX
stpos:
	sta	<PX
	ldd	#PLAYER+1
	vdp_write_address
	lda	<PX
	sta	VDP_VRAM

	ldu	#SHOOTING
	clra
	pshs	A
	ldb	#1
	jsr	shot
	bne	@endshot
;
	tst	JOY_TRIGGER
	beq	@endshot
	com	<SHOOTING
	lda	<PX
	adda	#6
	sta	<SHOTX
	ldb	#160
	stb	<SHOTY
	tfr	D,W
	clra
	ldb	2,S
	addd	#PLAYER+4
	vdp_write_address
	stf	VDP_VRAM
	jsr	vdp_delay
	ste	VDP_VRAM
@endshot:
	puls	A
	clr	<TMP
!	leau	3,U
	clrb
	adda	#4
	pshs	A
	jsr	shot
	puls	A
	com	<TMP
	bne	<

	ldd	#$3000
!
	decd
	bne	<
	lda	<TICK
	adda	#2
	cmpa	<TICK_MAX
	blo	>
	clra
!	sta	<TICK
	lbne	game_loop

	ldd	<ENEMY_BOT
	pshs	D

	ldf	#1
	lda	<WALK_POS
	adda	<WALK_DIR
	cmpa	#10
	beq	>
	cmpa	#2
	beq	>
	bra	@l1
!	neg	<WALK_DIR
	ldf	#33
	pshs	A
	ldd	<ENEMY_BOT
	addd	#32
	std	<ENEMY_BOT
	ldd	<ENEMY_TOP
	addd	#32
	std	<ENEMY_TOP
	puls	A
@l1:
	sta	<WALK_POS

	ldu	#ROW_OK
	clre
l1:
	ldd	,S
	tst	E,U
	lbne	@l2
	vdp_read_address
	ldx	#BUFFER
	clrb
@l1:
	jsr	vdp_delay
	lda	VDP_VRAM
	beq	>
	eora	#2
	ldb	#1
!
	sta	,X+
	cmpx	#BUFFER_END
	bne	@l1
	tstb
	bne	>
	com	E,U
!
	ldx	#BUFFER
	ldd	,S
	tst	<WALK_DIR
	bpl	>
	cmpf	#1
	bgt	>
	decd
!
	vdp_write_address
	clra
	tst	<WALK_DIR
	bmi	>
	cmpf	#1
	bgt	>
	sta	VDP_VRAM
	jsr	vdp_delay
!
	pshsW
!
	decf
	beq	>
	sta	VDP_VRAM
	jsr	vdp_delay
	bra	<
!
	pulsW
!
	lda	,X+
	sta	VDP_VRAM
	jsr	vdp_delay
	cmpx	#BUFFER_END-1
	bne	<
	tst	<WALK_DIR
	bpl	>
	clra
	sta	VDP_VRAM
!
@l2:
	ince
	ldd	#-64
	addd	,S
	std	,S
	cmpf	#1
	beq	>
	addd	#32
!	cmpe	#5
	lbne	l1

	puls	D

	ldd	<ENEMY_BOT
	subd	#NAME
	lsrd
	lsrd
	andb	#$F8
	addb	#8
	ldu	#ESHOT1
	tst	,U
	beq	>
	leau	3,U
!	lbne	game_loop
	com	,U
	stb	2,U
	jsr	random
	sta	1,U

	jmp	game_loop


; Process a shot
;  U - Address of shot RAM vars (shooting ? !0 : 0, xpos, ypos)
;  A -
;  B -
;  2,S -
shot:
	tst	,U
	bne	>
	rts
!	lbpl	@explodeshot
	lda	2,U
	tstb
	beq	>
	suba	#12
!	adda	#4
	sta	2,U
	cmpa	#160
	blo	>
	cmpa	#191
	bhi	@startexploding
	ldb	1,U
	subb	<PX
	bmi	>
	cmpb	#16
	bgt	>
	jmp	RES				; Hit player
@startexploding:			; Switch pattern to explosion
	clra
	ldb	2,S				; Sprite number
	addd	#PLAYER+4+1		; Add SAT offset
	jsr	vdp_delay
	vdp_write_address
	lda	1,U
	suba	#4
	sta	VDP_VRAM
	lda	#8
	jsr	vdp_delay
	sta	VDP_VRAM
	sta	,U
	rts
!
	tfr	A,E
	clra
	ldb	2,S
	addd	#PLAYER+4
	vdp_write_address
	ste	VDP_VRAM
	tfr	E,B
	lda	1,U
	sta	VDP_VRAM
	andb	#$F8
	clra
	lsld
	lsld
	pshs	D
	clra
	ldb	1,U
	lsrb
	lsrb
	lsrb
	addd	,S++
	addd	#NAME
	tfr	D,W
	vdp_read_address
	jsr	vdp_delay
	lda	VDP_VRAM
	lbeq	@endshot
	cmpa	#16
	bhs	@shelter
	ldb	1,U
	andb	#7
	bita	#1
	beq	>
	cmpb	#5
	lbhi	@endshot
	decw
	bra	@l3
!	cmpb	#3
	lblo	@endshot
@l3:
	lda	<TICK_MAX
	beq	>
	deca
	sta	<TICK_MAX
	tfr	W,D
	vdp_write_address
	clra
	jsr	vdp_delay
	sta	VDP_VRAM
	jsr	vdp_delay
	sta	VDP_VRAM
	jmp	@startexploding
@shelter:
	inca
	bita	#1
	bne	>
	clra
!
	pshs	A
	tfr	W,D
	vdp_write_address
	puls	A
	sta	VDP_VRAM
	jsr	vdp_delay
	jmp	@startexploding
@explodeshot:
	dec	,U
	bne	@endshot
;
	clra
	ldb	2,S
	addd	#PLAYER+4
	vdp_write_address
	lda	#-10
	sta	VDP_VRAM
	jsr	vdp_delay
	sta	VDP_VRAM
	lda	#4
	jsr	vdp_delay
	sta	VDP_VRAM
@endshot:
	lda	#1
	rts

patterns:
; invaders/1a.png 4
	fcb	$08,$04,$0F,$1B,$3F,$2F,$28,$06
	fcb	$20,$40,$E0,$B0,$F8,$E8,$28,$C0
; invaders/1b.png 6
	fcb	$08,$24,$2F,$3B,$3F,$1F,$08,$10
	fcb	$20,$48,$E8,$B8,$F8,$F0,$20,$10
; invaders/2a.png 8
	fcb	$01,$03,$07,$0D,$0F,$02,$05,$0A
	fcb	$80,$C0,$E0,$B0,$F0,$40,$A0,$50
; invaders/2b.png 10
	fcb	$01,$03,$07,$0D,$0F,$05,$08,$04
	fcb	$80,$C0,$E0,$B0,$F0,$A0,$10,$20
; invaders/3a.png 12
	fcb	$03,$1F,$3F,$39,$3F,$0E,$19,$0C
	fcb	$C0,$F8,$FC,$9C,$FC,$70,$98,$30
; invaders/3b.png 14
	fcb	$03,$1F,$3F,$39,$3F,$06,$0D,$30
	fcb	$C0,$F8,$FC,$9C,$FC,$60,$B0,$0C
; shelter
	fcb	$0F,$1F,$3F,$7F,$FF,$FF,$FF,$FF	; ul
;	fcb	$0E,$1F,$3B,$7F,$EE,$FF,$BB,$FF
	fcb	$0A,$15,$2A,$55,$AA,$55,$AA,$55
	fcb	$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF	; um
;	fcb	$EE,$FF,$BB,$FF,$EE,$FF,$BB,$FF
	fcb	$AA,$55,$AA,$55,$AA,$55,$AA,$55
	fcb	$F0,$F8,$FC,$FE,$FF,$FF,$FF,$FF	; ur
;	fcb	$E0,$F8,$B8,$FE,$EE,$FF,$BB,$FF
	fcb	$A0,$50,$A8,$54,$AA,$55,$AA,$55
	fcb	$FF,$FF,$FF,$FF,$FF,$FE,$FC,$FC	; dl
;	fcb	$EE,$FF,$BB,$FF,$EE,$FE,$B8,$FC
	fcb	$AA,$55,$AA,$55,$AA,$54,$A8,$54
	fcb	$FF,$FF,$FF,$FF,$00,$00,$00,$00	; dm
;	fcb	$EE,$FF,$BB,$FF,$00,$00,$00,$00
	fcb	$AA,$55,$AA,$55,$00,$00,$00,$00
	fcb	$FF,$FF,$FF,$FF,$FF,$7F,$3F,$3F	; dr
;	fcb	$EE,$FF,$BB,$FF,$EE,$7F,$3B,$3F
	fcb	$AA,$55,$AA,$55,$AA,$55,$2A,$15

patterns_end:
sprite_patterns:
; invaders/player.png
	fcb	$02,$07,$07,$7F,$FF,$FF,$FF,$FF
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
	fcb	$00,$00,$00,$F0,$F8,$F8,$F8,$F8
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
; invaders/shot.png
	fcb	$80,$80,$80,$80,$80,$00,$00,$00
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
; invaders/shot_exploded.png
	fcb	$89,$22,$7E,$FF,$FF,$7E,$24,$91
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
	fcb	$00,$00,$00,$00,$00,$00,$00,$00
	fcb	$00,$00,$00,$00,$00,$00,$00,$00

sprite_patterns_end:
sprite_attr:
	fcb	160,120,0,$2
	fcb	-10,0,4,$F
	fcb	-10,0,4,$F
	fcb	-10,0,4,$F
	fcb	$D0
sprite_attr_end:
shelters:
	fcb	16,18,20,0,0,0,16,18,20,0,0,0,16,18,20,0,0,0,16,18,20,0,0,0,0,0
	fcb	0,0,0,0,0,0,22,24,26,0,0,0,22,24,26,0,0,0,22,24,26,0,0,0,22,24,26
shelters_end:

