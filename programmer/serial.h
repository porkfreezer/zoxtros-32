/*
 * serial.h - A (too) simple buffering serial library for the ATmega328
 * 
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdint.h>
#include <avr/pgmspace.h>


// Initialize UART registers for 115200 baud
void uart_init();

// Write a byte w to output
void uart_write(uint8_t w);

// Returns the number of bytes buffered
uint8_t uart_ready();

// Read a byte from the FIFO buffer, or 0 if none are available
uint8_t uart_read();

// Parse an unsigned integer from serial input with the specified 
// number of digits (this is a fast, lame library, so zero padded)
//  max_tens: the tens value of the maximum digit (eg 3 digits => 100)
uint64_t uart_read_uint(uint64_t max_tens);

// Wait until a byte is ready, and read it
uint8_t uart_wait_read();

// Send a null-terminated string to serial output
void uart_print(char *w);

// Send a null-terminated string from program memory to serial output
void uart_print_P(PGM_P w);

// Print <num> as a string (just use the macro forms)
//  max_tens: the tens value of the maximum expected digit (eg 3 digits => 100)
void uart_print_uint(uint64_t num, uint64_t max_tens);

// self-explanatory macros for print
#define uart_print_u8(num) uart_print_uint(num, 100U)
#define uart_print_u16(num) uart_print_uint(num, 10000U)
#define uart_print_u32(num) uart_print_uint(num, 1000000000U)
#define uart_print_u64(num) uart_print_uint(num, 10000000000000000000U)
#define uart_read_u8(num) uart_read_uint(100U)
#define uart_read_u16(num) uart_read_uint(10000U)
#define uart_read_u32(num) uart_read_uint(1000000000U)
#define uart_read_u64(num) uart_read_uint(10000000000000000000U)

#endif
