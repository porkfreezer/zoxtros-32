/*
 * serial.c - A (too) simple buffering serial library for the ATmega328
 * 
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "serial.h"

// Must be power of 2
#define BUFFER_SIZE 512

static volatile uint8_t buf[BUFFER_SIZE];
static volatile uint8_t bufi = 0;
static uint8_t read = 0;

ISR(USART_RX_vect) {
	buf[bufi] = UDR0;
	bufi++;
	bufi &= BUFFER_SIZE - 1;
}

void uart_init() {
    UCSR0B = 0b10011000;
    UCSR0C = 0b110;
    UBRR0 = 8;
    sei();
}

void uart_write(uint8_t w) {
    while (!(UCSR0A & 32));
    UDR0 = w;
}

uint8_t uart_ready() {
    return (bufi - read) & (BUFFER_SIZE - 1);
}

uint8_t uart_read() {
	if (uart_ready() > 0) {
		uint8_t r = buf[read];
		read++;
		read &= BUFFER_SIZE - 1;
		return r;
	}
	return 0;
}

uint64_t uart_read_uint(uint64_t max_tens) {
	uint64_t val = 0;
	for (uint64_t i = max_tens; i > 0; i /= 10) {
		while (!uart_ready());
		char in = uart_read();
		val += (in - '0') * i;
		uart_write(in);
	}
	return val;
}

uint8_t uart_wait_read() {
	while (!uart_ready());
	return uart_read();
}

void uart_print(char *w) {
    int i = 0;
    while (1) {
        if (w[i] == '\0')
            break;
        uart_write(w[i]);
        i++;
    }
}

void uart_print_P(PGM_P w) {
	for (uint16_t i = 0; ; i++) {
		char l = pgm_read_byte(w + i);
		if (l == '\0')
			break;
		uart_write(l);
	}
}

void uart_print_uint(uint64_t num, uint64_t max_tens) {
    uint8_t zeros = 0;
    for (uint64_t i = max_tens; i > 0; i /= 10) {
        uint8_t digit = num / i;
        if (zeros || digit > 0 || i == 1) {
            zeros = 1;
            uart_write(digit + '0');
        }
        num -= digit * i;
    }
}

