/*
 * An Arduino programmer for my Zoxtros-32 computer using shift registers for the address lines
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <serial.h>
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#define OE		_BV(PC0)
#define SER		_BV(PC1)
#define SRCLK	_BV(PC2)
#define RCLK	_BV(PC3)
#define RW		_BV(PC4)
#define E		_BV(PC5)

char buf[50];

uint8_t connected = 0;
void connect(uint8_t state) {
	connected = state;
	if (state) {
		PORTC = RW;
		DDRC = 0b111111;
	} else {
		PORTC = OE;
		DDRC = 0b001111;
		DDRB = 0;
		PORTB = 0;
		DDRD &= ~0b11000000;
		PORTB &= ~0b11000000;
	}
}

void set_address(uint16_t address) {
	PORTC &= ~RCLK;
	for (uint8_t i = 16; i > 0; i--) {
		PORTC &= ~SRCLK;
		PORTC &= ~0b10;
		PORTC |= (address & 1) << 1;
		address >>= 1;
		PORTC |= SRCLK;
		__builtin_avr_delay_cycles(3);
	}
	PORTC |= RCLK;
	PORTC &= ~OE;
}

uint8_t read(uint16_t address) {
	DDRB = 0;
	PORTB = 0;
	DDRD &= ~0b11000000;
	PORTB &= ~0b11000000;
	set_address(address);
	PORTC |= RW;
	PORTC |= E;
	__builtin_avr_delay_cycles(5);
	uint8_t data = (PINB & 0b111111) | (PIND & 0b11000000);
	PORTC &= ~E;
	return data;
}

void write(uint16_t address, uint8_t data) {
	set_address(address);
	DDRB = 0b111111;
	DDRD |= 0b11000000;
	PORTB = data & 0b111111;
	PORTD &= ~0b11000000;
	PORTD |= data & 0b11000000;
	PORTC &= ~RW;
	PORTC |= E;
	__builtin_avr_delay_cycles(5);
	PORTC &= ~E;
	DDRB = 0;
	PORTB = 0;
	DDRD &= ~0b11000000;
	PORTD &= ~0b11000000;
}

uint16_t parse_hex(char *hex, uint8_t digits) {
	uint16_t r = 0;
	uint16_t multiplier = 1;
	for (uint8_t i = digits; i > 0; i--) {
		uint16_t value = hex[i-1];
		if (value >= '0' && value <= '9')
			value -= '0';
		else if (value >= 'a' && value <= 'f')
			value -= 'a' - 10;
		else if (value >= 'A' && value <= 'F')
			value -= 'A' - 10;
		value *= multiplier;
		multiplier *= 16;
		r += value;
	}
	return r;
}

int main(void) {
	uart_init();
	uint16_t address = 0;
//	reset:
	connect(0);
	while (1) {
		uart_print("\n\r: ");
		for (uint8_t i = 0; i < 49; i++) {
			char in = uart_wait_read();
			if (in == '\r' || in == '\n') {
				buf[i] = '\0';
				break;
			}
			/*if (in == 3) {
				uart_print("\n\r");
				goto reset;
			}*/
			buf[i] = in;
		}
		// Set address
		if (buf[0] == 'a' && buf[1] == ' ') {
			address = parse_hex(&(buf[2]), 4);
			set_address(address);
			snprintf(buf, 50, "0x%04X\n\r", address);
			uart_print(buf);
		}
		// Read
		else if (buf[0] == 'r' && connected) {
			uint8_t data = read(address);
			snprintf(buf, 50, "0x%04X: 0x%02X = %hhu\n\r", address, data, data);
			uart_print(buf);
			address++;
		}
		// Test in ROM for bytes that are not a value
		else if (buf[0] == 't' && buf[1] == ' ' && connected) {
			uint8_t test = parse_hex(&(buf[2]), 2);
			for (uint16_t i = 0xC000; i != 0; i++) {
				uint8_t data = read(i);
				if (data != test) {
					snprintf(buf, 50, "0x%04X: 0x%02X = %hhu\n\r", i, data, data);
					uart_print(buf);
				}
				if (uart_ready() && uart_read() == 3)
					break;
			}
			address++;
		}
		// Write
		else if (buf[0] == 'w' && buf[1] == ' ' && connected) {
			uint8_t data = parse_hex(&(buf[2]), 2);
			write(address, data);
			snprintf(buf, 50, "0x%04X: 0x%02X = %hhu\n\r", address, data, data);
			uart_print(buf);
			address++;
		}
		// Fill RAM with value
		else if (buf[0] == 'f' && buf[1] == ' ' && connected) {
			uint8_t data = parse_hex(&(buf[2]), 2);
			for (uint16_t i = 0; i < 0x8000; i++) {
				write(i, data);
				if (!(i & 0xFF))
					uart_write('#');
				if (uart_ready() && uart_read() == 3)
					break;
			}
			uart_print("\n\r");
		}
		// Count zero bits left in EPROM
		else if (buf[0] == 'b' && connected) {
			uint32_t zeros = 0;
			for (uint16_t i = 0xC000; i != 0; i++) {
				uint8_t data = read(i);
				for (uint8_t mask = 1; mask != 0; mask <<= 1)
					zeros += !(data & mask);
			}
			snprintf(buf, 50, "%lu\n\r", zeros);
			uart_print(buf);
		}
		// Find the first byte in EPROM that is not fully erased (end)
		else if (buf[0] == 'e' && connected) {
			uint16_t end = 0xC000;
			for (uint16_t i = 0xC000; i != 0; i++) {
				if (read(i) != 0xFF)
					end = i;
			}
			snprintf(buf, 50, "0x%04X\n\r", end);
			uart_print(buf);
		}
		// Enter block writing mode. Only exited by reset
		// {16 bit big-endian address}{8 bit size}[data,...] {= from this}
		else if (buf[0] == 'w' && buf[1] == 'b' && connected) {
			while (1) {
				address = uart_wait_read() << 8;
				address |= uart_wait_read();
				uint8_t size = uart_wait_read();
				for ( ; size > 0; size--) {
					write(address, uart_wait_read());
					address++;
				}
				uart_write('=');
			}
		}
		// Disconnect
		else if (buf[0] == 'd')
			connect(0);
		// Connect
		else if (buf[0] == 'c')
			connect(1);

		else {
			uart_print(buf);
			uart_print("Invalid command\n\r");
		}
	}
}
