/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * ops.c
 * This file contains the incredibly long definitions of all the opcodes
 * in the 6309, as well as many helper functions.
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "6309.h"

uint8_t reg[19] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00};

uint8_t		*A  =             &reg[3];
uint8_t		*B  =             &reg[2];
uint16_t	*D  = (uint16_t *)&reg[2];
uint8_t		*E  =             &reg[1];
uint8_t		*F  =             &reg[0];
uint16_t	*W  = (uint16_t *)&reg[0];
uint32_t	*Q  = (uint32_t *)&reg[0];
uint16_t	*X  = (uint16_t *)&reg[4];
uint16_t	*Y  = (uint16_t *)&reg[6];
uint16_t	*U  = (uint16_t *)&reg[8];
uint16_t	*S  = (uint16_t *)&reg[10];
uint16_t	*V  = (uint16_t *)&reg[12];
uint16_t	*PC = (uint16_t *)&reg[14];
uint8_t		*DP =             &reg[16];
uint8_t		*CC =             &reg[17];
uint8_t		*ZERO =           &reg[18];

extern void clock_pulse(int);

#define CCH 5
#define CCN 3
#define CCZ 2
#define CCV 1
#define CCC 0

#define N(t) (((t) >> (7-CCN)) & B(CCN))
#define N16(t) (((t) >> (15-CCN)) & B(CCN))
#define Z(t) (((t) == 0) << CCZ)

#define HNZVC	0x2F
#define NZVC	0x0F
#define NZV		0x0E
#define NZC		0x0D
#define NZ		0x0C

#define NZV0(t) *CC &= ~NZV; *CC |= N(t) | Z(t)
#define NZV016(t) *CC &= ~NZV; *CC |= N16(t) | Z(t)
#define NZV032(t) *CC &= ~NZV; *CC |= ((t) >> (31-CCV)) | Z(t)

#define B(bit) (1 << (bit))
#define CCBIT(bit) ((*CC >> (bit)) & 1)

// Direct Address
#define DIR(a)	a = ((uint16_t)*DP << 8) | r(*PC); ++*PC
// Direct Address value
#define DIRV(d) d = r(((uint16_t)*DP << 8) | r(*PC)); ++*PC
#define DIRV16(d) d = r16(((uint16_t)*DP << 8) | r(*PC)); ++*PC
#define DIRV32(d) d = r32(((uint16_t)*DP << 8) | r(*PC)); ++*PC
// Extended Address
#define EXT(a) a = r16(*PC); *PC += 2
// Extended Address value
#define EXTV(d) d = r(r16(*PC)); *PC += 2
#define EXTV16(d) d = r16(r16(*PC)); *PC += 2
#define EXTV32(d) d = r32(r16(*PC)); *PC += 2
static uint16_t indexed(void) {
	uint16_t ret = 0;
	uint8_t post = r(*PC); ++*PC;
	uint8_t pmsk = post & 0b10001111;
	uint8_t rr = (post >> 5) & 0b11;
	uint16_t *R = X + rr;

	// special cases
	if (post == 0b10011111) {	// Extended indirect
		ret = r16(*PC);
		*PC += 2;
		ret = r16(ret);
		clock_pulse(4);
		return ret;
	}
	if (!(post >> 7)) {	// 5 bit offset
		int8_t offset = post & 0b00011111;
		uint8_t sign = offset >> 4;
		offset |= (sign << 5) | (sign << 6) | (sign << 7);
		ret = *R + offset;
		clock_pulse(1);
		return ret;
	}
	if (pmsk == 0b10001111 || pmsk == 0b10010000) {	// W
		switch (rr) {
			case 0b00:	ret = *W; 												break;	// No offset
			case 0b01:	ret = *W + (int16_t)r16(*PC); *PC += 2;	clock_pulse(2); break;	// 16 bit offset
			case 0b10:	ret = *W; ++*W; 						clock_pulse(1); break;	// Post-increment by 2
			case 0b11:	*W += 2; ret = *W; 						clock_pulse(1); break;	// Pre-decrement by 2
		}
	} else {
		switch (pmsk) {
			case 0b10000100:	ret = *R;														break;	// No offset
			// 5 bit offset is special
			case 0b10001000:	ret = *R +  (int8_t)  r(*PC); ++*PC;			clock_pulse(1); break;	// 8 bit offset
			case 0b10001001:	ret = *R + (int16_t)r16(*PC); *PC += 2;			clock_pulse(2); break;	// 16 bit offset

			case 0b10000110:	ret = *R +  (int8_t)*A;							clock_pulse(1); break;	// A offset
			case 0b10000101:	ret = *R +  (int8_t)*B; 						clock_pulse(1); break;	// B offset
			case 0b10001011:	ret = *R + (int16_t)*D;			 				clock_pulse(2); break;	// D offset
			case 0b10000111:	ret = *R +  (int8_t)(*E); 						clock_pulse(1); break;	// E offset
			case 0b10001010:	ret = *R +  (int8_t)(*F); 						clock_pulse(1); break;	// F offset
			case 0b10001110:	ret = *R + (int16_t)*W;			 				clock_pulse(1); break;	// W offset

			case 0b10000000:			ret = *R;	++*R; 						clock_pulse(1); break;	// Post-increment by 1
			case 0b10000001:			ret = *R;	*R+=2; 						clock_pulse(2); break;	// Post-increment by 2
			case 0b10000010:	--*R;	ret = *R; 								clock_pulse(1); break;	// Pre-decrement by 1
			case 0b10000011:	*R-=2;	ret = *R;		 						clock_pulse(2); break;	// Pre-decrement by 2

			case 0b10001100:	ret = *PC +  (int8_t)  r(*PC) + 1; ++*PC;		clock_pulse(1); break;	// PC 8 bit offset
			case 0b10001101:	ret = *PC + (int16_t)r16(*PC) + 2; *PC += 2;	clock_pulse(2); break;	// PC 16 bit offset
		}
	}
	if ((post >> 4) & 1) {
		clock_pulse(3);
		return r16(ret);
	} else {
		return ret;
	}
}
// Indexed Address
#define IND(a) a = indexed()
// Indexed Address value
#define INDV(d) d = r(indexed())
#define INDV16(d) d = r16(indexed())
#define INDV32(d) d = r32(indexed())
// Immediate
#define IMM(d) d = r(*PC); ++*PC
#define IMM16(d) d = r16(*PC); *PC += 2
#define IMM32(d) d = r32(*PC); *PC += 4

struct interreg_t {
	uint16_t src;
	void *src_reg;
	uint8_t src_width;
	uint16_t dst;
	void *dst_reg;
	uint8_t dst_width;
};

static void *ropcode(uint8_t code) {
	switch (code) {
		case 0x0:	return D;
		case 0x1:	return X;
		case 0x2:	return Y;
		case 0x3:	return U;
		case 0x4:	return S;
		case 0x5:	return PC;
		case 0x6:	return W;
		case 0x7:	return V;
		case 0x8:	return A;
		case 0x9:	return B;
		case 0xA:	return CC;
		case 0xB:	return DP;
		case 0xC:	return ZERO;
		case 0xD:	return ZERO;
		case 0xE:	return E;
		case 0xF:	return F;
		default:	return ZERO;
	}
}

static struct interreg_t interreg(uint8_t post) {
	uint8_t src = (post >> 4) & 0xF;
	uint8_t dst = post & 0xF;
	struct interreg_t ret;
	ret.src_reg = ropcode(src);
	ret.dst_reg = ropcode(dst);
	ret.src_width = ~(src >> 3) & 1;
	ret.dst_width = ~(dst >> 3) & 1;
	if (ret.dst_width) {
		ret.dst = *(uint16_t *)ret.dst_reg;
		if (ret.src_width) {
			ret.src = *(uint16_t *)ret.src_reg;
		} else {
			switch ((int)((void *)reg - ret.src_reg)) {
				case 3:	// A
				case 2: // B
					ret.src = *D;
					break;
				case 1: // E
				case 0: // F
					ret.src = *W;
					break;
				case 17: // CC
					ret.src = *CC;
					break;
				case 16: // DP
					ret.src = *DP << 8;
					break;
				default:
					ret.src = 0;
					break;
			}
		}
	} else {
		ret.dst = *(uint8_t *)ret.dst_reg;
		if (ret.src_width)
			ret.src = *(uint16_t *)ret.src_reg & 0xFF;
		else
			ret.src = *(uint8_t *)ret.src_reg;
	}
	return ret;
}
static struct interreg_t tfrexg(uint8_t post) {
	uint8_t src = (post >> 4) & 0xF;
	uint8_t dst = post & 0xF;
	struct interreg_t ret;
	ret.src_reg = ropcode(src);
	ret.dst_reg = ropcode(dst);
	ret.src_width = ~(src >> 3) & 1;
	ret.dst_width = ~(dst >> 3) & 1;
	if (ret.src_width) {
		if (ret.dst_width) { // 16 bit src and dst
			ret.src = *(uint16_t *)ret.src_reg;
			ret.dst = *(uint16_t *)ret.dst_reg;
		} else { // 16 bit src, 8 bit dst
			ret.src = (((uint8_t *)ret.dst_reg - reg) & 1) ? (*(uint16_t *)ret.src_reg >> 8) : (*(uint16_t *)ret.src_reg & 0xFF);
			ret.dst = (*(uint8_t *)ret.dst_reg << 8) | *(uint8_t *)ret.dst_reg;
		}
	} else {
		if (ret.dst_width) { // 8 bit src, 16 bit dst
			ret.src = (*(uint8_t *)ret.src_reg << 8) | *(uint8_t *)ret.src_reg;
			ret.dst = (((uint8_t *)ret.dst_reg - reg) & 1) ? (*(uint16_t *)ret.dst_reg >> 8) : (*(uint16_t *)ret.dst_reg & 0xFF);
		} else { // 8 bit src, 8 bit dst
			ret.src = *(uint8_t *)ret.src_reg;
			ret.dst = *(uint8_t *)ret.dst_reg;
		}
	}
	return ret;
}

struct bitop_t {
	uint8_t *r;
	uint8_t mask;
	uint8_t bit;
	uint16_t a;
};

static struct bitop_t bitop() {
	uint8_t post, m, mbit, rbit;
	struct bitop_t ret;
	IMM(post);
	DIR(ret.a);
	m = r(ret.a);
	switch (post >> 6) {
		case 0b00:	ret.r = CC; break;
		case 0b01:	ret.r = A;  break;
		case 0b10:	ret.r = B;  break;
		default:	ret.r = A; break;	// arbitrary, should be illegal or something
	}
	mbit = (post >> 3) & 7;
	rbit = post & 7;
	ret.mask = (1 << rbit);
	ret.bit = ((m >> mbit) & 1) << rbit;
	return ret;
}

static uint8_t	neg(uint8_t t) {
	*CC &= ~NZVC;
	t = 0-t;
	*CC |= N(t) | Z(t) | ((t == 0x80) << CCV) | ((t != 0) << CCC);
	return t;
}
static uint16_t	neg16(uint16_t t) {
	*CC &= ~NZVC;
	t = 0-t;
	*CC |= N16(t) | Z(t) | ((t == 0x8000) << CCV) | ((t != 0) << CCC);
	return t;
}
static void		oim(uint16_t ta, uint8_t t) {
	t |= r(ta);
	NZV0(t);
	w(ta, t);
}
static void		aim(uint16_t ta, uint8_t t) {
	t &= r(ta);
	NZV0(t);
	w(ta, t);
}
static uint8_t	com(uint8_t t) {
	t = ~t;
	NZV0(t);
	*CC |= B(CCC);
	return t;
}
static uint8_t	lsr(uint8_t t) {
	*CC &= ~NZC;
	*CC |= t & 1;
	t >>= 1;
	*CC |= Z(t);
	return t;
}
static uint16_t lsr16(uint16_t t) {
	*CC &= ~NZC;
	*CC |= t & 1;
	t >>= 1;
	*CC |= (t);
	return t;
}
static void		eim(uint16_t ta, uint8_t t) {
	t ^= r(ta);
	NZV0(t);
	w(ta, t);
}
static uint8_t	ror(uint8_t t) {
	uint8_t c = *CC & 1;
	*CC &= ~NZC;
	*CC |= t & 1;
	t = (t >> 1) | (c << 7);
	*CC |= N(t) | Z(t);
	return t;
}
static uint16_t	ror16(uint16_t t) {
	uint8_t c = *CC & 1;
	*CC &= ~NZC;
	*CC |= t & 1;
	t = (t >> 1) | (c << 15);
	*CC |= N16(t) | Z(t);
	return t;
}
static uint8_t	asr(int8_t t) {
	*CC &= ~NZC;
	*CC |= t & 1;
	t >>= 1;
	*CC |= N(t) | Z(t);
	return t;
}
static uint8_t	lsl(uint8_t t) {
	*CC &= ~NZVC;
	*CC |= (t >> 7) | (((t >> 7)^((t >> 6) & 1)) << CCV);
	t <<= 1;
	*CC |= N(t) | Z(t);
	return t;
}
static uint8_t	rol(uint8_t t) {
	uint8_t c = *CC & 1;
	*CC &= ~NZVC;
	*CC |= (t >> 7) | (((t >> 7)^((t >> 6) & 1)) << CCV);
	t = (t << 1) | c;
	*CC |= N(t) | Z(t);
	return t;
}
static uint16_t rol16(uint16_t t) {
	uint8_t c = *CC & 1;
	*CC &= ~NZVC;
	*CC |= (t >> 15) | (((t >> 15)^((t >> 14) & 1)) << CCV);
	t = (t << 1) | c;
	*CC |= N(t) | Z(t);
	return t;
}
static uint8_t	dec(uint8_t t) {
	*CC &= ~NZV;
	--t;
	*CC |= N(t) | Z(t) | ((t == 0x7F) << CCV);
	return t;
}
static void		tim(uint16_t ta, uint8_t t) {
	t &= r(ta);
	NZV0(t);
}
static uint8_t	inc(uint8_t t) {
	*CC &= ~NZV;
	++t;
	*CC |= N(t) | Z(t) | ((t == 0x80) << CCV);
	return t;
}
static uint8_t	clr(void) {
	*CC &= ~NZVC;
	*CC |= (1 << CCZ);
	return 0;
}
static uint8_t	daa(uint8_t t) {
	uint8_t lo = t & 0x0F;
	uint8_t hi = t >> 4;
	*CC &= ~NZC;
	if (CCBIT(CCH) | (lo > 9)) {
		lo += 6;
		*CC |= B(CCC);
	}
	if (CCBIT(CCC) | (hi > 9) | ((hi > 8) && (lo > 9)))
		hi += 6;
	uint8_t a = (hi << 4) | (lo & 0x0F);
	*CC |= N(a) | Z(a);
	return a;
}
static uint8_t	sub(uint8_t t, uint8_t t2, uint8_t carry) {
	*CC &= ~NZVC;
	uint16_t ret = t - t2 - carry;
	*CC |= N(ret) | Z(ret&0xFF) | (((t^t2) & (t^ret) & 0x80) >> (7-CCV)) | ((ret >> 8) & 1);
	return ret & 0xFF;
}
static uint16_t	sub16(uint16_t t, uint16_t t2, uint8_t carry) {
	*CC &= ~NZVC;
	uint32_t ret = t - t2 - carry;
	*CC |= N16(ret) | Z(ret&0xFFFF) | (((t^t2) & (t^ret) & 0x8000) >> (15-CCV)) | ((ret >> 16) & 1);
	return ret & 0xFFFF;
}
static uint8_t	add(uint8_t t, uint8_t t2, uint8_t carry) {
	*CC &= ~HNZVC;
	uint16_t ret = t + t2 + carry;
	*CC |= ((((t&0xF) + (t2&0xF) + carry) >= 0x10) >> (7-CCH)) | N(ret) | Z(ret&0xFF) | (((t^t2^0x80)&(t^ret)&0x80) >> (7-CCV)) | ((ret >> 8) & 1);
	return ret;
}
static uint16_t	add16(uint16_t t, uint16_t t2, uint8_t carry) {
	*CC &= ~NZVC;
	uint32_t ret = t + t2 + carry;
	*CC |= N16(ret) | Z(ret&0xFFFF) | (((t^t2^0x8000)&(t^ret)&0x8000) >> (7-CCV)) | ((ret >> 16) & 1);
	return ret;
}
static void		psh(uint16_t *reg, uint8_t post) {
	if (post & 0x80) {
		push16(reg, *PC);
		clock_pulse(2);
	}
	if (post & 0x40) {
		if (reg == S)
			push16(reg, *U);
		else
			push16(reg, *S);
		clock_pulse(2);
	}
	if (post & 0x20) {
		push16(reg, *Y);
		clock_pulse(2);
	}
	if (post & 0x10) {
		push16(reg, *X);
		clock_pulse(2);
	}
	if (post & 0x08) {
		push(reg, *DP);
		clock_pulse(1);
	}
	if (post & 0x04) {
		push(reg, *B);
		clock_pulse(1);
	}
	if (post & 0x02) {
		push(reg, *A);
		clock_pulse(1);
	}
	if (post & 0x01) {
		push(reg, *CC);
		clock_pulse(1);
	}
}
static void		pul(uint16_t *reg, uint8_t post) {
	if (post & 0x01) {
		pull(reg, *CC);
		clock_pulse(1);
	}
	if (post & 0x02) {
		pull(reg, *A);
		clock_pulse(1);
	}
	if (post & 0x04) {
		pull(reg, *B);
		clock_pulse(1);
	}
	if (post & 0x08) {
		pull(reg, *DP);
		clock_pulse(1);
	}
	if (post & 0x10) {
		pull16(reg, *X);
		clock_pulse(2);
	}
	if (post & 0x20) {
		pull16(reg, *Y);
		clock_pulse(2);
	}
	if (post & 0x40) {
		if (reg == S)
			pull16(reg, *U);
		else
			pull16(reg, *S);
		clock_pulse(2);
	}
	if (post & 0x80) {
		pull16(reg, *PC);
		clock_pulse(2);
	}
}
static void		divd(int8_t m) {
	if (m == 0) {
		// DIV0
		return;
	}
	*CC &= ~NZVC;
	int16_t res = (int16_t)*D / m;
	if (res > 0xFF) { // range overflow
		*CC |= B(CCV);
		clock_pulse(12);
		return;
	}
	int16_t d = *D;
	*B = res & 0xFF;
	*A = (d % m) & 0xFF;
	if ((int8_t)*B != res) { // two's complement overflow
		*CC |= N(*B) | B(CCV);
		clock_pulse(24);
		return;
	}
	*CC |= N(*B) | Z(*B) | (*B & 1);
	clock_pulse(25);
}
static void		divq(int16_t m) {
	if (m == 0) {
		// DIV0
		return;
	}
	*CC &= ~NZVC;
	int32_t res = (int32_t)*Q / m;
	if (res > 0xFFFF) { // range overflow
		*CC |= B(CCV);
		clock_pulse(13);
		return;
	}
	int32_t q = *Q;
	*W = res & 0xFFFF;
	*D = (q % m) & 0xFFFF;
	if ((int16_t)*W != res) { // two's complement overflow
		*CC |= N16(*W) | B(CCV);
		clock_pulse(34);
		return;
	}
	*CC |= N16(*W) | Z(*W) | (*W & 1);
	clock_pulse(34);
}
static void		muld(int16_t m) {
	*Q = (int16_t)*D * m;
	*CC &= ~NZ;
	*CC |= (*Q >> (31-CCN)) | Z(*Q);
}

void op_parse(uint8_t page, uint8_t instr) {
	// unimplemented: SYNC, CWAI, SWI, RTI, TFM, BITMD, LDMD
	uint16_t ta;
	uint8_t t;
	struct interreg_t ir;
	struct bitop_t b;
	if (instr >= 0x20 && instr <= 0x2F) {	// Branch instructions 0x20-0x2F and 0x1020-0x102F
		last_branch = *PC - (page ? 2 : 1);
		int16_t new;
		if (page == 0x10) {
			new = (int16_t)*PC + (int16_t)r16(*PC) + 2;
			*PC += 2;
			clock_pulse(2);
		} else {
			new = (int16_t)*PC + (int8_t)r(*PC) + 1;
			++*PC;
		}
		switch (instr) {
			case 0x20:	/* BRA */		*PC = new; break;
			case 0x21:	/* BRN */		break;
			case 0x22:	/* BHI */		if (!(CCBIT(CCZ) | CCBIT(CCC))) *PC = new; break;
			case 0x23:	/* BLS */		if (CCBIT(CCZ) | CCBIT(CCC)) *PC = new; break;
			case 0x24:	/* BHS/CC */	if (!CCBIT(CCC)) *PC = new; break;
			case 0x25:	/* BLO/CS */	if (CCBIT(CCC)) *PC = new; break;
			case 0x26:	/* BNE */		if (!CCBIT(CCZ)) *PC = new; break;
			case 0x27:	/* BEQ */		if (CCBIT(CCZ)) *PC = new; break;
			case 0x28:	/* BVC */		if (!CCBIT(CCV)) *PC = new; break;
			case 0x29:	/* BVS */		if (CCBIT(CCV)) *PC = new; break;
			case 0x2A:	/* BPL */		if (!CCBIT(CCN)) *PC = new; break;
			case 0x2B:	/* BMI */		if (CCBIT(CCN)) *PC = new; break;
			case 0x2C:	/* BGE */		if (CCBIT(CCN) == CCBIT(CCV)) *PC = new; break;
			case 0x2D:	/* BLT */		if (CCBIT(CCN) != CCBIT(CCV)) *PC = new; break;
			case 0x2E:	/* BGT */		if ((CCBIT(CCN) == CCBIT(CCV)) && !CCBIT(CCZ)) *PC = new; break;
			case 0x2F:	/* BLE */		if ((CCBIT(CCN) != CCBIT(CCV)) || CCBIT(CCZ)) *PC = new; break;
			default:					break;
		}
		clock_pulse(3);
	}
	else if (page == 0x10) {
		switch (instr) {
			case 0x30:	/* ADDR */			IMM(t); ir = interreg(t); t = *CC&0x20; if (ir.dst_width) *(uint16_t *)ir.dst_reg = add16(ir.dst, ir.src, 0);
																								else *(uint8_t *)ir.dst_reg = add(ir.dst, ir.src, 0);
											*CC |= t; clock_pulse(4); break;
			case 0x31:	/* ADCR */			IMM(t); ir = interreg(t); t = *CC&0x20; if (ir.dst_width) *(uint16_t *)ir.dst_reg = add16(ir.dst, ir.src, *CC&1);
																								else *(uint8_t *)ir.dst_reg = add(ir.dst, ir.src, *CC&1);
											*CC |= t; clock_pulse(4); break;
			case 0x32:	/* SUBR */			IMM(t); ir = interreg(t); if (ir.dst_width) *(uint16_t *)ir.dst_reg = sub16(ir.dst, ir.src, 0);
																					else *(uint8_t *)ir.dst_reg = sub(ir.dst, ir.src, 0);
											clock_pulse(4); break;
			case 0x33:	/* SBCR */			IMM(t); ir = interreg(t); if (ir.dst_width) *(uint16_t *)ir.dst_reg = sub16(ir.dst, ir.src, *CC&1);
																					else *(uint8_t *)ir.dst_reg = sub(ir.dst, ir.src, *CC&1);
											clock_pulse(4); break;
			case 0x34:	/* ANDR */			IMM(t); ir = interreg(t); if (ir.dst_width) { *(uint16_t *)ir.dst_reg = ir.dst & ir.src; NZV016(ir.dst&ir.src); }
																					else { *(uint8_t *)ir.dst_reg = ir.dst & ir.src; NZV0(ir.dst&ir.src); }
											clock_pulse(4); break;
			case 0x35:	/* ORR */			IMM(t); ir = interreg(t); if (ir.dst_width) { *(uint16_t *)ir.dst_reg = ir.dst | ir.src; NZV016(ir.dst|ir.src); }
																					else { *(uint8_t *)ir.dst_reg = ir.dst | ir.src; NZV0(ir.dst|ir.src); }
											clock_pulse(4); break;
			case 0x36:	/* EORR */			IMM(t); ir = interreg(t); if (ir.dst_width) { *(uint16_t *)ir.dst_reg = ir.dst ^ ir.src; NZV016(ir.dst^ir.src); }
																					else { *(uint8_t *)ir.dst_reg = ir.dst ^ ir.src; NZV0(ir.dst^ir.src); }
											clock_pulse(4); break;
			case 0x37:	/* CMPR */			IMM(t); ir = interreg(t); if (ir.dst_width) sub16(ir.dst, ir.src, 0);
																					else sub(ir.dst, ir.src, 0);
											clock_pulse(4); break;
			case 0x38:	/* PSHSW */			push16(S, *W); clock_pulse(6); break;
			case 0x3A:	/* PSHUW */			push16(U, *W); clock_pulse(6); break;
			case 0x39:	/* PULSW */			pull16(S, *W); clock_pulse(6); break;
			case 0x3B:	/* PULUW */			pull16(S, *W); clock_pulse(6); break;
//			case 0x3F:	/* SWI2 */			break;
			case 0x40:	/* NEGD */			*D = neg16(*D); clock_pulse(2); break;
			case 0x43:	/* COMD */			*D = ~*D; NZV016(*D); *CC |= B(CCC); clock_pulse(2); break;
			case 0x53:	/* COMW */			*W = ~*W; NZV016(*W); *CC |= B(CCC); clock_pulse(2); break;
			case 0x44:	/* LSRD */			*D = lsr16(*D); clock_pulse(2); break;
			case 0x54:	/* LSRW */			*W = lsr16(*W); clock_pulse(2); break;
			case 0x46:	/* RORD */			*D = ror16(*D); clock_pulse(2); break;
			case 0x56:	/* RORW */			*W = ror16(*W); clock_pulse(2); break;
			case 0x47:	/* ASRD */			*CC &= ~NZC; *CC |= *D & 1; *D = (int16_t)*D << 1; *CC |= N(*D) | Z(*D); clock_pulse(2);
			case 0x48:	/* LSLD */			*CC &= ~NZVC; *CC |= (((*D >> 15)^((*D >> 14) & 1)) << CCV)| (*D >> 15); *D <<= 1; *CC |= N16(*D) | Z(*D); clock_pulse(2); break;
			case 0x49:	/* ROLD */			*D = rol16(*D); clock_pulse(2); break;
			case 0x59:	/* ROLW */			*W = rol16(*W); clock_pulse(2); break;
			case 0x4A:	/* DECD */			--*D; NZV016(*D) | ((*D == 0x7FFF) << CCV); clock_pulse(2); break;
			case 0x5A:	/* DECW */			--*W; NZV016(*W) | ((*W == 0x7FFF) << CCV); clock_pulse(2); break;
			case 0x4C:	/* INCD */			++*D; NZV016(*D) | ((*D == 0x8000) << CCV); clock_pulse(2); break;
			case 0x5C:	/* INCW */			++*W; NZV016(*W) | ((*W == 0x8000) << CCV); clock_pulse(2); break;
			case 0x4D:	/* TSTD */			NZV016(*D); clock_pulse(2); break;
			case 0x5D:	/* TSTW */			NZV016(*W); clock_pulse(2); break;
			case 0x4F:	/* CLRD */			*D = 0; *CC &= 0xF0; *CC |= B(CCV); clock_pulse(2); break;
			case 0x5F:	/* CLRW */			*W = 0; *CC &= 0xF0; *CC |= B(CCV); clock_pulse(2); break;
			case 0x80:	/* SUBW immed */	IMM16(ta); *W = sub16(*W, ta, 0); clock_pulse(4); break;
			case 0x90:	/* SUBW direct */	DIRV16(ta); *W = sub16(*W, ta, 0); clock_pulse(5); break;
			case 0xA0:	/* SUBW indexed */	INDV16(ta); *W = sub16(*W, ta, 0); clock_pulse(6); break;
			case 0xB0:	/* SUBW extend */	EXTV16(ta); *W = sub16(*W, ta, 0); clock_pulse(6); break;
			case 0x81:	/* CMPW immed */	IMM16(ta); sub16(*W, ta, 0); clock_pulse(4); break;
			case 0x91:	/* CMPW direct */	DIRV16(ta); sub16(*W, ta, 0); clock_pulse(5); break;
			case 0xA1:	/* CMPW indexed */	INDV16(ta); sub16(*W, ta, 0); clock_pulse(6); break;
			case 0xB1:	/* CMPW extend */	EXTV16(ta); sub16(*W, ta, 0); clock_pulse(6); break;
			case 0x82:	/* SBCD immed */	IMM16(ta); *D = sub16(*D, ta, *CC&1); clock_pulse(4); break;
			case 0x92:	/* SBCD direct */	DIRV16(ta); *D = sub16(*D, ta, *CC&1); clock_pulse(5); break;
			case 0xA2:	/* SBCD indexed */	INDV16(ta); *D = sub16(*D, ta, *CC&1); clock_pulse(6); break;
			case 0xB2:	/* SBCD extend */	EXTV16(ta); *D = sub16(*D, ta, *CC&1); clock_pulse(6); break;
			case 0x83:	/* CMPD immed */	IMM16(ta); sub16(*D, ta, 0); clock_pulse(4); break;
			case 0x93:	/* CMPD direct */	DIRV16(ta); sub16(*D, ta, 0); clock_pulse(5); break;
			case 0xA3:	/* CMPD indexed */	INDV16(ta); sub16(*D, ta, 0); clock_pulse(6); break;
			case 0xB3:	/* CMPD extend */	EXTV16(ta); sub16(*D, ta, 0); clock_pulse(6); break;
			case 0x84:	/* ANDD immed */	IMM16(ta); *D &= ta; NZV016(*D); clock_pulse(4); break;
			case 0x94:	/* ANDD direct */	DIRV16(ta); *D &= ta; NZV016(*D); clock_pulse(5); break;
			case 0xA4:	/* ANDD indexed */	INDV16(ta); *D &= ta; NZV016(*D); clock_pulse(6); break;
			case 0xB4:	/* ANDD extend */	EXTV16(ta); *D &= ta; NZV016(*D); clock_pulse(6); break;
			case 0x85:	/* BITD immed */	IMM16(ta); NZV016(*D&ta); clock_pulse(4); break;
			case 0x95:	/* BITD direct */	DIRV16(ta); NZV016(*D&ta); clock_pulse(5); break;
			case 0xA5:	/* BITD indexed */	INDV16(ta); NZV016(*D&ta); clock_pulse(6); break;
			case 0xB5:	/* BITD extend */	EXTV16(ta); NZV016(*D&ta); clock_pulse(6); break;
			case 0x86:	/* LDW immed */		IMM16(*W); NZV016(*W); clock_pulse(4); break;
			case 0x96:	/* LDW direct */	DIRV16(*W); NZV016(*W); clock_pulse(5); break;
			case 0xA6:	/* LDW indexed */	INDV16(*W); NZV016(*W); clock_pulse(6); break;
			case 0xB6:	/* LDW extend */	EXTV16(*W); NZV016(*W); clock_pulse(6); break;
			case 0x97:	/* STW direct */	DIR(ta); w16(ta, *W); NZV016(*W); clock_pulse(5); break;
			case 0xA7:	/* STW indexed */	IND(ta); w16(ta, *W); NZV016(*W); clock_pulse(6); break;
			case 0xB7:	/* STW extend */	EXT(ta); w16(ta, *W); NZV016(*W); clock_pulse(6); break;
			case 0x88:	/* EORD immed */	IMM16(ta); *D ^= ta; NZV016(*D); clock_pulse(4); break;
			case 0x98:	/* EORD direct */	DIRV16(ta); *D ^= ta; NZV016(*D); clock_pulse(5); break;
			case 0xA8:	/* EORD indexed */	INDV16(ta); *D ^= ta; NZV016(*D); clock_pulse(6); break;
			case 0xB8:	/* EORD extend */	EXTV16(ta); *D ^= ta; NZV016(*D); clock_pulse(6); break;
			case 0x89:	/* ADCD immed */	IMM16(ta); *D = add16(*D, ta, *CC&1); clock_pulse(4); break;
			case 0x99:	/* ADCD direct */	DIRV16(ta); *D = add16(*D, ta, *CC&1); clock_pulse(5); break;
			case 0xA9:	/* ADCD indexed */	INDV16(ta); *D = add16(*D, ta, *CC&1); clock_pulse(6); break;
			case 0xB9:	/* ADCD extend */	EXTV16(ta); *D = add16(*D, ta, *CC&1); clock_pulse(6); break;
			case 0x8A:	/* ORD immed */		IMM16(ta); *D |= ta; NZV016(*D); clock_pulse(4); break;
			case 0x9A:	/* ORD direct */	DIRV16(ta); *D |= ta; NZV016(*D); clock_pulse(5); break;
			case 0xAA:	/* ORD indexed */	INDV16(ta); *D |= ta; NZV016(*D); clock_pulse(6); break;
			case 0xBA:	/* ORD extend */	EXTV16(ta); *D |= ta; NZV016(*D); clock_pulse(6); break;
			case 0x8B:	/* ADDW immed */	IMM16(ta); *W = add16(*W, ta, 0); clock_pulse(4); break;
			case 0x9B:	/* ADDW direct */	DIRV16(ta); *W = add16(*W, ta, 0); clock_pulse(5); break;
			case 0xAB:	/* ADDW indexed */	INDV16(ta); *W = add16(*W, ta, 0); clock_pulse(6); break;
			case 0xBB:	/* ADDW extend */	EXTV16(ta); *W = add16(*W, ta, 0); clock_pulse(6); break;
			case 0x8C:	/* CMPY immed */	IMM16(ta); sub16(*Y, ta, 0); clock_pulse(4); break;
			case 0x9C:	/* CMPY direct */	DIRV16(ta); sub16(*Y, ta, 0); clock_pulse(5); break;
			case 0xAC:	/* CMPY indexed */	INDV16(ta); sub16(*Y, ta, 0); clock_pulse(6); break;
			case 0xBC:	/* CMPY extend */	EXTV16(ta); sub16(*Y, ta, 0); clock_pulse(6); break;
			case 0x8E:	/* LDY immed */		IMM16(*Y); NZV016(*Y); clock_pulse(4); break;
			case 0x9E:	/* LDY direct */	DIRV16(*Y); NZV016(*Y); clock_pulse(5); break;
			case 0xAE:	/* LDY indexed */	INDV16(*Y); NZV016(*Y); clock_pulse(5); break;
			case 0xBE:	/* LDY extend */	EXTV16(*Y); NZV016(*Y); clock_pulse(6); break;
			case 0x9F:	/* STY direct */	DIR(ta); w16(ta, *Y); NZV016(*Y); clock_pulse(5); break;
			case 0xAF:	/* STY indexed */	IND(ta); w16(ta, *Y); NZV016(*Y); clock_pulse(6); break;
			case 0xBF:	/* STY extend */	EXT(ta); w16(ta, *Y); NZV016(*Y); clock_pulse(6); break;
			case 0xDC:	/* LDQ direct */	DIRV32(*Q); NZV032(*Q); clock_pulse(7); break;
			case 0xEC:	/* LDQ indexed */	INDV32(*Q); NZV032(*Q); clock_pulse(8); break;
			case 0xFC:	/* LDQ extend */	EXTV32(*Q); NZV032(*Q); clock_pulse(8); break;
			case 0xDD:	/* STQ direct */	DIR(ta); w32(ta, *Q); NZV032(*Q); clock_pulse(7); break;
			case 0xED:	/* STQ indexed */	IND(ta); w32(ta, *Q); NZV032(*Q); clock_pulse(8); break;
			case 0xFD:	/* STQ extend */	EXT(ta); w32(ta, *Q); NZV032(*Q); clock_pulse(8); break;
			case 0xCE:	/* LDS immed */		IMM16(*S); NZV016(*S); clock_pulse(4); break;
			case 0xDE:	/* LDS direct */	DIRV16(*S); NZV016(*S); clock_pulse(5); break;
			case 0xEE:	/* LDS indexed */	INDV16(*S); NZV016(*S); clock_pulse(6); break;
			case 0xFE:	/* LDS extend */	EXTV16(*S); NZV016(*S); clock_pulse(6); break;
			case 0xDF:	/* STS direct */	DIR(ta); w16(ta, *S); NZV016(*S); clock_pulse(5); break;
			case 0xEF:	/* STS indexed */	IND(ta); w16(ta, *S); NZV016(*S); clock_pulse(6); break;
			case 0xFF:	/* STS extend */	EXT(ta); w16(ta, *S); NZV016(*S); clock_pulse(6); break;
			default:	break;
		}
	} else if (page == 0x11) {
		switch (instr) {
			case 0x30:	/* BAND */			b = bitop(); *b.r &= (0xFF & ~b.mask) | b.bit; clock_pulse(6); break;
			case 0x31:	/* BIAND */			b = bitop(); *b.r &= (0xFF & ~b.mask) | ~b.bit; clock_pulse(6); break;
			case 0x32:	/* BOR */			b = bitop(); *b.r |= b.bit; clock_pulse(6); break;
			case 0x33:	/* BIOR */			b = bitop(); *b.r |= ~b.bit & b.mask; clock_pulse(6); break;
			case 0x34:	/* BEOR */			b = bitop(); *b.r ^= b.bit; clock_pulse(6); break;
			case 0x35:	/* BIEOR */			b = bitop(); *b.r ^= ~b.bit & b.mask; clock_pulse(6); break;
			case 0x36:	/* LDBT */			b = bitop(); *b.r = (*b.r & ~b.mask) | b.bit; clock_pulse(6); break;
			case 0x37:	/* STBT */			b = bitop(); w(b.a, (r(b.a) & ~b.mask) | b.bit); clock_pulse(7); break;
			case 0x38:	/* TFM r+,r+ */		*PC += 2; break;
			case 0x39:	/* TFM r-,r- */		*PC += 2; break;
			case 0x3A:	/* TFM r+,r */		*PC += 2; break;
			case 0x3B:	/* TFM r,r+ */		*PC += 2; break;
			case 0x3C:	/* BITMD */			++*PC; break;
			case 0x3D:	/* LDMD */			++*PC; break;
//			case 0x3F:	/* SWI3 */			break;
			case 0x43:	/* COME */			*E = com(*E); clock_pulse(2); break;
			case 0x53:	/* COMF */			*F = com(*E); clock_pulse(2); break;
			case 0x4A:	/* DECE */			*E = dec(*E); clock_pulse(2); break;
			case 0x5A:	/* DECF */			*F = dec(*F); clock_pulse(2); break;
			case 0x4C:	/* INCE */			*E = inc(*E); clock_pulse(2); break;
			case 0x5C:	/* INCF */			*F = inc(*F); clock_pulse(2); break;
			case 0x4D:	/* TSTE */			NZV0(*E); clock_pulse(2); break;
			case 0x5D:	/* TSTF */			NZV0(*F); clock_pulse(2); break;
			case 0x4F:	/* CLRE */			*E = clr(); clock_pulse(2); break;
			case 0x5F:	/* CLRF */			*F = clr(); clock_pulse(2); break;
			case 0x80:	/* SUBE immed */	IMM(t); *E = sub(*E, t, 0); clock_pulse(3); break;
			case 0x90:	/* SUBE direct */	DIRV(t); *E = sub(*E, t, 0); clock_pulse(4); break;
			case 0xA0:	/* SUBE indexed */	INDV(t); *E = sub(*E, t, 0); clock_pulse(5); break;
			case 0xB0:	/* SUBE extend */	EXTV(t); *E = sub(*E, t, 0); clock_pulse(5); break;
			case 0xC0:	/* SUBF immed */	IMM(t); *F = sub(*F, t, 0); clock_pulse(3); break;
			case 0xD0:	/* SUBF direct */	DIRV(t); *F = sub(*F, t, 0); clock_pulse(4); break;
			case 0xE0:	/* SUBF indexed */	INDV(t); *F = sub(*F, t, 0); clock_pulse(5); break;
			case 0xF0:	/* SUBF extend */	EXTV(t); *F = sub(*F, t, 0); clock_pulse(5); break;
			case 0x81:	/* CMPE immed */	IMM(t); sub(*E, t, 0); clock_pulse(3); break;
			case 0x91:	/* CMPE direct */	DIRV(t); sub(*E, t, 0); clock_pulse(4); break;
			case 0xA1:	/* CMPE indexed */	INDV(t); sub(*E, t, 0); clock_pulse(5); break;
			case 0xB1:	/* CMPE extend */	EXTV(t); sub(*E, t, 0); clock_pulse(5); break;
			case 0xC1:	/* CMPF immed */	IMM(t); sub(*F, t, 0); clock_pulse(3); break;
			case 0xD1:	/* CMPF direct */	DIRV(t); sub(*E, t, 0); clock_pulse(4); break;
			case 0xE1:	/* CMPF indexed */	INDV(t); sub(*E, t, 0); clock_pulse(5); break;
			case 0xF1:	/* CMPF extend */	EXTV(t); sub(*E, t, 0); clock_pulse(5); break;
			case 0x83:	/* CMPU immed */	IMM16(ta); sub16(*U, ta, 0); clock_pulse(4); break;
			case 0x93:	/* CMPU direct */	DIRV16(ta); sub16(*U, ta, 0); clock_pulse(5); break;
			case 0xA3:	/* CMPU indexed */	INDV16(ta); sub16(*U, ta, 0); clock_pulse(6); break;
			case 0xB3:	/* CMPU extend */	EXTV16(ta); sub16(*U, ta, 0); clock_pulse(6); break;
			case 0x86:	/* LDE immed */		IMM(*E); NZV0(*E); clock_pulse(3); break;
			case 0x96:	/* LDE direct */	DIRV(*E); NZV0(*E); clock_pulse(4); break;
			case 0xA6: 	/* LDE indexed */	INDV(*E); NZV0(*E); clock_pulse(5); break;
			case 0xB6:	/* LDE extend */	EXTV(*E); NZV0(*E); clock_pulse(5); break;
			case 0xC6:	/* LDF immed */		IMM(*F); NZV0(*F); clock_pulse(3); break;
			case 0xD6:	/* LDF direct */	DIRV(*F); NZV0(*F); clock_pulse(4); break;
			case 0xE6:	/* LDF indexed */	INDV(*F); NZV0(*F); clock_pulse(5); break;
			case 0xF6:	/* LDF extend */	EXTV(*F); NZV0(*F); clock_pulse(5); break;
			case 0x97:	/* STE direct */	DIR(ta); w(ta, *E); NZV0(*E); clock_pulse(4); break;
			case 0xA7:	/* STE indexed */	IND(ta); w(ta, *E); NZV0(*E); clock_pulse(5); break;
			case 0xB7:	/* STE extend */	EXT(ta); w(ta, *E); NZV0(*E); clock_pulse(5); break;
			case 0xD7:	/* STF direct */	DIR(ta); w(ta, *F); NZV0(*F); clock_pulse(4); break;
			case 0xE7:	/* STF indexed */	IND(ta); w(ta, *F); NZV0(*F); clock_pulse(5); break;
			case 0xF7:	/* STF extend */	EXT(ta); w(ta, *F); NZV0(*F); clock_pulse(5); break;
			case 0x8B:	/* ADDE immed */	IMM(t); *E = add(*E, t, 0); clock_pulse(3); break;
			case 0x9B:	/* ADDE direct */	DIRV(t); *E = add(*E, t, 0); clock_pulse(4); break;
			case 0xAB:	/* ADDE indexed */	INDV(t); *E = add(*E, t, 0); clock_pulse(5); break;
			case 0xBB:	/* ADDE extend */	EXTV(t); *E = add(*E, t, 0); clock_pulse(5); break;
			case 0xCB:	/* ADDF immed */	IMM(t); *F = add(*F, t, 0); clock_pulse(3); break;
			case 0xDB:	/* ADDF direct */	DIRV(t); *F = add(*F, t, 0); clock_pulse(4); break;
			case 0xEB:	/* ADDF indexed */	INDV(t); *F = add(*F, t, 0); clock_pulse(5); break;
			case 0xFB:	/* ADDF extend */	EXTV(t); *F = add(*F, t, 0); clock_pulse(5); break;
			case 0x8C:	/* CMPS immed */	IMM16(ta); sub16(*S, ta, 0); clock_pulse(4); break;
			case 0x9C:	/* CMPS direct */	DIRV16(ta); sub16(*S, ta, 0); clock_pulse(5); break;
			case 0xAC:	/* CMPS indexed */	INDV16(ta); sub16(*S, ta, 0); clock_pulse(6); break;
			case 0xBC:	/* CMPS extend */	EXTV16(ta); sub16(*S, ta, 0); clock_pulse(6); break;
			case 0x8D:	/* DIVD immed */	IMM(t); divd(t); break;
			case 0x9D:	/* DIVD direct */	DIRV(t); divd(t); clock_pulse(1); break;
			case 0xAD:	/* DIVD indexed */	INDV(t); divd(t); clock_pulse(2); break;
			case 0xBD:	/* DIVD extend */	EXTV(t); divd(t); clock_pulse(2); break;
			case 0x8E:	/* DIVQ immed */	IMM16(ta); divq(ta); break;
			case 0x9E:	/* DIVQ direct */	DIRV16(ta); divq(ta); clock_pulse(1); break;
			case 0xAE:	/* DIVQ indexed */	INDV16(ta); divq(ta); clock_pulse(2); break;
			case 0xBE:	/* DIVQ extend */	EXTV16(ta); divq(ta); clock_pulse(2); break;
			case 0x8F:	/* MULD immed */	IMM16(ta); muld(ta); clock_pulse(28); break;
			case 0x9F:	/* MULD direct */	DIRV16(ta); muld(ta); clock_pulse(29); break;
			case 0xAF:	/* MULD indexed */	INDV16(ta); muld(ta); clock_pulse(30); break;
			case 0xBF:	/* MULD extend */	EXTV16(ta); muld(ta); clock_pulse(30); break;
			default:	break;
		}
	} else {
		switch (instr) {
			case 0x00:	/* NEG direct */	DIR(ta); w(ta, neg(r(ta))); clock_pulse(5); break;
			case 0x01:	/* OIM direct */	IMM(t); DIR(ta); oim(ta, t); clock_pulse(6); break;
			case 0x02:	/* AIM direct */	IMM(t); DIR(ta); aim(ta, t); clock_pulse(6); break;
			case 0x03:	/* COM direct */	DIR(ta); w(ta, com(r(ta))); clock_pulse(5); break;
			case 0x04:	/* LSR direct */	DIR(ta); w(ta, lsr(r(ta))); clock_pulse(5); break;
			case 0x05:	/* EIM direct */	IMM(t); DIR(ta); eim(ta, t); clock_pulse(6); break;
			case 0x06:	/* ROR direct */	DIR(ta); w(ta, ror(r(ta))); clock_pulse(5); break;
			case 0x07:	/* ASR direct */	DIR(ta); w(ta, asr(r(ta))); clock_pulse(5); break;
			case 0x08:	/* LSL direct */	DIR(ta); w(ta, lsl(r(ta))); clock_pulse(5); break;
			case 0x09:	/* ROL direct */	DIR(ta); w(ta, rol(r(ta))); clock_pulse(5); break;
			case 0x0A:	/* DEC direct */	DIR(ta); w(ta, dec(r(ta))); clock_pulse(5); break;
			case 0x0B:	/* TIM direct */	IMM(t); DIR(ta); tim(ta, t); clock_pulse(6); break;
			case 0x0C:	/* INC direct */	DIR(ta); w(ta, inc(r(ta))); clock_pulse(5); break;
			case 0x0D:	/* TST direct */	DIR(ta); NZV0(r(ta)); clock_pulse(4); break;
			case 0x0E:	/* JMP direct */	last_branch = *PC-1; DIR(*PC); --*PC; clock_pulse(2); break;
			case 0x0F:	/* CLR direct */	DIR(ta); w(ta, clr()); clock_pulse(5); break;
			case 0x12:	/* NOP */			clock_pulse(1); break;
//			case 0x13:	/* SYNC */			break;
			case 0x14:	/* SEXW */			t = *W >> 15; *D = t ? 0xFFFF : 0x0000; *CC &= ~NZ; *CC |= (t << CCN) | Z(*Q); clock_pulse(4); break;
			case 0x16:	/* LBRA */			*PC = (int16_t)*PC + (int16_t)r16(*PC) + 2; clock_pulse(4); break;
			case 0x17:	/* LBSR */			last_branch = *PC-2; push16(S, *PC); *PC = (int16_t)*PC + (int16_t)r16(*PC) + 2; clock_pulse(7); break;
			case 0x19:	/* DAA */			*A = daa(*A); clock_pulse(1); break;
			case 0x1A:	/* ORCC */			IMM(t); *CC |= t; clock_pulse(3); break;
			case 0x1C:	/* ANDCC */			IMM(t); *CC &= t; clock_pulse(3); break;
			case 0x1D:	/* SEX */			t = *A >> 7; *A = t ? 0xFF : 0x00; *CC &= ~NZ; *CC |= (t << CCN) | Z(*D); clock_pulse(1); break;
			case 0x1E:	/* EXG */			IMM(t); ir = tfrexg(t); if (ir.src_width) *(uint16_t *)ir.src_reg = ir.dst; else *(uint8_t *)ir.src_reg = ir.dst;
																	if (ir.dst_width) *(uint16_t *)ir.dst_reg = ir.src; else *(uint8_t *)ir.dst_reg = ir.src; clock_pulse(5); break;
			case 0x1F:	/* TFR */			IMM(t); ir = tfrexg(t); if (ir.dst_width) *(uint16_t *)ir.dst_reg = ir.src; else *(uint8_t *)ir.dst_reg = ir.src; clock_pulse(4); break;
			case 0x30:	/* LEAX */			IND(*X); *CC &= ~0x04; *CC |= Z(*X); clock_pulse(4); break;
			case 0x31:	/* LEAY */			IND(*Y); *CC &= ~0x04; *CC |= Z(*Y); clock_pulse(4); break;
			case 0x32:	/* LEAS */			IND(*S); clock_pulse(4); break;
			case 0x33:	/* LEAU */			IND(*U); clock_pulse(4); break;
			case 0x34:	/* PSHS */			IMM(t); psh(S, t); clock_pulse(4); break;
			case 0x35:	/* PULS */			IMM(t); pul(S, t); clock_pulse(4); break;
			case 0x36:	/* PSHU */			IMM(t); psh(U, t); clock_pulse(4); break;
			case 0x37:	/* PULU */			IMM(t); pul(U, t); clock_pulse(4); break;
			case 0x39:	/* RTS */			last_branch = *PC-1; pull16(S, *PC); clock_pulse(4); break;
			case 0x3A:	/* ABX */			*X += *B; clock_pulse(1); break;
			case 0x3B:	/* RTI */			clock_pulse(1); break;
//			case 0x3C:	/* CWAI */			break;
			case 0x3D:	/* MUL */			*D = (uint16_t)*A * (uint16_t)*B; *CC &= ~5; *CC |= Z(*D) | (*B >> 7); clock_pulse(10); break;
//			case 0x3F:	/* SWI */			break;
			case 0x40:	/* NEGA */			*A = neg(*A); clock_pulse(1); break;
			case 0x43:	/* COMA */			*A = com(*A); clock_pulse(1); break;
			case 0x44:	/* LSRA */			*A = lsr(*A); clock_pulse(1); break;
			case 0x46:	/* RORA */			*A = ror(*A); clock_pulse(1); break;
			case 0x47:	/* ASRA */			*A = asr(*A); clock_pulse(1); break;
			case 0x48:	/* LSLA */			*A = lsl(*A); clock_pulse(1); break;
			case 0x49:	/* ROLA */			*A = rol(*A); clock_pulse(1); break;
			case 0x4A:	/* DECA */			*A = dec(*A); clock_pulse(1); break;
			case 0x4C:	/* INCA */			*A = inc(*A); clock_pulse(1); break;
			case 0x4D:	/* TSTA */			NZV0(*A); clock_pulse(1); break;
			case 0x4F:	/* CLRA */			*A = clr(); clock_pulse(1); break;
			case 0x50:	/* NEGB */			*B = neg(*B); clock_pulse(1); break;
			case 0x53:	/* COMB */			*B = com(*B); clock_pulse(1); break;
			case 0x54:	/* LSRB */			*B = lsr(*B); clock_pulse(1); break;
			case 0x56:	/* RORB */			*B = ror(*B); clock_pulse(1); break;
			case 0x57:	/* ASRB */			*B = asr(*B); clock_pulse(1); break;
			case 0x58:	/* LSLB */			*B = lsl(*B); clock_pulse(1); break;
			case 0x59:	/* ROLB */			*B = rol(*B); clock_pulse(1); break;
			case 0x5A:	/* DECB */			*B = dec(*B); clock_pulse(1); break;
			case 0x5C:	/* INCB */			*B = inc(*B); clock_pulse(1); break;
			case 0x5D:	/* TSTB */			NZV0(*B); clock_pulse(1); break;
			case 0x5F:	/* CLRB */			*B = clr(); clock_pulse(1); break;
			case 0x60:	/* NEG indexed */	IND(ta); w(ta, neg(r(ta))); clock_pulse(6); break;
			case 0x61:	/* OIM indexed */	IMM(t); IND(ta); oim(ta, t); clock_pulse(7); break;
			case 0x62:	/* AIM indexed */	IMM(t); IND(ta); aim(ta, t); clock_pulse(7); break;
			case 0x63:	/* COM indexed */	IND(ta); w(ta, com(r(ta))); clock_pulse(6); break;
			case 0x64:	/* LSR indexed */	IND(ta); w(ta, lsr(r(ta))); clock_pulse(6); break;
			case 0x65:	/* EIM indexed */	IMM(t); IND(ta); eim(ta, t); clock_pulse(7); break;
			case 0x66:	/* ROR indexed */	IND(ta); w(ta, ror(r(ta))); clock_pulse(6); break;
			case 0x67:	/* ASR indexed */	IND(ta); w(ta, asr(r(ta))); clock_pulse(6); break;
			case 0x68:	/* LSL indexed */	IND(ta); w(ta, lsl(r(ta))); clock_pulse(6); break;
			case 0x69:	/* ROL indexed */	IND(ta); w(ta, rol(r(ta))); clock_pulse(6); break;
			case 0x6A:	/* DEC indexed */	IND(ta); w(ta, dec(r(ta))); clock_pulse(6); break;
			case 0x6B:	/* TIM indexed */	IMM(t); IND(ta); tim(ta, t); clock_pulse(7); break;
			case 0x6C:	/* INC indexed */	IND(ta); w(ta, inc(r(ta))); clock_pulse(6); break;
			case 0x6D:	/* TST indexed */	IND(ta); NZV0(r(ta)); clock_pulse(5); break;
			case 0x6E:	/* JMP indexed */	last_branch = *PC-1; IND(*PC); clock_pulse(3); break;
			case 0x6F:	/* CLR indexed */	IND(ta); w(ta, clr()); clock_pulse(6); break;
			case 0x70:	/* NEG extend */	EXT(ta); w(ta, neg(r(ta))); clock_pulse(6); break;
			case 0x71:	/* OIM extend */	IMM(t); EXT(ta); oim(ta, t); clock_pulse(7); break;
			case 0x72:	/* AIM extend */	IMM(t); EXT(ta); aim(ta, t); clock_pulse(7); break;
			case 0x73:	/* COM extend */	EXT(ta); w(ta, com(r(ta))); clock_pulse(6); break;
			case 0x74:	/* LSR extend */	EXT(ta); w(ta, lsr(r(ta))); clock_pulse(6); break;
			case 0x75:	/* EIM extend */	IMM(t); EXT(ta); eim(ta, t); clock_pulse(7); break;
			case 0x76:	/* ROR extend */	EXT(ta); w(ta, ror(r(ta))); clock_pulse(6); break;
			case 0x77:	/* ASR extend */	EXT(ta); w(ta, asr(r(ta))); clock_pulse(6); break;
			case 0x78:	/* LSL extend */	EXT(ta); w(ta, lsl(r(ta))); clock_pulse(6); break;
			case 0x79:	/* ROL extend */	EXT(ta); w(ta, rol(r(ta))); clock_pulse(6); break;
			case 0x7A:	/* DEC extend */	EXT(ta); w(ta, dec(r(ta))); clock_pulse(6); break;
			case 0x7B:	/* TIM extend */	IMM(t); EXT(ta); tim(ta, t); clock_pulse(7); break;
			case 0x7C:	/* INC extend */	EXT(ta); w(ta, inc(r(ta))); clock_pulse(6); break;
			case 0x7D:	/* TST extend */	EXT(ta); NZV0(r(ta)); clock_pulse(5); break;
			case 0x7E:	/* JMP extend */	last_branch = *PC-1; EXT(*PC); *PC -= 2; clock_pulse(3); break;
			case 0x7F:	/* CLR extend */	EXT(ta); w(ta, clr()); clock_pulse(6); break;
			case 0x80:	/* SUBA immed */	IMM(t); *A = sub(*A, t, 0); clock_pulse(2); break;
			case 0x81:	/* CMPA immed */	IMM(t); sub(*A, t, 0); clock_pulse(2); break;
			case 0x82:	/* SBCA immed */	IMM(t); *A = sub(*A, t, CCBIT(CCC)); clock_pulse(2); break;
			case 0x83:	/* SUBD immed */	IMM16(ta); *D = sub16(*D, ta, 0); clock_pulse(3); break;
			case 0x84:	/* ANDA immed */	IMM(t); *A &= t; NZV0(*A); clock_pulse(2); break;
			case 0x85:	/* BITA immed */	IMM(t); t &= *A; NZV0(t); clock_pulse(2); break;
			case 0x86:	/* LDA immed */		IMM(*A); NZV0(*A); clock_pulse(2); break;
			case 0x88:	/* EORA immed */	IMM(t); *A ^= t; NZV0(*A); clock_pulse(2); break;
			case 0x89:	/* ADCA immed */	IMM(t); *A = add(*A, t, CCBIT(CCC)); clock_pulse(2); break;
			case 0x8A:	/* ORA immed */		IMM(t); *A |= t; NZV0(*A); clock_pulse(2); break;
			case 0x8B:	/* ADDA immed */	IMM(t); *A = add(*A, t, 0); clock_pulse(2); break;
			case 0x8C:	/* CMPX immed */	IMM16(ta); sub16(*X, ta, 0); clock_pulse(3); break;
			case 0x8D:	/* BSR */			last_branch = *PC-1; IMM(t); push16(S, *PC); *PC += (int16_t)(int8_t)t; clock_pulse(6); break;
			case 0x8E:	/* LDX immed */		IMM16(*X); NZV016(*X); clock_pulse(3); break;
			case 0x90:	/* SUBA direct */	DIRV(t); *A = sub(*A, t, 0); clock_pulse(3); break;
			case 0x91:	/* CMPA direct */	DIRV(t); sub(*A, t, 0); clock_pulse(3); break;
			case 0x92:	/* SBCA direct */	DIRV(t); *A = sub(*A, t, CCBIT(CCC)); clock_pulse(3); break;
			case 0x93:	/* SUBD direct */	DIRV16(t); *D = sub16(*D, t, 0); clock_pulse(4); break;
			case 0x94:	/* ANDA direct */	DIRV(t); *A &= t; NZV0(*A); clock_pulse(3); break;
			case 0x95:	/* BITA direct */	DIRV(t); t &= *A; NZV0(t); clock_pulse(3); break;
			case 0x96:	/* LDA direct */	DIRV(*A); NZV0(*A); clock_pulse(3); break;
			case 0x97:	/* STA direct */	DIR(ta); w(ta, *A); NZV0(*A); clock_pulse(3);	break;
			case 0x98:	/* EORA direct */	DIRV(t); *A ^= t; NZV0(*A); clock_pulse(3); break;
			case 0x99:	/* ADCA direct */	DIRV(t); *A = add(*A, t, CCBIT(CCC)); clock_pulse(3); break;
			case 0x9A:	/* ORA direct */	DIRV(t); *A |= t; NZV0(*A); clock_pulse(3); break;
			case 0x9B:	/* ADDA direct */	DIRV(t); *A = add(*A, t, 0); clock_pulse(3); break;
			case 0x9C:	/* CMPX direct */	DIRV16(ta); sub16(*X, ta, 0); clock_pulse(4); break;
			case 0x9D:	/* JSR direct */	last_branch = *PC-1; DIR(ta); push16(S, *PC); *PC = ta; clock_pulse(6); break;
			case 0x9E:	/* LDX direct */	DIRV16(*X); NZV016(*X); clock_pulse(4); break;
			case 0x9F:	/* STX direct */	DIR(ta); NZV016(*X); w16(ta, *X); clock_pulse(4); break;
			case 0xA0:	/* SUBA indexed */	INDV(t); *A = sub(*A, t, 0); clock_pulse(4); break;
			case 0xA1:	/* CMPA indexed */	INDV(t); sub(*A, t, 0); clock_pulse(4); break;
			case 0xA2:	/* SBCA indexed */	INDV(t); *A = sub(*A, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xA3:	/* SUBD indexed */	INDV16(t); *D = sub16(*D, t, 0); clock_pulse(5); break;
			case 0xA4:	/* ANDA indexed */	INDV(t); *A &= t; NZV0(*A); clock_pulse(4); break;
			case 0xA5:	/* BITA indexed */	INDV(t); t &= *A; NZV0(t); clock_pulse(4); break;
			case 0xA6:	/* LDA indexed */	INDV(*A); NZV0(*A); clock_pulse(4); break;
			case 0xA7:	/* STA indexed */	IND(ta); w(ta, *A); NZV0(*A); clock_pulse(4);	break;
			case 0xA8:	/* EORA indexed */	INDV(t); *A ^= t; NZV0(*A); clock_pulse(4); break;
			case 0xA9:	/* ADCA indexed */	INDV(t); *A = add(*A, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xAA:	/* ORA indexed */	INDV(t); *A |= t; NZV0(*A); clock_pulse(4); break;
			case 0xAB:	/* ADDA indexed */	INDV(t); *A = add(*A, t, 0); clock_pulse(4); break;
			case 0xAC:	/* CMPX indexed */	INDV16(ta); sub16(*X, ta, 0); clock_pulse(5); break;
			case 0xAD:	/* JSR indexed */	last_branch = *PC-1; IND(ta); push16(S, *PC); *PC = ta; clock_pulse(6); break;
			case 0xAE:	/* LDX indexed */	INDV16(*X); NZV016(*X); clock_pulse(5); break;
			case 0xAF:	/* STX indexed */	IND(ta); NZV016(*X); w16(ta, *X); clock_pulse(5); break;
			case 0xB0:	/* SUBA extend */	EXTV(t); *A = sub(*A, t, 0); clock_pulse(4); break;
			case 0xB1:	/* CMPA extend */	EXTV(t); sub(*A, t, 0); clock_pulse(4); break;
			case 0xB2:	/* SBCA extend */	EXTV(t); *A = sub(*A, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xB3:	/* SUBD extend */	EXTV16(t); *D = sub16(*D, t, 0); clock_pulse(5); break;
			case 0xB4:	/* ANDA extend */	EXTV(t); *A &= t; NZV0(*A); clock_pulse(4); break;
			case 0xB5:	/* BITA extend */	EXTV(t); t &= *A; NZV0(t); clock_pulse(4); break;
			case 0xB6:	/* LDA extend */	EXTV(*A); NZV0(*A); clock_pulse(4); break;
			case 0xB7:	/* STA direct */	EXT(ta); w(ta, *A); NZV0(*A); clock_pulse(4);	break;
			case 0xB8:	/* EORA extend */	EXTV(t); *A ^= t; NZV0(*A); clock_pulse(4); break;
			case 0xB9:	/* ADCA extend */	EXTV(t); *A = add(*A, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xBA:	/* ORA extend */	EXTV(t); *A |= t; NZV0(*A); clock_pulse(4); break;
			case 0xBB:	/* ADDA extend */	EXTV(t); *A = add(*A, t, 0); clock_pulse(4); break;
			case 0xBC:	/* CMPX extend */	EXTV16(ta); sub16(*X, ta, 0); clock_pulse(5); break;
			case 0xBD:	/* JSR extend */	last_branch = *PC-1; EXT(ta); push16(S, *PC); *PC = ta; clock_pulse(7); break;
			case 0xBE:	/* LDX extend */	EXTV16(*X); NZV016(*X); clock_pulse(5); break;
			case 0xBF:	/* STX extend */	EXT(ta); NZV016(*X); w16(ta, *X); clock_pulse(5); break;
			case 0xC0:	/* SUBB immed */	IMM(t); *B = sub(*B, t, 0); clock_pulse(2); break;
			case 0xC1:	/* CMPB immed */	IMM(t); sub(*B, t, 0); clock_pulse(2); break;
			case 0xC2:	/* SBCB immed */	IMM(t); *B = sub(*B, t, CCBIT(CCC)); clock_pulse(2); break;
			case 0xC3:	/* ADDD immed */	IMM16(ta); *D = add16(*D, ta, 0); clock_pulse(3); break;
			case 0xC4:	/* ANDB immed */	IMM(t); *B &= t; NZV0(*B); clock_pulse(2); break;
			case 0xC5:	/* BITB immed */	IMM(t); t &= *B; NZV0(t); clock_pulse(2); break;
			case 0xC6:	/* LDB immed */		IMM(*B); NZV0(*B); clock_pulse(2); break;
			case 0xC8:	/* EORB immed */	IMM(t); *B ^= t; NZV0(*B); clock_pulse(2); break;
			case 0xC9:	/* ADCB immed */	IMM(t); *B = add(*B, t, CCBIT(CCC)); clock_pulse(2); break;
			case 0xCA:	/* ORB immed */		IMM(t); *B |= t; NZV0(*B); clock_pulse(2); break;
			case 0xCB:	/* ADDB immed */	IMM(t); *B = add(*B, t, 0); clock_pulse(2); break;
			case 0xCC:	/* LDD immed */		IMM16(*D); NZV016(*D); clock_pulse(3); break;
			case 0xCD:	/* LDQ immed */		IMM32(*Q); NZV032(*Q); clock_pulse(5); break;
			case 0xCE:	/* LDU immed */		IMM16(*U); NZV016(*U); clock_pulse(3); break;
			case 0xD0:	/* SUBB direct */	DIRV(t); *B = sub(*B, t, 0); clock_pulse(3); break;
			case 0xD1:	/* CMPB direct */	DIRV(t); sub(*B, t, 0); clock_pulse(3); break;
			case 0xD2:	/* SBCB direct */	DIRV(t); *B = sub(*B, t, CCBIT(CCC)); clock_pulse(3); break;
			case 0xD3:	/* ADDD direct */	DIRV16(ta); *D = add16(*D, ta, 0); clock_pulse(4); break;
			case 0xD4:	/* ANDB direct */	DIRV(t); *B &= t; NZV0(*B); clock_pulse(3); break;
			case 0xD5:	/* BITB direct */	DIRV(t); t &= *B; NZV0(t); clock_pulse(3); break;
			case 0xD6:	/* LDB direct */	DIRV(*B); NZV0(*B); clock_pulse(3); break;
			case 0xD7:	/* STB direct */	DIR(ta); w(ta, *B); NZV0(*B); clock_pulse(3);	break;
			case 0xD8:	/* EORB direct */	DIRV(t); *B ^= t; NZV0(*B); clock_pulse(3); break;
			case 0xD9:	/* ADCB direct */	DIRV(t); *B = add(*B, t, CCBIT(CCC)); clock_pulse(3); break;
			case 0xDA:	/* ORB direct */	DIRV(t); *B |= t; NZV0(*B); clock_pulse(3); break;
			case 0xDB:	/* ADDB direct */	DIRV(t); *B = add(*B, t, 0); clock_pulse(3); break;
			case 0xDC:	/* LDD direct */	DIRV16(*D); NZV016(*D); clock_pulse(4); break;
			case 0xDD:	/* STD direct */	DIR(ta); NZV016(*D); w16(ta, *D); clock_pulse(4); break;
			case 0xDE:	/* LDU direct */	DIRV16(*U); NZV016(*U); clock_pulse(4); break;
			case 0xDF:	/* STU direct */	DIR(ta); NZV016(*U); w16(ta, *U); clock_pulse(4); break;
			case 0xE0:	/* SUBB indexed */	INDV(t); *B = sub(*B, t, 0); clock_pulse(4); break;
			case 0xE1:	/* CMPB indexed */	INDV(t); sub(*B, t, 0); clock_pulse(4); break;
			case 0xE2:	/* SBCB indexed */	INDV(t); *B = sub(*B, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xE3:	/* ADDD indexed */	INDV16(ta); *D = add16(*D, ta, 0); clock_pulse(5); break;
			case 0xE4:	/* ANDB indexed */	INDV(t); *B &= t; NZV0(*B); clock_pulse(4); break;
			case 0xE5:	/* BITB indexed */	INDV(t); t &= *B; NZV0(t); clock_pulse(4); break;
			case 0xE6:	/* LDB indexed */	INDV(*B); NZV0(*B); clock_pulse(3); break;
			case 0xE7:	/* STB indexed */	IND(ta); w(ta, *B); NZV0(*B); clock_pulse(4);	break;
			case 0xE8:	/* EORB indexed */	INDV(t); *B ^= t; NZV0(*B); clock_pulse(4); break;
			case 0xE9:	/* ADCB indexed */	INDV(t); *B = add(*B, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xEA:	/* ORB indexed */	INDV(t); *B |= t; NZV0(*B); clock_pulse(4); break;
			case 0xEB:	/* ADDB indexed */	INDV(t); *B = add(*B, t, 0); clock_pulse(4); break;
			case 0xEC:	/* LDD indexed */	INDV16(*D); NZV016(*D); clock_pulse(5); break;
			case 0xED:	/* STD indexed */	IND(ta); NZV016(*D); w16(ta, *D); clock_pulse(5); break;
			case 0xEE:	/* LDU indexed */	INDV16(*U); NZV016(*U); clock_pulse(5); break;
			case 0xEF:	/* STU indexed */	IND(ta); NZV016(*U); w16(ta, *U); clock_pulse(5); break;
			case 0xF0:	/* SUBB extend */	EXTV(t); *B = sub(*B, t, 0); clock_pulse(4); break;
			case 0xF1:	/* CMPB extend */	EXTV(t); sub(*B, t, 0); clock_pulse(4); break;
			case 0xF2:	/* SBCB extend */	EXTV(t); *B = sub(*B, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xF3:	/* ADDD extend */	EXTV16(ta); *D = add16(*D, ta, 0); clock_pulse(5); break;
			case 0xF4:	/* ANDB extend */	EXTV(t); *B &= t; NZV0(*B); clock_pulse(4); break;
			case 0xF5:	/* BITB extend */	EXTV(t); t &= *B; NZV0(t); clock_pulse(4); break;
			case 0xF6:	/* LDB extend */	EXTV(*B); NZV0(*B); clock_pulse(4); break;
			case 0xF7:	/* STB extend */	EXT(ta); w(ta, *B); NZV0(*B); clock_pulse(4);	break;
			case 0xF8:	/* EORB extend */	EXTV(t); *B ^= t; NZV0(*B); clock_pulse(4); break;
			case 0xF9:	/* ADCB extend */	EXTV(t); *B = add(*B, t, CCBIT(CCC)); clock_pulse(4); break;
			case 0xFA:	/* ORB extend */	EXTV(t); *B |= t; NZV0(*B); clock_pulse(4); break;
			case 0xFB:	/* ADDB extend */	EXTV(t); *B = add(*B, t, 0); clock_pulse(4); break;
			case 0xFC:	/* LDD extend */	EXTV16(*D); NZV016(*D); clock_pulse(5); break;
			case 0xFD:	/* STD extend */	EXT(ta); NZV016(*D); w16(ta, *D); clock_pulse(5); break;
			case 0xFE:	/* LDU extend */	EXTV16(*U); NZV016(*U); clock_pulse(5); break;
			case 0xFF:	/* STU extend */	EXT(ta); NZV016(*U); w16(ta, *U); clock_pulse(5); break;
			default:			printf("Warning: unimplement instruction (%02X%02X) PC=0x%04X\n", page, instr, *PC); break;
		}
	}
}
