/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 * 
 * reg.h
 * 
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
 
#ifndef _REG_H
#define _REG_H

#include <stdint.h>

extern uint8_t reg[20];

extern uint8_t	*A;
extern uint8_t	*B;
extern uint16_t	*D;
extern uint8_t	*E;
extern uint8_t	*F;
extern uint16_t	*W;
extern uint32_t	*Q;
extern uint16_t	*X;
extern uint16_t	*Y;
extern uint16_t	*U;
extern uint16_t	*S;
extern uint16_t	*V;
extern uint16_t	*PC;
extern uint8_t	*DP;
extern uint8_t 	*CC;
extern uint8_t	*ZERO;


#endif
