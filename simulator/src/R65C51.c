/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * R65C51.c
 * This file emulates the ACIA and prints the serial output to stdout
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "6309.h"
#include "reg.h"

static uint8_t acia_read(uint address) {
	address &= 0b11;
	if (address == 1)	// Status
		return 0b00010000;	// TX empty, no input, no errors
	return	0xFF;
}

static void acia_write(uint address, uint data) {
	address &= 0b11;
	if (address == 0)
		printf("ACIA:\t0x%02X\t%u\t%d\t'%c'\n", data, data, (int8_t)data, (data >= ' ' && data <= '~') ? data : ' ');
}

BusDevice ACIA = {NULL, &acia_read, &acia_write};
