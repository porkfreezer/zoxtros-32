/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * ROM.c
 * This is a basic device to represent the ROM, only writable by the
 * SREC parser.
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include "6309.h"
#include "reg.h"

static uint8_t rom[16384];

static uint8_t rom_read(uint address) {
	return rom[address & 0x3FFF];
}

static void rom_write(uint address, uint data) {
	if (address < 0xC000)
		rom[address & 0x3FFF] = data;
}

BusDevice ROM = {NULL, &rom_read, &rom_write};
