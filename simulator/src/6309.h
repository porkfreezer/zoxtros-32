/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * 6309.h
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef _6309_H
#define _6309_H

#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BIT(byte, bit) (((byte) >> (bit)) & 1)

struct arguments {
	uint		scale;
	char		*srec;
	int			debug;
	uint16_t	*breakpoints;
	int			num_breakpoints;
	uint16_t	*watches;
	int			num_watches;
};

// The BusDevice struct/class defines each device connected
// to the CPU bus, such as the RAM, ROM, and VDP
typedef struct {
	void	(*init)(struct arguments *arguments);	// Optional intialization function, called specially in the main loop
	uint8_t	(*read)(uint address);					// Return the value the real device should return based on the address (mask it yourself)
	void	(*write)(uint address, uint data);		// Write data to address (mask it yourself) or do nothing
} BusDevice;

extern BusDevice *memmap[32];
extern uint16_t last_branch;

uint8_t r(uint address);
uint16_t r16(uint address);
uint32_t r32(uint address);

void w(uint address, uint8_t data);
void w16(uint address, uint16_t data);
void w32(uint address, uint32_t data);

// Stacking macros
#define push(reg, data) do{--*(reg); w(*(reg), data);}while(0)
#define push16(reg, data) do{*(reg) -= 2; w16(*(reg), data);}while(0)
#define pull(reg, var) do{var = r(*(reg)); ++*(reg);}while(0)
#define pull16(reg, var) do{var = r16(*(reg)); *(reg) += 2;}while(0)

#endif
