/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * HD6340.c
 * This file takes care of sound
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include "6309.h"
#include "reg.h"

#define FREQ 48000

static uint8_t cr1 = 0;
static uint8_t cr2 = 0;
static uint8_t cr3 = 0;
static uint16_t timers[3] = {0, 0, 0};
static uint8_t buffer = 0;

static int8_t sound[(FREQ * 0x10000UL) / 16666];
static int sound_i = 0;
static int sound_end = 0;
static SDL_AudioDeviceID device;

static void sound_start() {
	int time = 0.000001 * FREQ * timers[2]/2;
	int duration = 0.000030 * FREQ * timers[1];
	if (time == 0) {
		memset(sound, 0, duration);
	}
	else {
		int i = 0;
		while (i < duration) {
			for (int j = 0; j < time && i < duration; j++)
				sound[i++] = 64;
			for (int j = 0; j < time && i < duration; j++)
				sound[i++] = 0;
		}
	}
	sound_end = duration;
	sound_i = 0;
	w(0x0016, 1);
	SDL_PauseAudioDevice(device, 0);
}

static void ptm_copy_samples(void *data, uint8_t *stream, int len) {
	memset(stream, 0, len);

	if (sound_i == sound_end) {
		cr1 = 1;
		w(0x0016, 0);
		uint SONG = r16(0x0014);
		if (SONG) {
			timers[2] = r16(SONG);
			if (timers[2] != 0) {
				timers[1] = r16(SONG+2);
				w16(0x0014, SONG+4);
				cr1 = 0;
				sound_start();
			} else {
				w16(0x0014, 0);
			}

		}
		return;
	}


	if (len + sound_i > sound_end - 1)
		len = sound_end - sound_i;

	memcpy(stream, &sound[sound_i], len);
	sound_i += len;
}

static void ptm_init(struct arguments *arguments) {
	SDL_AudioSpec spec = { .freq = FREQ, .format = AUDIO_S8, .channels = 1, .samples = 512, .callback = ptm_copy_samples, .userdata = NULL };
	device = SDL_OpenAudioDevice(NULL, 0, &spec, NULL, 0);
}

static uint8_t ptm_read(uint address) {
	return 0;
}

static void ptm_write(uint address, uint data) {
	address &= 0b111;
	if (address > 1) {
		if (address & 1)
			timers[(address >> 1) - 1] = (buffer << 8) | data;
		else
			buffer = data;
	}
	else if (address == 1) {
		cr2 = data;
	}
	else {
		if (cr2 & 1) {
			cr1 = data;
			if (!(cr1 & 1))
				sound_start();
		}
		else {
			cr3 = data;
		}
	}
}

BusDevice PTM = {&ptm_init, &ptm_read, &ptm_write};
