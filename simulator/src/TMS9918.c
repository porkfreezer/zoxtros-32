/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * TMS9918.c
 * This emualates the VDP with SDL (currently only Text and Graphics I modes)
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "6309.h"
#include "reg.h"
#include <unistd.h>

enum R0 { EXVDP, M3 };
enum R1 { MAG, SIZE, M2=3, M1, IE, BLANK, VRAMSIZE };

static uint8_t vram[16384];
static uint8_t vreg[8];
static uint8_t status = 0;

static uint8_t tmp;
static uint16_t tmp_a;
static char tmpc = 0;
static char dirty = 1;

static SDL_Renderer *renderer;
static int scale;

static const uint8_t colors[][4] = {
	{0x00,0x00,0x00,0x00}, // 0 Transparent
	{0x00,0x00,0x00,0xFF}, // 1 Black
	{0x0A,0xAD,0x1E,0xFF}, // 2 Medium Green
	{0x34,0xC8,0x4C,0xFF}, // 3 Light Green
	{0x2D,0x2B,0xE3,0xFF}, // 4 Dark Blue
	{0x51,0x4B,0xFB,0xFF}, // 5 Light Blue
	{0xBD,0x29,0x25,0xFF}, // 6 Dark Red
	{0x1E,0xE2,0xEF,0xFF}, // 7 Cyan
	{0xFB,0x2C,0x2B,0xFF}, // 8 Medium Red
	{0xFF,0x5F,0x4C,0xFF}, // 9 Light Red
	{0xBD,0xA2,0x2B,0xFF}, // A Dark Yellow
	{0xD7,0xB4,0x54,0xFF}, // B Light Yellow
	{0x0A,0x8C,0x18,0xFF}, // C Dark Green
	{0xAF,0x32,0x9A,0xFF}, // D Magenta
	{0xB2,0xB2,0xB2,0xFF}, // E Gray
	{0xFF,0xFF,0xFF,0xFF}, // F White
};

#define setcolor(c) SDL_SetRenderDrawColor(renderer, colors[c][0], colors[c][1], colors[c][2], colors[c][3]);


static uint8_t vdp_read(uint address) {
	address &= 1;
	if (address) {
		dirty = 1;
		return status;
	}
	else {
		tmpc = 0;
		return vram[tmp_a++];
	}
}

static void vdp_write(uint address, uint data) {
	address &= 1;
	if (address) {
		switch (tmpc) {
			case 0:
				tmp = data;
				++tmpc;
				break;
			case 1:
				if (data & 0x80) {
					vreg[data & 7] = tmp;
					tmpc = 0;
					dirty = 1;
				}
				else {
					tmp_a =  ((data & 0x3F) << 8) | tmp;
					++tmpc;
				}
				break;
		}
	}
	else {
		vram[tmp_a] = data;
		//printf("\tVRAM: 0x%04X = 0x%02X  (PC = 0x%04X)\n", tmp_a, data, *PC);
		tmpc = 0;
		++tmp_a;
		dirty = 1;
	}
}

static void vdp_draw_pixel(int x, int y, int color) {
	if (x >= 0 && x <= 255 && y >= 0 && y <= 191) {
		SDL_Rect pixel = {(x + 16) * scale, (y + 16) * scale, scale, scale};
		setcolor(color);
		SDL_RenderFillRect(renderer, &pixel);
	}
}

SDL_Thread *render_thread;

static int vdp_draw(void *p) {
	while (1) {
		if (dirty) {
			dirty = 0;
			setcolor(vreg[7] & 0xF);
			SDL_RenderClear(renderer);

			if (BIT(vreg[1], BLANK)) {
				char mode = BIT(vreg[0], M3) | (BIT(vreg[1], M2) << 1) | (BIT(vreg[1], M1) << 2);
				uint16_t name_a = vreg[2] * 0x400;
				uint16_t color_a = vreg[3] * 0x40;
				uint16_t pattern_a = vreg[4] * 0x800;
				uint16_t sprite_attr_a = vreg[5] * 0x80;
				uint16_t sprite_pat_a = vreg[6] * 0x800;

				if (mode == 0b000) {		// Graphics I
					for (uint16_t chr = 0; chr < 24*32; chr++) {
						uint8_t name = vram[name_a + chr];
						uint8_t *pattern = &vram[pattern_a + (name * 8)];
						uint8_t color = vram[color_a + (name >> 3)] >> 4;
						uint8_t bcolor = vram[color_a + (name >> 3)] & 0xF;
						uint8_t row = chr / 32;
						for (uint8_t y = 0; y < 8; y++) {
							for (uint8_t x = 0; x < 8; x++) {
								vdp_draw_pixel((chr - (row * 32)) * 8 + x, (row * 8) + y, ((pattern[y] >> (7-x)) & 1) ? color : bcolor);
							}
						}
					}
				} else if (mode == 0b001) {	// Graphics II

				} else if (mode == 0b010) {	// Multicolor

				} else if (mode == 0b100) {	// Text
					uint8_t color = vreg[7] >> 4;
					for (uint16_t chr = 0; chr < 24*40; chr++) {
						uint8_t name = vram[name_a + chr];
						uint8_t *pattern = &vram[pattern_a + (name * 8)];
						uint8_t row = chr / 40;
						for (uint8_t y = 0; y < 8; y++) {
							for (uint8_t x = 0; x < 6; x++) {
								if ((pattern[y] >> (7-x)) & 1)
									vdp_draw_pixel((chr - (row * 40)) * 6 + x, (row * 8) + y, color);
							}
						}
					}

				}

				if (mode != 0b100) {	// Sprites (not rendered in text mode)
					int end = 0x80-4;
					for (int i = 0; i < 0x80; i += 4) {
						if (vram[sprite_attr_a + i] == 0xD0) {
							end = i;
							break;
						}
					}
					for (int spr = end; spr >= 0; spr -= 4) {
						uint8_t sy_raw = vram[sprite_attr_a + spr];
						uint8_t sx_raw = vram[sprite_attr_a + spr+1];
						uint8_t name = vram[sprite_attr_a + spr+2];
						uint8_t ec = vram[sprite_attr_a + spr+3] >> 7;
						uint8_t color = vram[sprite_attr_a + spr+3] & 0xF;
						uint8_t pat_byte = sprite_pat_a + (name*8);
						int sy;
						if (sy_raw >= 192)
							sy = (int8_t)(sy_raw + 1);
						else
							sy = sy_raw + 1;
						int sx = sx_raw;
						if (ec)
							sx -= 32;
						for (uint8_t y = 0; y < 8; y++) {
							for (uint8_t x = 0; x < 8; x++) {
								if ((vram[pat_byte + y] >> (7-x)) & 1)
									vdp_draw_pixel(sx + x, sy + y, color);
								if ((vram[pat_byte + 8 + y] >> (7-x)) & 1)
									vdp_draw_pixel(sx + x, sy + 8 + y, color);
								if ((vram[pat_byte + 16 + y] >> (7-x)) & 1)
									vdp_draw_pixel(sx + 8 + x, sy + y, color);
								if ((vram[pat_byte + 24 + y] >> (7-x)) & 1)
									vdp_draw_pixel(sx + 8 + x, sy + 8 + y, color);
							}
						}

					}
				}
			}
			SDL_RenderPresent(renderer);
		}
		usleep(500);
	}
	return 0;
}


static void vdp_init(struct arguments *arguments) {
	SDL_Window *window = SDL_CreateWindow("TMS9918A", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 288*arguments->scale, 224*arguments->scale, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	scale = arguments->scale;
	render_thread = SDL_CreateThread(vdp_draw, "Draw", NULL);
}

BusDevice VDP = {&vdp_init, &vdp_read, &vdp_write};
