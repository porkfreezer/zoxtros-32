/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * HD6309.c
 * This is the main file of the simulator. It handles parsing the
 * SREC input, then reading and calling the appropriate operations,
 * as well as debugging.
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "6309.h"
#include "reg.h"
#include <argp.h>
#include <time.h>

extern BusDevice VDP;
extern BusDevice RAM;
extern BusDevice ROM;
extern BusDevice VIA;
extern BusDevice ACIA;
extern BusDevice PTM;

// 2048 byte chunks
BusDevice *memmap[32] = {
	&RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, &RAM, // 32K RAM
	&VIA,
	&VDP,
	&PTM,
	&ROM,	// JOY
	&ROM, &ROM, &ROM,	// Unused
	&ACIA,
	&ROM, &ROM, &ROM, &ROM, &ROM, &ROM, &ROM, &ROM	// 16K ROM
};

uint16_t last_branch = 0xFFFE;

extern void op_parse(uint8_t page, uint8_t instr);



static struct argp_option options[] = {
	{"scale",		's', "SCALE",		0, "Scale of video output pixels"},
	{"debug",		'd', NULL,			0, "Step through each instruction and print registers"},
	{"breakpoint",	'b', "ADDRESS",		0, "Set a breakpoint at hex ADDRESS (the first byte of an instruction only)"},
	{"watch",		'w', "ADDRESS",		0, "Watch a memory address and print when it is written to"},
	{0}
};

static struct arguments arguments = {
	.scale = 4,
	.srec = NULL,
	.debug = 0,
	.breakpoints = NULL,
	.num_breakpoints = 0
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	switch (key) {
		case 's':
			sscanf(arg, "%u", &arguments.scale);
			break;
		case 'd':
			arguments.debug = 1;
			break;
		case 'b':
			++arguments.num_breakpoints;
			arguments.breakpoints = realloc(arguments.breakpoints, sizeof(uint16_t) * arguments.num_breakpoints);
			if (arguments.breakpoints == NULL)
				exit(-1);
			sscanf(arg, "%4hx", &arguments.breakpoints[arguments.num_breakpoints-1]);
			printf("Setting breakpoint at 0x%04X\n", arguments.breakpoints[arguments.num_breakpoints-1]);
			break;
		case 'w':
			++arguments.num_watches;
			arguments.watches = realloc(arguments.watches, sizeof(uint16_t) * arguments.num_watches);
			if (arguments.watches == NULL)
				exit(-1);
			sscanf(arg, "%4hx", &arguments.watches[arguments.num_watches-1]);
			printf("Setting watchpoint at 0x%04X\n", arguments.watches[arguments.num_watches-1]);
			break;
		case ARGP_KEY_ARG:
			if (state->arg_num >= 1)
				argp_usage(state);
			arguments.srec = arg;
			break;
		case ARGP_KEY_END:
			if (state->arg_num < 1)
				argp_usage(state);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static char args_doc[] = "SREC";
static char doc[] = "A Zoxtros-32 6309 computer emulator";

static struct argp argp = {options, parse_opt, args_doc, doc};

static struct timespec delay_cycle = {0, 60000};
static ulong num_cycle = 0;
static struct timespec clock_start;
void clock_pulse(int times) {
	for (; times > 0; times--) {
		num_cycle++;
		if ((num_cycle & 0xFF) == 0) {
			struct timespec now;
			clock_gettime(CLOCK_MONOTONIC, &now);
			long diff = now.tv_nsec - clock_start.tv_nsec + (now.tv_sec - clock_start.tv_sec)*1000000000UL;
			if (diff > 256*500 && delay_cycle.tv_nsec > 0)
				delay_cycle.tv_nsec -= 10;
			else if (diff < 256*500)
				delay_cycle.tv_nsec += 10;
			clock_start = now;

			nanosleep(&delay_cycle, NULL);
		}
	}
}

static void dump_registers(void) {
	printf("Last branch from PC=0x%04X\n", last_branch);
	printf("Instruction at 0x%04X\n", *PC);
	  puts("Registers:");
	  puts("|A |B |D   |E |F |W   |Q       |X   |Y   |U   |S   |V   |DP|");
	printf("|%02X|%02X|%04X|%02X|%02X|%04X|%08X|%04X|%04X|%04X|%04X|%04X|%02X|\n\n",*A,*B,*D,*E,*F,*W,*Q,*X,*Y,*U,*S,*V,*DP);
	  puts("|E|F|H|I|N|Z|V|C|");
	printf("|%u|%u|%u|%u|%u|%u|%u|%u|\n",(*CC>>7)&1,(*CC>>6)&1,(*CC>>5)&1,(*CC>>4)&1,(*CC>>3)&1,(*CC>>2)&1,(*CC>>1)&1,*CC&1);
}

static void parse_srec(char *filename) {
	FILE *srec;
	if ((srec = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Error: Can't open SREC\n");
		exit(EXIT_FAILURE);
	}

	int invalid = 1;
	int line = 1;
	int size = 0;
	while (1) {
		char c = fgetc(srec);
		if (c == EOF) {
			invalid = 0;
			break;
		}
		if (c != 'S')
			break;
		c = fgetc(srec);
		if (c < '0' || c > '9')
			break;
		if (c != '1') {
			do {
				c = fgetc(srec);
			} while (c != '\n' && c != EOF);
			++line;
		} else {
			char num[4];
			uint8_t checksum = 0;
			int bytecount;
			int address;
			int data;
			if (fread(num, 2, 1, srec) != 1)
				break;
			sscanf(num, "%2x", &bytecount);
			checksum += bytecount;
			bytecount -= 3;

			if (fread(num, 4, 1, srec) != 1)
				break;
			sscanf(num, "%4x", &address);
			checksum += (address >> 8) + (address & 0xFF);

			int i;
			for (i = 0; i < bytecount; i++) {
				if (fread(num, 2, 1, srec) != 1)
					break;
				sscanf(num, "%2x", &data);
				checksum += data;
				if (address+i >= 0xC000) {
					ROM.write(address - 0xC000 + i, data);
					++size;
				}
			}
			if (i < bytecount)
				break;

			if (fread(num, 2, 1, srec) != 1)
				break;
			int checksum_check;
			sscanf(num, "%2x", &checksum_check);
			if ((~checksum & 0xFF) != checksum_check)
				break;

			do {
				c = fgetc(srec);
			} while (c == '\n' || c == '\r');
			fseek(srec, -1, SEEK_CUR);
			++line;
		}
	}
	if (invalid) {
		fprintf(stderr, "Error: Invalid SREC on line %d\n", line);
		exit(EXIT_FAILURE);
	}
	printf("%dB loaded from '%s'\n", size, filename);
}

static int step = 0;

static void debug_wait() {
	printf("> ");
	int c;
	int ok = 1;
	unsigned a;
	do {
		ok = 1;
		c = getchar();
		switch (c) {
			case 'q':
				SDL_Quit();
				exit(0);
			case 's':
				puts("Step");
				step = 1;
				break;
			case 'c':
				puts("Continuing . . .");
				break;
			case 'b':
				ok = 0;
				scanf(" %4x", &a);
				++arguments.num_breakpoints;
				arguments.breakpoints = realloc(arguments.breakpoints, arguments.num_breakpoints*sizeof(uint16_t));
				if (arguments.breakpoints == NULL)
					exit(-1);
				arguments.breakpoints[arguments.num_breakpoints - 1] = a;
				printf("Adding breakpoint at 0x%04X\n> ", a);
				break;
			case 'm':
				ok = 0;
				scanf(" %4x", &a);
				printf("0x%04X = 0x%02X\n> ", a, r(a));
				break;
			case 'i':
				*PC = r16(0xFFFC);
				step = 1;
				break;
			default:
				printf("Enter q, s, b, m, or c\n> ");
				ok = 0;
				break;
		}
		while ((c = getchar()) != EOF && c != '\n');
	} while (!ok);
}

inline uint8_t r(uint address) {
	if (address > 0xFFFF) {
		fprintf(stderr, "ERROR: PC=0x%04X Out of bounds read to 0x%04X\n", *PC, address);
		exit(EXIT_FAILURE);
	}
	return memmap[address >> 11]->read(address);
}
inline uint16_t r16(uint address) {
	return (r(address) << 8) | r((address) + 1);
}
inline uint32_t r32(uint address) {
	return (r16(address) << 16) | r16((address) + 2);
}

inline void w(uint address, uint8_t data) {
	if (address > 0xFFFF) {
		fprintf(stderr, "ERROR: PC=0x%04X Out of bounds write to 0x%04X\n", *PC, address);
		exit(EXIT_FAILURE);
	}
	if (arguments.num_watches) {
		for (int i = 0; i < arguments.num_watches; i++) {
			if (address == arguments.watches[i])
				printf(" WATCH %d: PC=0x%04X    0x%04X=0x%02X\n", i, *PC, address, data);
		}
	}
	memmap[address >> 11]->write(address, data);
}
inline void w16(uint address, uint16_t data) {
	w(address, data >> 8);
	w(address + 1, data & 0xFF);
}
inline void w32(uint address, uint32_t data) {
	w16(address, data >> 16);
	w16(address + 2, data & 0xFFFF);
}

int main(int argc, char **argv) {

	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	parse_srec(arguments.srec);

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

	VDP.init(&arguments);
	PTM.init(&arguments);

	int ctrl = 0;
	SDL_Event event;

	*PC = r16(0xFFFE);
	SDL_StartTextInput();
	clock_gettime(CLOCK_MONOTONIC, &clock_start);
	while (1) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					SDL_Quit();
					return 0;
				case SDL_KEYDOWN: {
					uint32_t code = event.key.keysym.sym;
					if (code == SDLK_RETURN) {
						w(0x0007, 0x0A);
						w(0x0006, 0x7F);
					}
					else if (code == SDLK_BACKSPACE) {
						w(0x0007, 0x7F);
						w(0x0006, 0x7F);
					}
					else if (code == SDLK_LCTRL)
						ctrl = 1;
					else if (code == SDLK_c && ctrl) {
						w(0x0007, 0x03);
						w(0x0006, 0x7F);
					}
					else if (code == SDLK_UP)
						w(0x000B, 1);
					else if (code == SDLK_DOWN)
						w(0x000C, 1);
					else if (code == SDLK_RIGHT)
						w(0x000D, 1);
					else if (code == SDLK_LEFT)
						w(0x000E, 1);
					else if (code == SDLK_RALT)
						w(0x000F, 1);
					else if (code == SDLK_RCTRL)
						w(0x0010, 1);
					break;
				}
				case SDL_KEYUP: {
					uint32_t code = event.key.keysym.sym;
					if (code == SDLK_LCTRL)
						ctrl = 0;
					else if (code == SDLK_UP)
						w(0x000B, 0);
					else if (code == SDLK_DOWN)
						w(0x000C, 0);
					else if (code == SDLK_RIGHT)
						w(0x000D, 0);
					else if (code == SDLK_LEFT)
						w(0x000E, 0);
					else if (code == SDLK_RALT)
						w(0x000F, 0);
					else if (code == SDLK_RCTRL)
						w(0x0010, 0);
					break;
				}
				case SDL_TEXTINPUT:
					w(0x0007, event.text.text[0]);
					w(0x0006, 0x7F);
					break;
				case SDL_WINDOWEVENT: {
					if (event.window.event == SDL_WINDOWEVENT_EXPOSED)
						VDP.read(1);
				}

			}
		}

		for (int i = 0; i < 512; i++) {		// Run 512 instructions before polling for events, since it's slow
			if (arguments.debug || step) {
				step = 0;
				dump_registers();
				debug_wait();
			} else {
				for (int i = 0; i < arguments.num_breakpoints; i++) {
					if (*PC == arguments.breakpoints[i]) {
						printf("Breakpoint %d:\n", i+1);
						dump_registers();
						debug_wait();
						break;
					}
				}
			}
			uint8_t page = 0;
			uint8_t instr = r(*PC);
			++*PC;
			if (instr == 0x10 || instr == 0x11) {
				page = instr;
				instr = r(*PC);
				++*PC;
			}
			op_parse(page, instr);
		}
	}
	SDL_Quit();
	return 0;
}
