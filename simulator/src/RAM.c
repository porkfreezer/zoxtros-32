/*
 * Part of a simulator for my Zoxtros-32 6309 computer
 *
 * RAM.c
 * This is a simple device to represent the 32K RAM.
 *
 * Copyright 2021 Park Frazer <parkmfrazer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "6309.h"
#include "reg.h"

static uint8_t ram[32768];

uint8_t ram_read(uint address) {
	return ram[address & 0x7FFF];
}

void ram_write(uint address, uint data) {
	if (address < 0x8000)
		ram[address] = data;
}

BusDevice RAM = {NULL, &ram_read, &ram_write};
